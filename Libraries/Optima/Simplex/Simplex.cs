using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Numerical.Optima
{

    /// <summary> Simplex method numerical optimization. </summary>
    /// <remarks>
    /// David, 2014-03-19, 1.0.0.0. based on Amoeba Method Optimization using C# by James McCaffrey
    /// http://msdn.microsoft.com/en-us/magazine/dn201752.aspx <para>
    /// Reference: </para><para>
    /// A Simplex Method for Function Minimization, J.A. Nelder and R. Mead, The Computer Journal,
    /// vol. 7, no. 4, 1965, pp.308-313. </para><para>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class Simplex : SolverBase
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="identity">          The identity. </param>
        /// <param name="dimension">         The dimension. </param>
        /// <param name="minimumValue">      The minimum value. </param>
        /// <param name="maximumValue">      The maximum value. </param>
        /// <param name="maximumIterations"> The maximum iterations. </param>
        /// <param name="convergenceRadius"> The convergence radius. </param>
        /// <param name="objectiveLimit">    The objective limit. </param>
        public Simplex( string identity, int dimension, double minimumValue, double maximumValue, int maximumIterations,
                       double convergenceRadius, double objectiveLimit ) : this( identity, dimension, Enumerate( dimension, minimumValue ), Enumerate( dimension, maximumValue ), maximumIterations, Enumerate( dimension, convergenceRadius ), objectiveLimit )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks>
        /// Setting convergence radius: often, the expected solution resides outside the simplex
        /// convergence region between the best and worst simplex nodes. Consequently, the distance
        /// between the best and solution and expected value may exceed the convergence radius. For this
        /// reason, the convergence radius might be best set much smaller than the desired accuracy of
        /// the estimate.
        /// </remarks>
        /// <param name="identity">          The identity. </param>
        /// <param name="dimension">         The dimension. </param>
        /// <param name="minimumValues">     The minimum values. </param>
        /// <param name="maximumValues">     The maximum values. </param>
        /// <param name="maximumIterations"> The maximum iterations. </param>
        /// <param name="convergenceRadii">  The convergence Radii. </param>
        /// <param name="objectiveLimit">    The objective limit. </param>
        public Simplex( string identity, int dimension, double[] minimumValues, double[] maximumValues,
                       int maximumIterations, double[] convergenceRadii, double objectiveLimit ) : base( identity, dimension, maximumIterations, convergenceRadii, objectiveLimit )
        {
            this._SimplexSize = dimension + 1;
            this._MinimumValues = new double[dimension];
            Array.Copy( minimumValues, this._MinimumValues, dimension );
            this._MaximumValues = new double[dimension];
            Array.Copy( maximumValues, this._MaximumValues, this._MaximumValues.Length );
        }

        /// <summary> Constructor. </summary>
        /// <remarks>
        /// Setting convergence radius: often, the expected solution resides outside the simplex
        /// convergence region between the best and worst simplex nodes. Consequently, the distance
        /// between the best and solution and expected value may exceed the convergence radius. For this
        /// reason, the convergence radius might be best set much smaller than the desired accuracy of
        /// the estimate.
        /// </remarks>
        /// <param name="identity">          The identity. </param>
        /// <param name="dimension">         The dimension. </param>
        /// <param name="minimumValues">     The minimum values. </param>
        /// <param name="maximumValues">     The maximum values. </param>
        /// <param name="maximumIterations"> The maximum iterations. </param>
        /// <param name="convergenceRadii">  The convergence Radii. </param>
        /// <param name="objectiveLimit">    The objective limit. </param>
        public Simplex( string identity, int dimension, IEnumerable<double> minimumValues,
                       IEnumerable<double> maximumValues, int maximumIterations, IEnumerable<double> convergenceRadii,
                       double objectiveLimit ) : base( identity, dimension, maximumIterations, convergenceRadii, objectiveLimit )
        {
            this._SimplexSize = dimension + 1;
            this._MinimumValues = new double[dimension];
            Array.Copy( minimumValues.ToArray(), this._MinimumValues, this.Dimension );
            this._MaximumValues = new double[this.Dimension];
            Array.Copy( maximumValues.ToArray(), this._MaximumValues, this._MaximumValues.Length );
        }

        /// <summary> Validated simplex. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="simplex"> The simplex. </param>
        /// <returns> A Simplex. </returns>
        public static Simplex ValidatedSimplex( Simplex simplex )
        {
            return simplex is null ? throw new ArgumentNullException( nameof( simplex ) ) : simplex;
        }

        /// <summary> Creates a new solution. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> A Solution. </returns>
        public Solution NewSolution( IEnumerable<double> values )
        {
            if ( this.ClippingMode != ClippingModes.None )
            {
                values = Solution.Clip( values, this.ClippingMode, this._MinimumValues, this._MaximumValues );
            }

            return new Solution( values, this.ObjectiveFunction.EvaluateObjective( values ) );
        }

        /// <summary> Enumerate an array of values. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="length"> The length. </param>
        /// <param name="value">  The value. </param>
        /// <returns> An array of values. </returns>
        public static IEnumerable<double> Enumerate( int length, double value )
        {
            if ( length <= 0 )
                throw new ArgumentException( "length must be positive", nameof( length ) );
            var l = new List<double>();
            while ( l.Count != length )
                l.Add( value );
            return l;
        }

        /// <summary> The size of the simplex or the number of number of solutions</summary>
        private readonly int _SimplexSize;

        /// <summary> The minimum values. </summary>
        private readonly double[] _MinimumValues;

        /// <summary> The maximum values. </summary>
        private readonly double[] _MaximumValues;

        /// <summary> Gets or sets the clipping mode. </summary>
        /// <value> The clipping mode. </value>
        public ClippingModes ClippingMode { get; set; }

        /// <summary> The reflection coefficient. </summary>
        /// <value> The reflection coefficient. </value>
        public static double ReflectionCoefficient { get; private set; } = 1.0d;

        /// <summary> The contraction coefficient. </summary>
        /// <value> The contraction coefficient. </value>
        public static double ContractionCoefficient { get; private set; } = 0.5d;

        /// <summary> The expansion coefficient. </summary>
        /// <value> The expansion coefficient. </value>
        public static double ExpansionCoefficient { get; private set; } = 2.0d;

        #endregion

        #region " INITIALIZE "

        /// <summary> Generates the solutions. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="random">            The random. </param>
        /// <param name="count">             Number of solutions to generate. </param>
        /// <param name="minimumValues">     The minimum values. </param>
        /// <param name="maximumValues">     The maximum values. </param>
        /// <param name="objectivefunction"> The objective function. </param>
        /// <returns> The solutions. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private static Solution[] GenerateSolutions( Random random, int count, double[] minimumValues, double[] maximumValues, ObjectiveFunctionBase objectivefunction )
        {
            if ( minimumValues is null )
                throw new ArgumentNullException( nameof( minimumValues ) );
            if ( maximumValues is null )
                throw new ArgumentNullException( nameof( maximumValues ) );
            var solutions = new Solution[count];
            for ( int i = 0, loopTo = solutions.Length - 1; i <= loopTo; i++ )
                // calls the objective function to compute value
                solutions[i] = new Solution( random, minimumValues, maximumValues, objectivefunction );
            Array.Sort( solutions );
            return solutions;
        }

        /// <summary> Generates the solutions. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="offset">            The offset. </param>
        /// <param name="slope">             The slope. </param>
        /// <param name="count">             Number of solutions to generate. </param>
        /// <param name="minimumValues">     The minimum values. </param>
        /// <param name="maximumValues">     The maximum values. </param>
        /// <param name="objectivefunction"> The objective function. </param>
        /// <returns> The solutions. </returns>
        private static Solution[] GenerateSolutions( double offset, double slope, int count, double[] minimumValues, double[] maximumValues, ObjectiveFunctionBase objectivefunction )
        {
            if ( minimumValues is null )
                throw new ArgumentNullException( nameof( minimumValues ) );
            if ( maximumValues is null )
                throw new ArgumentNullException( nameof( maximumValues ) );
            var solutions = new Solution[count];
            double s = offset;
            for ( int i = 0, loopTo = solutions.Length - 1; i <= loopTo; i++ )
            {
                // calls the objective function to compute value
                solutions[i] = new Solution( s, minimumValues, maximumValues, objectivefunction );
                s += slope / (count - 1);
            }

            Array.Sort( solutions );
            return solutions;
        }

        /// <summary> Generates the solutions. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="random">            The random. </param>
        /// <param name="offset">            The offset. </param>
        /// <param name="slope">             The slope. </param>
        /// <param name="count">             Number of solutions to generate. </param>
        /// <param name="minimumValues">     The minimum values. </param>
        /// <param name="maximumValues">     The maximum values. </param>
        /// <param name="objectivefunction"> The objective function. </param>
        /// <returns> The solutions. </returns>
        private static Solution[] GenerateSolutions( Random random, double offset, double slope, int count, double[] minimumValues, double[] maximumValues, ObjectiveFunctionBase objectivefunction )
        {
            if ( minimumValues is null )
                throw new ArgumentNullException( nameof( minimumValues ) );
            if ( maximumValues is null )
                throw new ArgumentNullException( nameof( maximumValues ) );
            var solutions = new Solution[count];
            double s = offset;
            for ( int i = 0, loopTo = solutions.Length - 1; i <= loopTo; i++ )
            {
                // calls the objective function to compute value
                solutions[i] = new Solution( random, s, minimumValues, maximumValues, objectivefunction );
                s += slope / (count - 1);
            }

            Array.Sort( solutions );
            return solutions;
        }

        /// <summary> Gets or sets the default offset. </summary>
        /// <value> The default offset. </value>
        public static double DefaultOffset { get; set; } = 0.01d;

        /// <summary> Gets or sets the default slope. </summary>
        /// <value> The default slope. </value>
        public static double DefaultSlope { get; set; } = 1d - 2d * DefaultOffset;

        /// <summary> Initializes the simplex with a random set of solution values. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="random">            The random. </param>
        /// <param name="objectiveFunction"> The objective function. </param>
        public void Initialize( Random random, ObjectiveFunctionBase objectiveFunction )
        {
            if ( objectiveFunction is null )
                throw new ArgumentNullException( nameof( objectiveFunction ) );
            // define the relative initial sphere. At least one solution needs to be outside this sphere to start the search.
            int trialCount = 10;
            int trialNumber = 0;
            if ( !(this.ConvergenceRadii?.Any()) == true )
                throw new InvalidOperationException( "Convergence sphere not defined." );
            double maximumObjectiveRange;
            do
            {
                trialNumber += 1;
                this.Initialize( GenerateSolutions( random, DefaultOffset, DefaultSlope, this._SimplexSize, this._MinimumValues, this._MaximumValues, objectiveFunction ), objectiveFunction );
                maximumObjectiveRange = this.MaximumObjectiveRange();
            }
            while ( trialNumber < trialCount && (this.Converged( this.RelativeConvergenceSphere( MinimumInitialConvergenceRadius ) ) || maximumObjectiveRange > this.ObjectiveLimit && maximumObjectiveRange < MinimumInitialRelativeObjective * this.ObjectiveLimit) );
            if ( trialNumber >= trialCount )
            {
                if ( this.Converged( this.RelativeConvergenceSphere( MinimumInitialConvergenceRadius ) ) )
                {
                    throw new InvalidOperationException( "Failed initializing Simplex because it lies within 10% of the convergence sphere" );
                }
                else if ( maximumObjectiveRange > this.ObjectiveLimit && maximumObjectiveRange < MinimumInitialRelativeObjective * this.ObjectiveLimit )
                {
                    throw new InvalidOperationException( $"Failed initializing Simplex because the range of objective values [?,{maximumObjectiveRange}] is too tight." );
                }
            }
        }

        /// <summary> Initializes the simplex with a linear set of solution values. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="offset">            The offset. </param>
        /// <param name="slope">             The slope. </param>
        /// <param name="objectiveFunction"> The objective function. </param>
        public void Initialize( double offset, double slope, ObjectiveFunctionBase objectiveFunction )
        {
            if ( objectiveFunction is null )
                throw new ArgumentNullException( nameof( objectiveFunction ) );
            // define the relative initial sphere. At least one solution needs to be outside this sphere to start the search.
            int trialCount = 10;
            int trialNumber = 0;
            if ( !(this.ConvergenceRadii?.Any()) == true )
                throw new InvalidOperationException( "Convergence sphere not defined." );
            double maximumObjectiveRange;
            do
            {
                trialNumber += 1;
                this.Initialize( GenerateSolutions( offset, slope, this._SimplexSize, this._MinimumValues, this._MaximumValues, objectiveFunction ), objectiveFunction );
                maximumObjectiveRange = this.MaximumObjectiveRange();
            }
            while ( trialNumber < trialCount && (this.Converged( this.RelativeConvergenceSphere( MinimumInitialConvergenceRadius ) ) || maximumObjectiveRange > this.ObjectiveLimit && maximumObjectiveRange < MinimumInitialRelativeObjective * this.ObjectiveLimit) );
            if ( trialNumber >= trialCount )
            {
                if ( this.Converged( this.RelativeConvergenceSphere( MinimumInitialConvergenceRadius ) ) )
                {
                    throw new InvalidOperationException( "Failed initializing Simplex because it lies within 10% of the convergence sphere" );
                }
                else if ( maximumObjectiveRange > this.ObjectiveLimit && maximumObjectiveRange < MinimumInitialRelativeObjective * this.ObjectiveLimit )
                {
                    throw new InvalidOperationException( $"Failed initializing Simplex because the range of objective values [?,{maximumObjectiveRange}] is too tight." );
                }
            }
        }

        /// <summary> Initializes the simplex with a linear set of solution values. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="random">            The random. </param>
        /// <param name="offset">            The offset. </param>
        /// <param name="slope">             The slope. </param>
        /// <param name="objectiveFunction"> The objective function. </param>
        public void Initialize( Random random, double offset, double slope, ObjectiveFunctionBase objectiveFunction )
        {
            if ( objectiveFunction is null )
                throw new ArgumentNullException( nameof( objectiveFunction ) );
            // define the relative initial sphere. At least one solution needs to be outside this sphere to start the search.
            int trialCount = 10;
            int trialNumber = 0;
            if ( !(this.ConvergenceRadii?.Any()) == true )
                throw new InvalidOperationException( "Convergence sphere not defined." );
            double maximumObjectiveRange;
            do
            {
                trialNumber += 1;
                this.Initialize( GenerateSolutions( random, offset, slope, this._SimplexSize, this._MinimumValues, this._MaximumValues, objectiveFunction ), objectiveFunction );
                maximumObjectiveRange = this.MaximumObjectiveRange();
            }
            while ( trialNumber < trialCount && (this.Converged( this.RelativeConvergenceSphere( MinimumInitialConvergenceRadius ) ) || maximumObjectiveRange > this.ObjectiveLimit && maximumObjectiveRange < MinimumInitialRelativeObjective * this.ObjectiveLimit) );
            if ( trialNumber >= trialCount )
            {
                if ( this.Converged( this.RelativeConvergenceSphere( MinimumInitialConvergenceRadius ) ) )
                {
                    throw new InvalidOperationException( "Failed initializing Simplex because it lies within 10% of the convergence sphere" );
                }
                else if ( maximumObjectiveRange > this.ObjectiveLimit && maximumObjectiveRange < MinimumInitialRelativeObjective * this.ObjectiveLimit )
                {
                    throw new InvalidOperationException( $"Failed initializing Simplex because the range of objective values [?,{maximumObjectiveRange}] is too tight." );
                }
            }
        }

        /// <summary> Initializes the simplex with the specified set of solution values. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="solutions">         The solutions. </param>
        /// <param name="objectiveFunction"> The objective function. </param>
        public void Initialize( Solution[] solutions, ObjectiveFunctionBase objectiveFunction )
        {
            if ( objectiveFunction is null )
                throw new ArgumentNullException( nameof( objectiveFunction ) );
            if ( solutions is null )
                throw new ArgumentNullException( nameof( solutions ) );
            this.ObjectiveFunction = objectiveFunction;
            this.Solutions = new List<Solution>( solutions );
        }

        /// <summary> Initializes the simplex with a random set of solution values. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="objectiveFunction"> The objective function. </param>
        public void Initialize( ObjectiveFunctionBase objectiveFunction )
        {
            if ( objectiveFunction is null )
                throw new ArgumentNullException( nameof( objectiveFunction ) );
            this.ObjectiveFunction = objectiveFunction;
        }

        /// <summary> Adds a solution. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="solution"> The solution. </param>
        public override void AddSolution( Solution solution )
        {
            if ( solution is null )
                throw new ArgumentNullException( nameof( solution ) );
            if ( this.Solutions.Count >= this._SimplexSize )
            {
                throw new InvalidOperationException( $"Solution count {this.Solutions.Count} already equals simplex size {this._SimplexSize}" );
            }

            base.AddSolution( solution );
        }

        /// <summary> Adds a solution. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values">            The values. </param>
        /// <param name="objectiveFunction"> The objective function. </param>
        public void AddSolution( IEnumerable<double> values, ObjectiveFunctionBase objectiveFunction )
        {
            if ( objectiveFunction is null )
                throw new ArgumentNullException( nameof( objectiveFunction ) );
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            if ( this.ObjectiveFunction is null )
                this.Initialize( objectiveFunction );
            this.AddSolution( values, objectiveFunction.EvaluateObjective( values ) );
        }

        /// <summary> Adds a solution. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values">         The values. </param>
        /// <param name="objectiveValue"> The objective value. </param>
        public void AddSolution( IEnumerable<double> values, double objectiveValue )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            this.AddSolution( new Solution( values, objectiveValue ) );
        }

        /// <summary> Adds a solution. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="values"> The values. </param>
        public void AddSolution( IEnumerable<double> values )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            if ( this.ObjectiveFunction is null )
            {
                throw new InvalidOperationException( "Objective function not specified" );
            }

            this.AddSolution( values, this.ObjectiveFunction.EvaluateObjective( values ) );
        }

        /// <summary> Gets or sets the minimum initial convergence radius. </summary>
        /// <value> The minimum initial convergence radius. </value>
        public static double MinimumInitialConvergenceRadius { get; set; } = 0.1d;

        /// <summary> Enumerates relative convergence sphere in this collection. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="relativeRadius"> The relative radius. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process relative convergence sphere in this
        /// collection.
        /// </returns>
        public IEnumerable<double> RelativeConvergenceSphere( double relativeRadius )
        {
            var result = new double[this._MinimumValues.Length];
            for ( int i = 0, loopTo = result.Length - 1; i <= loopTo; i++ )
                result[i] = relativeRadius * (this._MaximumValues[i] - this._MinimumValues[i]);
            return result;
        }

        /// <summary> Validates the initial convergence sphere. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        public void ValidateInitialConvergenceSphere()
        {
            this.ValidateInitialConvergenceSphere( MinimumInitialConvergenceRadius );
        }

        /// <summary> Validates the initial convergence sphere. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="initialConvergenceRadius"> The initial convergence radius. </param>
        public void ValidateInitialConvergenceSphere( double initialConvergenceRadius )
        {
            if ( this.Converged( this.RelativeConvergenceSphere( initialConvergenceRadius ) ) )
            {
                throw new InvalidOperationException( $"Failed initializing Simplex because it lies within {initialConvergenceRadius:P} of the convergence sphere" );
            }
        }

        /// <summary> Maximum objective range. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns> A Double. </returns>
        public double MaximumObjectiveRange()
        {
            double maximumObjectiveValue = double.MinValue;
            foreach ( Solution sol in this.Solutions )
            {
                if ( maximumObjectiveValue < sol.Objective )
                    maximumObjectiveValue = sol.Objective;
            }

            return maximumObjectiveValue;
        }

        /// <summary> Objective range. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns> A Tuple(Of Double, Double) </returns>
        public Tuple<double, double> ObjectiveRange()
        {
            double minimumObjectiveValue = double.MaxValue;
            double maximumObjectiveValue = double.MinValue;
            foreach ( Solution sol in this.Solutions )
            {
                if ( minimumObjectiveValue > sol.Objective )
                    minimumObjectiveValue = sol.Objective;
                if ( maximumObjectiveValue < sol.Objective )
                    maximumObjectiveValue = sol.Objective;
            }

            return new Tuple<double, double>( minimumObjectiveValue, maximumObjectiveValue );
        }

        /// <summary> Gets or sets the minimum initial relative objective. </summary>
        /// <value> The minimum initial relative objective. </value>
        public static double MinimumInitialRelativeObjective { get; set; } = 10d;

        /// <summary> Validates the initial objectives. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        public void ValidateInitialObjectives()
        {
            double maximumObjectiveRange = this.MaximumObjectiveRange();
            if ( maximumObjectiveRange > this.ObjectiveLimit && maximumObjectiveRange < MinimumInitialRelativeObjective * this.ObjectiveLimit )
            {
                throw new InvalidOperationException( $"Failed initializing Simplex because the range of objective values [?,{maximumObjectiveRange}] is too tight." );
            }
        }

        /// <summary> Validates the initial values. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        public void ValidateInitialValues()
        {
            this.ValidateInitialConvergenceSphere();
            this.ValidateInitialObjectives();
        }

        #endregion

        #region " SIMPLEX "

        /// <summary> Calculates the centroid solution. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns>
        /// The centroid of all solution vectors except for the worst (highest index) vector.
        /// </returns>
        protected override Solution Centroid()
        {
            var c = new double[this.Dimension];
            // exclude the last solution
            for ( int i = 0, loopTo = this._SimplexSize - 2; i <= loopTo; i++ )
            {
                for ( int j = 0, loopTo1 = this.Dimension - 1; j <= loopTo1; j++ )
                    // accumulate sum of each vector component
                    c[j] += this.Solutions[i].Values().ElementAtOrDefault( j );
            }

            // get the average of each coordinate
            for ( int j = 0, loopTo2 = this.Dimension - 1; j <= loopTo2; j++ )
                c[j] /= this._SimplexSize - 1;

            // feed the centroid values to the objective functions to get the objective value.
            // This creates the centroid solution.
            return this.NewSolution( c );
        }

        /// <summary> Reflects the worst solution around the centroid. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="centroid"> The centroid. </param>
        /// <returns>
        /// The reflected solution extends from the worst (lowest index) solution through the centroid.
        /// </returns>
        private Solution Reflected( Solution centroid )
        {
            return this.Reflected( this.Solutions[this._SimplexSize - 1], centroid );
        }

        /// <summary> Gets the reflected solution around the centroid. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="solution"> The solution. </param>
        /// <param name="centroid"> The centroid. </param>
        /// <returns>
        /// The reflected solution extends from the worst (lowest index) solution through the centroid.
        /// </returns>
        private Solution Reflected( Solution solution, Solution centroid )
        {
            if ( solution is null )
                throw new ArgumentNullException( nameof( solution ) );
            if ( centroid is null )
                throw new ArgumentNullException( nameof( centroid ) );
            var r = new double[this.Dimension];
            for ( int j = 0, loopTo = this.Dimension - 1; j <= loopTo; j++ )
                r[j] = (1d + ReflectionCoefficient) * centroid.Values().ElementAtOrDefault( j ) - ReflectionCoefficient * solution.Values().ElementAtOrDefault( j );
            return this.NewSolution( r );
        }

        /// <summary> Gets the expanded solution around the centroid. </summary>
        /// <remarks> The expanded solution extends even more, from centroid, through reflected. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="reflected"> The reflected. </param>
        /// <param name="centroid">  The centroid. </param>
        /// <returns> The expanded solution around the centroid. </returns>
        private Solution Expanded( Solution reflected, Solution centroid )
        {
            if ( reflected is null )
                throw new ArgumentNullException( nameof( reflected ) );
            if ( centroid is null )
                throw new ArgumentNullException( nameof( centroid ) );
            var e = new double[this.Dimension];
            for ( int j = 0, loopTo = this.Dimension - 1; j <= loopTo; j++ )
                e[j] = ExpansionCoefficient * reflected.Values().ElementAtOrDefault( j ) + (1d - ExpansionCoefficient) * centroid.Values().ElementAtOrDefault( j );
            return this.NewSolution( e );
        }

        /// <summary> Contracts the worst solution around the centroid. </summary>
        /// <remarks>
        /// Contracted extends from worst (lowest index) towards centroid, but not past centroid.
        /// </remarks>
        /// <param name="centroid"> The centroid. </param>
        /// <returns> The contracted solution around the centroid. </returns>
        private Solution Contracted( Solution centroid )
        {
            return this.Contracted( this.WorstSolution, centroid );
        }

        /// <summary> Contracts the solution around the centroid. </summary>
        /// <remarks>
        /// Contracted extends from worst (lowest index) towards centroid, but not past centroid.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="solution"> The solution. </param>
        /// <param name="centroid"> The centroid. </param>
        /// <returns> The contracted solution around the centroid. </returns>
        private Solution Contracted( Solution solution, Solution centroid )
        {
            if ( solution is null )
                throw new ArgumentNullException( nameof( solution ) );
            if ( centroid is null )
                throw new ArgumentNullException( nameof( centroid ) );
            var v = new double[this.Dimension]; // didn't want to reuse 'c' from centroid routine
            for ( int j = 0, loopTo = this.Dimension - 1; j <= loopTo; j++ )
                v[j] = ContractionCoefficient * solution.Values().ElementAtOrDefault( j ) + (1d - ContractionCoefficient) * centroid.Values().ElementAtOrDefault( j );
            return this.NewSolution( v );
        }

        /// <summary> Shrinks the simplex. </summary>
        /// <remarks>
        /// Moves all vectors, except for the best vector (at index 0), halfway to the best vector
        /// compute new objective function values and sort result.
        /// </remarks>
        private void Shrink()
        {
            List<double> l;
            Solution solution;
            for ( int i = 1, loopTo = this._SimplexSize - 1; i <= loopTo; i++ ) // note we don't start at 0
            {
                l = new List<double>();
                for ( int j = 0, loopTo1 = this.Dimension - 1; j <= loopTo1; j++ )
                    // _Solutions(i).Values(j) = (_Solutions(i).Values(j) + _Solutions(0).Values(j)) / 2.0
                    l.Add( 0.5d * (this.Solutions[i].Values().ElementAtOrDefault( j ) + this.BestSolution.Values().ElementAtOrDefault( j )) );
                solution = this.Solutions[i];
                solution.CopyFrom( l );
                solution.Objective = this.ObjectiveFunction.EvaluateObjective( this.Solutions[i].Values() );
            }

            this.Sort();
        }

        /// <summary>
        /// Replaces the worst solution (at index size-1) with contents of parameter newSolution's vector.
        /// </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="newSolution"> The new solution. </param>
        private void ReplaceWorst( Solution newSolution )
        {
            if ( newSolution is null )
                throw new ArgumentNullException( nameof( newSolution ) );
            for ( int j = 0, loopTo = this.Dimension - 1; j <= loopTo; j++ )
            {
                // _Solutions(Me.simplexSize - 1).Values(j) = newSolution.Values(j)
            }

            var solution = this.Solutions[this._SimplexSize - 1];
            solution.CopyFrom( newSolution.Values() );
            solution.Objective = newSolution.Objective;
            this.Sort();
        }

        /// <summary> Is worse than all but worst. </summary>
        /// <remarks>
        /// Solve needs to know if the reflected solution is worse (greater objective value) than every
        /// solution in the simplex, except for the worst solution (highest index).
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="reflected"> The reflected. </param>
        /// <returns>
        /// <c>True</c> reflected solution is worse (greater objective value) than every solution in the
        /// simplex, except for the worst solution (highest index).
        /// </returns>
        private bool IsWorseThanAllButWorst( Solution reflected )
        {
            if ( reflected is null )
                throw new ArgumentNullException( nameof( reflected ) );
            for ( int i = 0, loopTo = this._SimplexSize - 2; i <= loopTo; i++ ) // not the highest index (worst)
            {
                // reflected is better (smaller value) than at least one of the non-worst solution vectors
                if ( reflected.Objective <= this.Solutions[i].Objective )
                {
                    return false;
                }
            }

            return true;
        }

        #endregion

        #region " SOLVE "

        /// <summary> Solves one step. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        public override void SolveStep()
        {
            var centroid = this.Centroid(); // compute centroid
            var reflected = this.Reflected( centroid ); // compute reflected
            if ( reflected.Objective < this.BestSolution.Objective ) // reflected is better than the current best
            {
                var expanded = this.Expanded( reflected, centroid ); // can we do even better??
                if ( expanded.Objective < this.BestSolution.Objective ) // winner! expanded is better than current best
                {
                    this.ReplaceWorst( expanded ); // replace current worst solution with expanded
                }
                else
                {
                    this.ReplaceWorst( reflected );
                } // it was worth a try...

                return;
            }

            if ( this.IsWorseThanAllButWorst( reflected ) == true )
            {
                // reflected is worse (larger value) than all solution vectors (except possibly the worst one)
                if ( reflected.Objective <= this.Solutions[this._SimplexSize - 1].Objective )
                {
                    // reflected is better (smaller) than the currENT worst (last index) vector
                    this.ReplaceWorst( reflected );
                }

                // compute a point 'inside' the simplex
                var contracted = this.Contracted( centroid );
                if ( contracted.Objective > this.Solutions[this._SimplexSize - 1].Objective )
                {
                    // contracted is worse (larger value) than current worst (last index) solution vector
                    this.Shrink();
                }
                else
                {
                    this.ReplaceWorst( contracted );
                }

                return;
            }

            this.ReplaceWorst( reflected );
            if ( this.DebugEnabled && !this.IsSorted() )
            {
                throw new InvalidOperationException( "Unsorted at iteration number = " + this.IterationNumber );
            }
        }

        /// <summary> Checks if sorted. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns> <c>True</c> if sorted. </returns>
        public bool IsSorted()
        {
            for ( int i = 0, loopTo = this.Solutions.Count - 2; i <= loopTo; i++ )
            {
                if ( this.Solutions[i].Objective > this.Solutions[i + 1].Objective )
                {
                    return false;
                }
            }

            return true;
        }

        #endregion

    }

    /// <summary> Values that represent clipping modes. </summary>
    /// <remarks> David, 2020-10-09. </remarks>
    [Flags]
    public enum ClippingModes
    {

        /// <summary> An enum constant representing the none option. </summary>
        None = 0,

        /// <summary> An enum constant representing the minimum option. </summary>
        Minimum = 1 << 0,

        /// <summary> An enum constant representing the maximum option. </summary>
        Maximum = 1 << 1
    }
}
