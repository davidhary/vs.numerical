﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

// Review the values of the assembly attributes

[assembly: AssemblyTitle("Simplex Tests")]
[assembly: AssemblyDescription("Simplex Tests")]
[assembly: AssemblyProduct("Numerical.SimplexTests")]
[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
