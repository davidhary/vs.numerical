''' <summary> Simplex method numerical optimization. </summary>
''' <remarks>
''' David, 3/19/2014, 1.0.0.0. based on Amoeba Method Optimization using C# by James McCaffrey
''' http://msdn.microsoft.com/en-us/magazine/dn201752.aspx <para>
''' Reference: </para><para>
''' A Simplex Method for Function Minimization, J.A. Nelder and R. Mead, The Computer Journal,
''' vol. 7, no. 4, 1965, pp.308-313. </para><para>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Class Simplex
    Inherits SolverBase

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="identity">          The identity. </param>
    ''' <param name="dimension">         The dimension. </param>
    ''' <param name="minimumValue">      The minimum value. </param>
    ''' <param name="maximumValue">      The maximum value. </param>
    ''' <param name="maximumIterations"> The maximum iterations. </param>
    ''' <param name="convergenceRadius"> The convergence radius. </param>
    ''' <param name="objectiveLimit">    The objective limit. </param>
    Public Sub New(ByVal identity As String, ByVal dimension As Integer,
                   ByVal minimumValue As Double, ByVal maximumValue As Double,
                   ByVal maximumIterations As Integer, ByVal convergenceRadius As Double,
                   ByVal objectiveLimit As Double)
        Me.New(identity, dimension, Simplex.Enumerate(dimension, minimumValue), Simplex.Enumerate(dimension, maximumValue),
               maximumIterations, Simplex.Enumerate(dimension, convergenceRadius), objectiveLimit)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks>
    ''' Setting convergence radius: often, the expected solution resides outside the simplex
    ''' convergence region between the best and worst simplex nodes. Consequently, the distance
    ''' between the best and solution and expected value may exceed the convergence radius. For this
    ''' reason, the convergence radius might be best set much smaller than the desired accuracy of
    ''' the estimate.
    ''' </remarks>
    ''' <param name="identity">          The identity. </param>
    ''' <param name="dimension">         The dimension. </param>
    ''' <param name="minimumValues">     The minimum values. </param>
    ''' <param name="maximumValues">     The maximum values. </param>
    ''' <param name="maximumIterations"> The maximum iterations. </param>
    ''' <param name="convergenceRadii">  The convergence Radii. </param>
    ''' <param name="objectiveLimit">    The objective limit. </param>
    Public Sub New(ByVal identity As String, ByVal dimension As Integer,
                   ByVal minimumValues() As Double, ByVal maximumValues() As Double,
                   ByVal maximumIterations As Integer, ByVal convergenceRadii() As Double,
                   ByVal objectiveLimit As Double)
        MyBase.New(identity, dimension, maximumIterations, convergenceRadii, objectiveLimit)
        Me._SimplexSize = dimension + 1
        Me._MinimumValues = New Double(dimension - 1) {}
        Array.Copy(minimumValues, Me._MinimumValues, dimension)
        Me._MaximumValues = New Double(dimension - 1) {}
        Array.Copy(maximumValues, Me._MaximumValues, Me._MaximumValues.Length)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks>
    ''' Setting convergence radius: often, the expected solution resides outside the simplex
    ''' convergence region between the best and worst simplex nodes. Consequently, the distance
    ''' between the best and solution and expected value may exceed the convergence radius. For this
    ''' reason, the convergence radius might be best set much smaller than the desired accuracy of
    ''' the estimate.
    ''' </remarks>
    ''' <param name="identity">          The identity. </param>
    ''' <param name="dimension">         The dimension. </param>
    ''' <param name="minimumValues">     The minimum values. </param>
    ''' <param name="maximumValues">     The maximum values. </param>
    ''' <param name="maximumIterations"> The maximum iterations. </param>
    ''' <param name="convergenceRadii">  The convergence Radii. </param>
    ''' <param name="objectiveLimit">    The objective limit. </param>
    Public Sub New(ByVal identity As String, ByVal dimension As Integer,
                   ByVal minimumValues As IEnumerable(Of Double), ByVal maximumValues As IEnumerable(Of Double),
                   ByVal maximumIterations As Integer, ByVal convergenceRadii As IEnumerable(Of Double),
                   ByVal objectiveLimit As Double)
        MyBase.New(identity, dimension, maximumIterations, convergenceRadii, objectiveLimit)
        Me._SimplexSize = dimension + 1
        Me._MinimumValues = New Double(dimension - 1) {}
        Array.Copy(minimumValues.ToArray, Me._MinimumValues, Me.Dimension)
        Me._MaximumValues = New Double(Me.Dimension - 1) {}
        Array.Copy(maximumValues.ToArray, Me._MaximumValues, Me._MaximumValues.Length)
    End Sub

    ''' <summary> Validated simplex. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="simplex"> The simplex. </param>
    ''' <returns> A Simplex. </returns>
    Public Shared Function ValidatedSimplex(ByVal simplex As Simplex) As Simplex
        If simplex Is Nothing Then Throw New ArgumentNullException(NameOf(simplex))
        Return simplex
    End Function

    ''' <summary> Creates a new solution. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="values"> The values. </param>
    ''' <returns> A Solution. </returns>
    Public Function NewSolution(ByVal values As IEnumerable(Of Double)) As Solution
        If Me.ClippingMode <> ClippingModes.None Then
            values = Solution.Clip(values, Me.ClippingMode, Me._MinimumValues, Me._MaximumValues)
        End If
        Return New Solution(values, Me.ObjectiveFunction.EvaluateObjective(values))
    End Function

    ''' <summary> Enumerate an array of values. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="length"> The length. </param>
    ''' <param name="value">  The value. </param>
    ''' <returns> An array of values. </returns>
    Public Shared Function Enumerate(ByVal length As Integer, ByVal value As Double) As IEnumerable(Of Double)
        If length <= 0 Then Throw New ArgumentException("length must be positive", NameOf(length))
        Dim l As New List(Of Double)()
        Do Until l.Count = length
            l.Add(value)
        Loop
        Return l
    End Function

    ''' <summary> The size of the simplex or the number of number of solutions</summary>
    Private ReadOnly _SimplexSize As Integer

    ''' <summary> The minimum values. </summary>
    Private ReadOnly _MinimumValues() As Double

    ''' <summary> The maximum values. </summary>
    Private ReadOnly _MaximumValues() As Double

    ''' <summary> Gets or sets the clipping mode. </summary>
    ''' <value> The clipping mode. </value>
    Public Property ClippingMode As ClippingModes

    ''' <summary> The reflection coefficient. </summary>
    ''' <value> The reflection coefficient. </value>
    Public Shared ReadOnly Property ReflectionCoefficient As Double = 1.0

    ''' <summary> The contraction coefficient. </summary>
    ''' <value> The contraction coefficient. </value>
    Public Shared ReadOnly Property ContractionCoefficient As Double = 0.5

    ''' <summary> The expansion coefficient. </summary>
    ''' <value> The expansion coefficient. </value>
    Public Shared ReadOnly Property ExpansionCoefficient As Double = 2.0

#End Region

#Region " INITIALIZE "

    ''' <summary> Generates the solutions. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="random">            The random. </param>
    ''' <param name="count">             Number of solutions to generate. </param>
    ''' <param name="minimumValues">     The minimum values. </param>
    ''' <param name="maximumValues">     The maximum values. </param>
    ''' <param name="objectivefunction"> The objective function. </param>
    ''' <returns> The solutions. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Shared Function GenerateSolutions(ByVal random As Random, ByVal count As Integer,
                                              ByVal minimumValues() As Double, ByVal maximumValues() As Double,
                                              ByVal objectivefunction As ObjectiveFunctionBase) As Solution()
        If minimumValues Is Nothing Then Throw New ArgumentNullException(NameOf(minimumValues))
        If maximumValues Is Nothing Then Throw New ArgumentNullException(NameOf(maximumValues))
        Dim solutions() As Solution = New Solution(count - 1) {}
        For i As Integer = 0 To solutions.Length - 1
            ' calls the objective function to compute value
            solutions(i) = New Solution(random, minimumValues, maximumValues, objectivefunction)
        Next i
        Array.Sort(solutions)
        Return solutions
    End Function

    ''' <summary> Generates the solutions. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="offset">            The offset. </param>
    ''' <param name="slope">             The slope. </param>
    ''' <param name="count">             Number of solutions to generate. </param>
    ''' <param name="minimumValues">     The minimum values. </param>
    ''' <param name="maximumValues">     The maximum values. </param>
    ''' <param name="objectivefunction"> The objective function. </param>
    ''' <returns> The solutions. </returns>
    Private Shared Function GenerateSolutions(ByVal offset As Double, ByVal slope As Double, ByVal count As Integer,
                                              ByVal minimumValues() As Double, ByVal maximumValues() As Double,
                                              ByVal objectivefunction As ObjectiveFunctionBase) As Solution()
        If minimumValues Is Nothing Then Throw New ArgumentNullException(NameOf(minimumValues))
        If maximumValues Is Nothing Then Throw New ArgumentNullException(NameOf(maximumValues))
        Dim solutions() As Solution = New Solution(count - 1) {}
        Dim s As Double = offset
        For i As Integer = 0 To solutions.Length - 1
            ' calls the objective function to compute value
            solutions(i) = New Solution(s, minimumValues, maximumValues, objectivefunction)
            s += slope / (count - 1)
        Next i
        Array.Sort(solutions)
        Return solutions
    End Function

    ''' <summary> Generates the solutions. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="random">            The random. </param>
    ''' <param name="offset">            The offset. </param>
    ''' <param name="slope">             The slope. </param>
    ''' <param name="count">             Number of solutions to generate. </param>
    ''' <param name="minimumValues">     The minimum values. </param>
    ''' <param name="maximumValues">     The maximum values. </param>
    ''' <param name="objectivefunction"> The objective function. </param>
    ''' <returns> The solutions. </returns>
    Private Shared Function GenerateSolutions(ByVal random As Random, ByVal offset As Double, ByVal slope As Double, ByVal count As Integer,
                                              ByVal minimumValues() As Double, ByVal maximumValues() As Double,
                                              ByVal objectivefunction As ObjectiveFunctionBase) As Solution()
        If minimumValues Is Nothing Then Throw New ArgumentNullException(NameOf(minimumValues))
        If maximumValues Is Nothing Then Throw New ArgumentNullException(NameOf(maximumValues))
        Dim solutions() As Solution = New Solution(count - 1) {}
        Dim s As Double = offset
        For i As Integer = 0 To solutions.Length - 1
            ' calls the objective function to compute value
            solutions(i) = New Solution(random, s, minimumValues, maximumValues, objectivefunction)
            s += slope / (count - 1)
        Next i
        Array.Sort(solutions)
        Return solutions
    End Function

    ''' <summary> Gets or sets the default offset. </summary>
    ''' <value> The default offset. </value>
    Public Shared Property DefaultOffset As Double = 0.01

    ''' <summary> Gets or sets the default slope. </summary>
    ''' <value> The default slope. </value>
    Public Shared Property DefaultSlope As Double = 1 - 2 * DefaultOffset

    ''' <summary> Initializes the simplex with a random set of solution values. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="random">            The random. </param>
    ''' <param name="objectiveFunction"> The objective function. </param>
    Public Sub Initialize(ByVal random As Random, ByVal objectiveFunction As ObjectiveFunctionBase)
        If objectiveFunction Is Nothing Then Throw New ArgumentNullException(NameOf(objectiveFunction))
        ' define the relative initial sphere. At least one solution needs to be outside this sphere to start the search.
        Dim trialCount As Integer = 10
        Dim trialNumber As Integer = 0
        If Not Me.ConvergenceRadii?.Any Then Throw New InvalidOperationException("Convergence sphere not defined.")
        Dim maximumObjectiveRange As Double

        Do
            trialNumber += 1
            Me.Initialize(Simplex.GenerateSolutions(random, Simplex.DefaultOffset, Simplex.DefaultSlope,
                                                    Me._SimplexSize, Me._MinimumValues, Me._MaximumValues, objectiveFunction), objectiveFunction)
            maximumObjectiveRange = Me.MaximumObjectiveRange
        Loop While trialNumber < trialCount AndAlso
                   (Me.Converged(Me.RelativeConvergenceSphere(Simplex.MinimumInitialConvergenceRadius)) OrElse
                   ((maximumObjectiveRange > Me.ObjectiveLimit) AndAlso maximumObjectiveRange < Simplex.MinimumInitialRelativeObjective * Me.ObjectiveLimit))
        If trialNumber >= trialCount Then
            If Me.Converged(Me.RelativeConvergenceSphere(Simplex.MinimumInitialConvergenceRadius)) Then
                Throw New InvalidOperationException("Failed initializing Simplex because it lies within 10% of the convergence sphere")
            ElseIf (maximumObjectiveRange > Me.ObjectiveLimit) AndAlso maximumObjectiveRange < Simplex.MinimumInitialRelativeObjective * Me.ObjectiveLimit Then
                Throw New InvalidOperationException($"Failed initializing Simplex because the range of objective values [?,{maximumObjectiveRange}] is too tight.")
            End If
        End If
    End Sub

    ''' <summary> Initializes the simplex with a linear set of solution values. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="offset">            The offset. </param>
    ''' <param name="slope">             The slope. </param>
    ''' <param name="objectiveFunction"> The objective function. </param>
    Public Sub Initialize(ByVal offset As Double, ByVal slope As Double, ByVal objectiveFunction As ObjectiveFunctionBase)
        If objectiveFunction Is Nothing Then Throw New ArgumentNullException(NameOf(objectiveFunction))
        ' define the relative initial sphere. At least one solution needs to be outside this sphere to start the search.
        Dim trialCount As Integer = 10
        Dim trialNumber As Integer = 0
        If Not Me.ConvergenceRadii?.Any Then Throw New InvalidOperationException("Convergence sphere not defined.")
        Dim maximumObjectiveRange As Double

        Do
            trialNumber += 1
            Me.Initialize(Simplex.GenerateSolutions(offset, slope, Me._SimplexSize, Me._MinimumValues, Me._MaximumValues, objectiveFunction), objectiveFunction)
            maximumObjectiveRange = Me.MaximumObjectiveRange
        Loop While trialNumber < trialCount AndAlso
                   (Me.Converged(Me.RelativeConvergenceSphere(Simplex.MinimumInitialConvergenceRadius)) OrElse
                   ((maximumObjectiveRange > Me.ObjectiveLimit) AndAlso maximumObjectiveRange < Simplex.MinimumInitialRelativeObjective * Me.ObjectiveLimit))
        If trialNumber >= trialCount Then
            If Me.Converged(Me.RelativeConvergenceSphere(Simplex.MinimumInitialConvergenceRadius)) Then
                Throw New InvalidOperationException("Failed initializing Simplex because it lies within 10% of the convergence sphere")
            ElseIf (maximumObjectiveRange > Me.ObjectiveLimit) AndAlso maximumObjectiveRange < Simplex.MinimumInitialRelativeObjective * Me.ObjectiveLimit Then
                Throw New InvalidOperationException($"Failed initializing Simplex because the range of objective values [?,{maximumObjectiveRange}] is too tight.")
            End If
        End If
    End Sub

    ''' <summary> Initializes the simplex with a linear set of solution values. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="random">            The random. </param>
    ''' <param name="offset">            The offset. </param>
    ''' <param name="slope">             The slope. </param>
    ''' <param name="objectiveFunction"> The objective function. </param>
    Public Sub Initialize(ByVal random As Random, ByVal offset As Double, ByVal slope As Double, ByVal objectiveFunction As ObjectiveFunctionBase)
        If objectiveFunction Is Nothing Then Throw New ArgumentNullException(NameOf(objectiveFunction))
        ' define the relative initial sphere. At least one solution needs to be outside this sphere to start the search.
        Dim trialCount As Integer = 10
        Dim trialNumber As Integer = 0
        If Not Me.ConvergenceRadii?.Any Then Throw New InvalidOperationException("Convergence sphere not defined.")
        Dim maximumObjectiveRange As Double

        Do
            trialNumber += 1
            Me.Initialize(Simplex.GenerateSolutions(random, offset, slope, Me._SimplexSize, Me._MinimumValues, Me._MaximumValues, objectiveFunction), objectiveFunction)
            maximumObjectiveRange = Me.MaximumObjectiveRange
        Loop While trialNumber < trialCount AndAlso
                   (Me.Converged(Me.RelativeConvergenceSphere(Simplex.MinimumInitialConvergenceRadius)) OrElse
                   ((maximumObjectiveRange > Me.ObjectiveLimit) AndAlso maximumObjectiveRange < Simplex.MinimumInitialRelativeObjective * Me.ObjectiveLimit))
        If trialNumber >= trialCount Then
            If Me.Converged(Me.RelativeConvergenceSphere(Simplex.MinimumInitialConvergenceRadius)) Then
                Throw New InvalidOperationException("Failed initializing Simplex because it lies within 10% of the convergence sphere")
            ElseIf (maximumObjectiveRange > Me.ObjectiveLimit) AndAlso maximumObjectiveRange < Simplex.MinimumInitialRelativeObjective * Me.ObjectiveLimit Then
                Throw New InvalidOperationException($"Failed initializing Simplex because the range of objective values [?,{maximumObjectiveRange}] is too tight.")
            End If
        End If
    End Sub

    ''' <summary> Initializes the simplex with the specified set of solution values. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="solutions">         The solutions. </param>
    ''' <param name="objectiveFunction"> The objective function. </param>
    Public Sub Initialize(ByVal solutions() As Solution, ByVal objectiveFunction As ObjectiveFunctionBase)
        If objectiveFunction Is Nothing Then Throw New ArgumentNullException(NameOf(objectiveFunction))
        If solutions Is Nothing Then Throw New ArgumentNullException(NameOf(solutions))
        Me.ObjectiveFunction = objectiveFunction
        Me.Solutions = New List(Of Solution)(solutions)
    End Sub

    ''' <summary> Initializes the simplex with a random set of solution values. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="objectiveFunction"> The objective function. </param>
    Public Sub Initialize(ByVal objectiveFunction As ObjectiveFunctionBase)
        If objectiveFunction Is Nothing Then Throw New ArgumentNullException(NameOf(objectiveFunction))
        Me.ObjectiveFunction = objectiveFunction
    End Sub

    ''' <summary> Adds a solution. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="solution"> The solution. </param>
    Public Overrides Sub AddSolution(ByVal solution As Solution)
        If solution Is Nothing Then Throw New ArgumentNullException(NameOf(solution))
        If Me.Solutions.Count >= Me._SimplexSize Then
            Throw New InvalidOperationException($"Solution count {Me.Solutions.Count } already equals simplex size {Me._SimplexSize}")
        End If
        MyBase.AddSolution(solution)
    End Sub

    ''' <summary> Adds a solution. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values">            The values. </param>
    ''' <param name="objectiveFunction"> The objective function. </param>
    Public Overloads Sub AddSolution(ByVal values As IEnumerable(Of Double), ByVal objectiveFunction As ObjectiveFunctionBase)
        If objectiveFunction Is Nothing Then Throw New ArgumentNullException(NameOf(objectiveFunction))
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If Me.ObjectiveFunction Is Nothing Then Me.Initialize(objectiveFunction)
        Me.AddSolution(values, objectiveFunction.EvaluateObjective(values))
    End Sub

    ''' <summary> Adds a solution. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values">         The values. </param>
    ''' <param name="objectiveValue"> The objective value. </param>
    Public Overloads Sub AddSolution(ByVal values As IEnumerable(Of Double), ByVal objectiveValue As Double)
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Me.AddSolution(New Solution(values, objectiveValue))
    End Sub

    ''' <summary> Adds a solution. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="values"> The values. </param>
    Public Overloads Sub AddSolution(ByVal values As IEnumerable(Of Double))
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If Me.ObjectiveFunction Is Nothing Then
            Throw New InvalidOperationException("Objective function not specified")
        End If
        Me.AddSolution(values, Me.ObjectiveFunction.EvaluateObjective(values))
    End Sub

    ''' <summary> Gets or sets the minimum initial convergence radius. </summary>
    ''' <value> The minimum initial convergence radius. </value>
    Public Shared Property MinimumInitialConvergenceRadius As Double = 0.1

    ''' <summary> Enumerates relative convergence sphere in this collection. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="relativeRadius"> The relative radius. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process relative convergence sphere in this
    ''' collection.
    ''' </returns>
    Public Function RelativeConvergenceSphere(ByVal relativeRadius As Double) As IEnumerable(Of Double)
        Dim result As Double() = New Double(Me._MinimumValues.Length - 1) {}
        For i As Integer = 0 To result.Length - 1
            result(i) = relativeRadius * (Me._MaximumValues(i) - Me._MinimumValues(i))
        Next
        Return result
    End Function

    ''' <summary> Validates the initial convergence sphere. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    Public Sub ValidateInitialConvergenceSphere()
        Me.ValidateInitialConvergenceSphere(Simplex.MinimumInitialConvergenceRadius)
    End Sub

    ''' <summary> Validates the initial convergence sphere. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="initialConvergenceRadius"> The initial convergence radius. </param>
    Public Sub ValidateInitialConvergenceSphere(ByVal initialConvergenceRadius As Double)
        If Me.Converged(Me.RelativeConvergenceSphere(initialConvergenceRadius)) Then
            Throw New InvalidOperationException($"Failed initializing Simplex because it lies within {initialConvergenceRadius:P} of the convergence sphere")
        End If
    End Sub

    ''' <summary> Maximum objective range. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <returns> A Double. </returns>
    Public Function MaximumObjectiveRange() As Double
        Dim maximumObjectiveValue As Double = Double.MinValue
        For Each sol As Solution In Me.Solutions
            If maximumObjectiveValue < sol.Objective Then maximumObjectiveValue = sol.Objective
        Next
        Return maximumObjectiveValue
    End Function

    ''' <summary> Objective range. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <returns> A Tuple(Of Double, Double) </returns>
    Public Function ObjectiveRange() As Tuple(Of Double, Double)
        Dim minimumObjectiveValue As Double = Double.MaxValue
        Dim maximumObjectiveValue As Double = Double.MinValue
        For Each sol As Solution In Me.Solutions
            If minimumObjectiveValue > sol.Objective Then minimumObjectiveValue = sol.Objective
            If maximumObjectiveValue < sol.Objective Then maximumObjectiveValue = sol.Objective
        Next
        Return New Tuple(Of Double, Double)(minimumObjectiveValue, maximumObjectiveValue)
    End Function

    ''' <summary> Gets or sets the minimum initial relative objective. </summary>
    ''' <value> The minimum initial relative objective. </value>
    Public Shared Property MinimumInitialRelativeObjective As Double = 10

    ''' <summary> Validates the initial objectives. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    Public Sub ValidateInitialObjectives()
        Dim maximumObjectiveRange As Double = Me.MaximumObjectiveRange
        If (maximumObjectiveRange > Me.ObjectiveLimit) AndAlso maximumObjectiveRange < Simplex.MinimumInitialRelativeObjective * Me.ObjectiveLimit Then
            Throw New InvalidOperationException($"Failed initializing Simplex because the range of objective values [?,{maximumObjectiveRange}] is too tight.")
        End If
    End Sub

    ''' <summary> Validates the initial values. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    Public Sub ValidateInitialValues()
        Me.ValidateInitialConvergenceSphere()
        Me.ValidateInitialObjectives()
    End Sub

#End Region

#Region " SIMPLEX "

    ''' <summary> Calculates the centroid solution. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <returns>
    ''' The centroid of all solution vectors except for the worst (highest index) vector.
    ''' </returns>
    Protected Overrides Function Centroid() As Solution

        Dim c(Me.Dimension - 1) As Double
        ' exclude the last solution
        For i As Integer = 0 To Me._SimplexSize - 2
            For j As Integer = 0 To Me.Dimension - 1
                ' accumulate sum of each vector component
                c(j) += Me.Solutions(i).Values(j)
            Next j
        Next i

        ' get the average of each coordinate
        For j As Integer = 0 To Me.Dimension - 1
            c(j) /= (Me._SimplexSize - 1)
        Next j

        ' feed the centroid values to the objective functions to get the objective value.
        ' This creates the centroid solution.
        Return Me.NewSolution(c)
    End Function

    ''' <summary> Reflects the worst solution around the centroid. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="centroid"> The centroid. </param>
    ''' <returns>
    ''' The reflected solution extends from the worst (lowest index) solution through the centroid.
    ''' </returns>
    Private Function Reflected(ByVal centroid As Solution) As Solution
        Return Me.Reflected(Me.Solutions(Me._SimplexSize - 1), centroid)
    End Function

    ''' <summary> Gets the reflected solution around the centroid. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="solution"> The solution. </param>
    ''' <param name="centroid"> The centroid. </param>
    ''' <returns>
    ''' The reflected solution extends from the worst (lowest index) solution through the centroid.
    ''' </returns>
    Private Function Reflected(ByVal solution As Solution, ByVal centroid As Solution) As Solution
        If solution Is Nothing Then Throw New ArgumentNullException(NameOf(solution))
        If centroid Is Nothing Then Throw New ArgumentNullException(NameOf(centroid))
        Dim r(Me.Dimension - 1) As Double
        For j As Integer = 0 To Me.Dimension - 1
            r(j) = ((1 + Simplex.ReflectionCoefficient) * centroid.Values(j)) - (Simplex.ReflectionCoefficient * solution.Values(j))
        Next j
        Return Me.NewSolution(r)
    End Function

    ''' <summary> Gets the expanded solution around the centroid. </summary>
    ''' <remarks> The expanded solution extends even more, from centroid, through reflected. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="reflected"> The reflected. </param>
    ''' <param name="centroid">  The centroid. </param>
    ''' <returns> The expanded solution around the centroid. </returns>
    Private Function Expanded(ByVal reflected As Solution, ByVal centroid As Solution) As Solution
        If reflected Is Nothing Then Throw New ArgumentNullException(NameOf(reflected))
        If centroid Is Nothing Then Throw New ArgumentNullException(NameOf(centroid))
        Dim e(Me.Dimension - 1) As Double
        For j As Integer = 0 To Me.Dimension - 1
            e(j) = (ExpansionCoefficient * reflected.Values(j)) + ((1 - ExpansionCoefficient) * centroid.Values(j))
        Next j
        Return Me.NewSolution(e)
    End Function

    ''' <summary> Contracts the worst solution around the centroid. </summary>
    ''' <remarks>
    ''' Contracted extends from worst (lowest index) towards centroid, but not past centroid.
    ''' </remarks>
    ''' <param name="centroid"> The centroid. </param>
    ''' <returns> The contracted solution around the centroid. </returns>
    Private Function Contracted(ByVal centroid As Solution) As Solution
        Return Me.Contracted(Me.WorstSolution, centroid)
    End Function

    ''' <summary> Contracts the solution around the centroid. </summary>
    ''' <remarks>
    ''' Contracted extends from worst (lowest index) towards centroid, but not past centroid.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="solution"> The solution. </param>
    ''' <param name="centroid"> The centroid. </param>
    ''' <returns> The contracted solution around the centroid. </returns>
    Private Function Contracted(ByVal solution As Solution, ByVal centroid As Solution) As Solution
        If solution Is Nothing Then Throw New ArgumentNullException(NameOf(solution))
        If centroid Is Nothing Then Throw New ArgumentNullException(NameOf(centroid))
        Dim v(Me.Dimension - 1) As Double ' didn't want to reuse 'c' from centroid routine
        For j As Integer = 0 To Me.Dimension - 1
            v(j) = (ContractionCoefficient * solution.Values(j)) + ((1 - ContractionCoefficient) * centroid.Values(j))
        Next j
        Return Me.NewSolution(v)
    End Function

    ''' <summary> Shrinks the simplex. </summary>
    ''' <remarks>
    ''' Moves all vectors, except for the best vector (at index 0), halfway to the best vector
    ''' compute new objective function values and sort result.
    ''' </remarks>
    Private Sub Shrink()
        Dim l As List(Of Double)
        Dim solution As Solution
        For i As Integer = 1 To Me._SimplexSize - 1 ' note we don't start at 0
            l = New List(Of Double)
            For j As Integer = 0 To Me.Dimension - 1
                ' _Solutions(i).Values(j) = (_Solutions(i).Values(j) + _Solutions(0).Values(j)) / 2.0
                l.Add(0.5 * (Me.Solutions(i).Values(j) + Me.BestSolution.Values(j)))
            Next j
            solution = Me.Solutions(i)
            solution.CopyFrom(l)
            solution.Objective = Me.ObjectiveFunction.EvaluateObjective(Me.Solutions(i).Values)
        Next i
        Me.Sort()
    End Sub

    ''' <summary>
    ''' Replaces the worst solution (at index size-1) with contents of parameter newSolution's vector.
    ''' </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="newSolution"> The new solution. </param>
    Private Sub ReplaceWorst(ByVal newSolution As Solution)
        If newSolution Is Nothing Then Throw New ArgumentNullException(NameOf(newSolution))
        For j As Integer = 0 To Me.Dimension - 1
            ' _Solutions(Me.simplexSize - 1).Values(j) = newSolution.Values(j)
        Next j
        Dim solution As Solution = Me.Solutions(Me._SimplexSize - 1)
        solution.CopyFrom(newSolution.Values)
        solution.Objective = newSolution.Objective
        Me.Sort()
    End Sub

    ''' <summary> Is worse than all but worst. </summary>
    ''' <remarks>
    ''' Solve needs to know if the reflected solution is worse (greater objective value) than every
    ''' solution in the simplex, except for the worst solution (highest index).
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="reflected"> The reflected. </param>
    ''' <returns>
    ''' <c>True</c> reflected solution is worse (greater objective value) than every solution in the
    ''' simplex, except for the worst solution (highest index).
    ''' </returns>
    Private Function IsWorseThanAllButWorst(ByVal reflected As Solution) As Boolean
        If reflected Is Nothing Then Throw New ArgumentNullException(NameOf(reflected))
        For i As Integer = 0 To Me._SimplexSize - 2 ' not the highest index (worst)
            ' reflected is better (smaller value) than at least one of the non-worst solution vectors
            If reflected.Objective <= Me.Solutions(i).Objective Then
                Return False
            End If
        Next i
        Return True
    End Function

#End Region

#Region " SOLVE "

    ''' <summary> Solves one step. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    Public Overrides Sub SolveStep()

        Dim centroid As Solution = Me.Centroid ' compute centroid
        Dim reflected As Solution = Me.Reflected(centroid) ' compute reflected

        If reflected.Objective < Me.BestSolution.Objective Then ' reflected is better than the current best
            Dim expanded As Solution = Me.Expanded(reflected, centroid) ' can we do even better??
            If expanded.Objective < Me.BestSolution.Objective Then ' winner! expanded is better than current best
                Me.ReplaceWorst(expanded) ' replace current worst solution with expanded
            Else
                Me.ReplaceWorst(reflected) ' it was worth a try...
            End If
            Return
        End If

        If Me.IsWorseThanAllButWorst(reflected) = True Then
            ' reflected is worse (larger value) than all solution vectors (except possibly the worst one)
            If reflected.Objective <= Me.Solutions(Me._SimplexSize - 1).Objective Then
                ' reflected is better (smaller) than the currENT worst (last index) vector
                Me.ReplaceWorst(reflected)
            End If

            ' compute a point 'inside' the simplex
            Dim contracted As Solution = Me.Contracted(centroid)

            If contracted.Objective > Me.Solutions(Me._SimplexSize - 1).Objective Then
                ' contracted is worse (larger value) than current worst (last index) solution vector
                Me.Shrink()
            Else
                Me.ReplaceWorst(contracted)
            End If

            Return
        End If

        Me.ReplaceWorst(reflected)

        If Me.Debugging AndAlso Not Me.IsSorted() Then
            Throw New InvalidOperationException("Unsorted at iteration number = " & Me.IterationNumber)
        End If

    End Sub

    ''' <summary> Checks if sorted. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <returns> <c>True</c> if sorted. </returns>
    Public Function IsSorted() As Boolean
        For i As Integer = 0 To Me.Solutions.Count - 2
            If Me.Solutions(i).Objective > Me.Solutions(i + 1).Objective Then
                Return False
            End If
        Next i
        Return True
    End Function

#End Region

End Class

''' <summary> Values that represent clipping modes. </summary>
''' <remarks> David, 2020-10-09. </remarks>
<Flags>
Public Enum ClippingModes

    ''' <summary> An enum constant representing the none option. </summary>
    None = 0

    ''' <summary> An enum constant representing the minimum option. </summary>
    Minimum = 1

    ''' <summary> An enum constant representing the maximum option. </summary>
    Maximum = 2
End Enum

