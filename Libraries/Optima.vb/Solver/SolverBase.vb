''' <summary> Solver base. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/22/2014 </para>
''' </remarks>
Public MustInherit Class SolverBase

#Region " CONSTRUCTION "

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="identity">          The identity. </param>
    ''' <param name="dimension">         The dimension or the size of the solution values. </param>
    ''' <param name="maximumIterations"> The maximum iterations. </param>
    ''' <param name="convergenceRadii">  The convergence radii. </param>
    ''' <param name="objectiveLimit">    The objective limit. The solution is said to be optimized if
    '''                                  the objective is lower than the objective limen. </param>
    Protected Sub New(ByVal identity As String, ByVal dimension As Integer, ByVal maximumIterations As Integer,
                      ByVal convergenceRadii() As Double, ByVal objectiveLimit As Double)
        MyBase.New()
        Me._Identity = identity
        Me._Solutions = New List(Of Solution)
        Me._Dimension = dimension
        Me._MaximumIterations = maximumIterations
        Me._ConvergenceRadii = New List(Of Double)(convergenceRadii)
        Me._ObjectiveLimit = objectiveLimit

        ' establish a distance for determining convergence of values. This is the Hypotenuse.
        Me._ConvergenceRadius = Optima.Solution.Hypotenuse(Me._ConvergenceRadii)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="identity">          The identity. </param>
    ''' <param name="dimension">         The dimension or the size of the solution values. </param>
    ''' <param name="maximumIterations"> The maximum iterations. </param>
    ''' <param name="convergenceRadii">  The convergence radii. </param>
    ''' <param name="objectiveLimit">    The objective limit. The solution is said to be optimized if
    '''                                  the objective is lower than the objective limen. </param>
    Protected Sub New(ByVal identity As String, ByVal dimension As Integer, ByVal maximumIterations As Integer,
                      ByVal convergenceRadii As IEnumerable(Of Double), ByVal objectiveLimit As Double)
        MyBase.New()
        Me._Identity = identity
        Me._Solutions = New List(Of Solution)
        Me._Dimension = dimension
        Me._MaximumIterations = maximumIterations
        Me._ConvergenceRadii = New List(Of Double)(convergenceRadii)
        Me._ObjectiveLimit = objectiveLimit

        ' establish a distance for determining convergence of values. This is the Hypotenuse.
        Me._ConvergenceRadius = Optima.Solution.Hypotenuse(Me._ConvergenceRadii)
    End Sub

#End Region

#Region " SOLUTION "

    ''' <summary> Gets the identity. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The identity. </value>
    Public Property Identity As String

    ''' <summary> The dimension or the size of the solution values. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The dimension. </value>
    Public Property Dimension As Integer

    ''' <summary> Gets the objective function. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The objective function. </value>
    Public Property ObjectiveFunction As ObjectiveFunctionBase

#End Region

#Region " SOLVER "

    ''' <summary> Gets the initial simplex. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The initial simplex. </value>
    Public ReadOnly Property InitialSimplex As String

    ''' <summary> Gets the initial objective. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The initial objective. </value>
    Public ReadOnly Property InitialObjective As Double

    ''' <summary> Solves one step. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    Public MustOverride Sub SolveStep()

    ''' <summary> Optimizes the solution objective. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="checkCompletion"> The check completion function. </param>
    ''' <returns> The best solution. </returns>
    Public Function Solve(checkCompletion As Func(Of Boolean)) As Solution
        If checkCompletion Is Nothing Then Throw New ArgumentNullException(NameOf(checkCompletion))
        Me.IterationNumber = 0
        Me._InitialSimplex = Me.ToString()
        Me._InitialObjective = Me.BestSolution.Objective
        Do Until checkCompletion.Invoke
            Me.IterationNumber += 1

            If Me.Debugging Then
                If True Then
                    If Me.IterationNumber = 1 OrElse Me.IterationNumber Mod 10 = 0 Then
                        Console.Write($"At t = {Me.IterationNumber} current best solution = ")
                        Console.WriteLine(Me.BestSolution)
                    End If
                Else
                    Console.Write($"At t = {Me.IterationNumber} current best solution = ")
                    Console.WriteLine(Me.BestSolution)
                End If
            End If

            ' run one step.            
            Me.SolveStep()
        Loop
        Return Me.BestSolution
    End Function

    ''' <summary> Gets the debugging sentinel. </summary>
    ''' <value> True if debugging. </value>
    Public Property Debugging As Boolean

    ''' <summary> Optimizes the solution objective. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <returns> The best solution. </returns>
    Public Function Solve() As Solution
        Return Me.Solve(Function()
                            Return Me.HitCountOverflow OrElse Me.Converged OrElse Me.Optimized
                        End Function)
    End Function

#End Region

#Region " CONVERGENCE "

    ''' <summary> Gets the centroid. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <returns> A Solution. </returns>
    Protected MustOverride Function Centroid() As Solution

    ''' <summary> Gets the best solution. </summary>
    ''' <value> The solution. </value>
    Public Overridable ReadOnly Property BestSolution As Solution
        Get
            Return Me.Solutions(0)
        End Get
    End Property

    ''' <summary> Gets the worst solution. </summary>
    ''' <value> The worst solution. </value>
    Public ReadOnly Property WorstSolution As Solution
        Get
            Return Me.Solutions(Me.Dimension)
        End Get
    End Property

    ''' <summary> The solutions. </summary>
    Private _Solutions As List(Of Solution)

    ''' <summary> Gets or sets the solutions. </summary>
    ''' <value> The solutions. </value>
    Public Property Solutions() As IList(Of Solution)
        Get
            Return Me._Solutions
        End Get
        Protected Set(value As IList(Of Solution))
            Me._Solutions = New List(Of Solution)(value)
            Me.Sort()
        End Set
    End Property

    ''' <summary> Clears the solutions. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    Public Sub ClearSolutions()
        Me._Solutions.Clear()
    End Sub

    ''' <summary> Adds a solution. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="solution"> The solution. </param>
    Public Overridable Sub AddSolution(ByVal solution As Solution)
        If solution Is Nothing Then Throw New ArgumentNullException(NameOf(solution))
        Me._Solutions.Add(solution)
        Me.Sort()
    End Sub

    ''' <summary> Sorts the solutions. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    Protected Sub Sort()
        Me._Solutions.Sort()
    End Sub

    ''' <summary> Gets or sets the iteration number. </summary>
    ''' <value> The iteration number. </value>
    Public Property IterationNumber As Integer

    ''' <summary>
    ''' The objective limit. The solution is said to be optimized if the objective is lower than the
    ''' objective limen.
    ''' </summary>
    ''' <value> The objective limit. </value>
    Public Property ObjectiveLimit As Double

    ''' <summary> The maximum iterations. </summary>
    ''' <value> The maximum iterations. </value>
    Protected Property MaximumIterations As Integer

    ''' <summary>
    ''' The convergence radius. The Hypotenuse of the  <see cref="ConvergenceSphere()"/>.
    ''' </summary>
    ''' <value> The convergence radius. </value>
    Protected Property ConvergenceRadius As Double

    ''' <summary> The convergence radii. </summary>
    ''' <value> The convergence radii. </value>
    Public Overridable ReadOnly Property ConvergenceRadii As IList(Of Double)

    ''' <summary> Gets or sets the sentinel indicating if convergence sphere is relative. </summary>
    ''' <value> <c>True</c> if the convergence sphere is relative to the centroid. </value>
    Public Property UsesRelativeConvergenceSphere As Boolean

    ''' <summary> The convergence radii. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <returns> The radii establishing the convergence sphere. </returns>
    Public Function ConvergenceSphere() As IEnumerable(Of Double)
        Return If(Me.UsesRelativeConvergenceSphere, Me.ConvergenceSphere(Me.Centroid), Me.ConvergenceRadii)
    End Function

    ''' <summary>
    ''' Returns the radii establishing the convergence sphere for relative convergence.
    ''' </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="centroid"> The centroid. </param>
    ''' <returns> The radii establishing the convergence sphere. </returns>
    Public Function ConvergenceSphere(ByVal centroid As Solution) As IEnumerable(Of Double)
        If centroid Is Nothing Then Throw New ArgumentNullException(NameOf(centroid))
        Dim values As New List(Of Double)
        For i As Integer = 0 To Me._ConvergenceRadii.Count - 1
            values.Add(Me.ConvergenceRadii(i) * centroid.Values(i))
        Next
        Return values
    End Function

    ''' <summary> Hit count overflow. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <returns> <c>True</c> if the number of iterations hit the maximum iteration allowed. </returns>
    Public Function HitCountOverflow() As Boolean
        Return Me.IterationNumber >= Me._MaximumIterations
    End Function

    ''' <summary> Checks if the best solution is within the objective. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <returns> <c>True</c> if the best solution reached the objective. </returns>
    Public Function Optimized() As Boolean
        Return Math.Abs(Me.BestSolution.Objective) < Me._ObjectiveLimit
    End Function

    ''' <summary>
    ''' Checks if the solution converged. Namely, if the values are close to each other.
    ''' </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <returns> <c>True</c> if the solution values are close to each other. </returns>
    Public Function Converged() As Boolean
        Dim centroid As Solution = Me.Centroid
        Return Me.Converged(Me.ConvergenceSphere(centroid), centroid)
    End Function

    ''' <summary>
    ''' Checks if the solution converged. Namely, if values are within or on a convergence sphere
    ''' around the centroid.
    ''' </summary>
    ''' <remarks>
    ''' Checks if the solutions are within or on a convergence sphere around the centroid.
    ''' </remarks>
    ''' <param name="convergenceRadii"> The values previsions. </param>
    ''' <returns>
    ''' <c>True</c> if the solutions are all within or on a convergence sphere around the centroid.
    ''' </returns>
    Public Function Converged(ByVal convergenceRadii As IEnumerable(Of Double)) As Boolean
        Return Me.Converged(convergenceRadii, Me.Centroid)
    End Function

    ''' <summary>
    ''' Checks if the solution converged. Namely, if values are within or on a convergence sphere
    ''' around the centroid.
    ''' </summary>
    ''' <remarks>
    ''' Checks if the solutions are within or on a convergence sphere around the centroid.
    ''' </remarks>
    ''' <param name="convergenceRadii"> The values previsions. </param>
    ''' <param name="centroid">         The centroid; center of current simplex. </param>
    ''' <returns>
    ''' <c>True</c> if the solutions are all within or on a convergence sphere around the centroid.
    ''' </returns>
    Public Function Converged(ByVal convergenceRadii As IEnumerable(Of Double), ByVal centroid As Solution) As Boolean
        Dim affirmative As Boolean = True
        For Each solution As Solution In Me.Solutions
            affirmative = affirmative AndAlso solution.Converged(convergenceRadii, centroid)
            If Not affirmative Then
                Exit For
            End If
        Next
        Return affirmative
    End Function

#End Region

#Region " PRESENTATION "

    ''' <summary> Gets or sets the default format. </summary>
    ''' <value> The default format. </value>
    Public Shared Property DefaultFormat As String = "G5"

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <returns> A string that represents the current object. </returns>
    Public Overrides Function ToString() As String
        Return Me.ToString(Solution.DefaultValuesFormat, Solution.DefaultObjectiveFormat)
    End Function

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="valuesFormat">    The values format. </param>
    ''' <param name="objectiveFormat"> The objective format. </param>
    ''' <returns> A string that represents the current object. </returns>
    Public Overloads Function ToString(ByVal valuesFormat As String, ByVal objectiveFormat As String) As String
        Dim builder As New Text.StringBuilder
        For Each s As Solution In Me.Solutions
            builder.AppendLine($"{s.ToString(valuesFormat, objectiveFormat)}")
        Next
        Return builder.ToString.TrimEnd(Environment.NewLine.ToCharArray)
    End Function

    ''' <summary> Returns a string summarizing the simplex state. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <returns> A String. </returns>
    Public Function ToSummary() As String
        Return Me.ToSummary(Solution.DefaultValuesFormat, Solution.DefaultObjectiveFormat)
    End Function

    ''' <summary> Returns a string summarizing the simplex state. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="valuesFormat">    The values format. </param>
    ''' <param name="objectiveFormat"> The objective format. </param>
    ''' <returns> A String. </returns>
    Public Function ToSummary(ByVal valuesFormat As String, ByVal objectiveFormat As String) As String
        Dim builder As New System.Text.StringBuilder
        builder.Append($"Simplex {Me.Identity}")
        builder.Append($"; Solution: {Me.BestSolution.ToString(valuesFormat, objectiveFormat)}")
        builder.Append($"; Iterations: {Me.IterationNumber}")
        builder.Append($"; Converged: {Me.Converged}")
        builder.Append($"; Optimized: {Me.Optimized}")
        Return builder.ToString.TrimEnd(Environment.NewLine.ToCharArray)
    End Function

    ''' <summary> Converts this object to a detailed summary. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <returns> The given data converted to a String. </returns>
    Public Function ToDetailedSummary() As String
        Return Me.ToDetailedSummary(Solution.DefaultValuesFormat, Solution.DefaultObjectiveFormat)
    End Function

    ''' <summary> Converts this object to a detailed summary. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="valuesFormat">    The values format. </param>
    ''' <param name="objectiveFormat"> The objective format. </param>
    ''' <returns> The given data converted to a String. </returns>
    Public Function ToDetailedSummary(ByVal valuesFormat As String, ByVal objectiveFormat As String) As String
        Dim builder As New System.Text.StringBuilder
        builder.AppendLine(Me.ToSummary(valuesFormat, objectiveFormat))
        builder.AppendLine($"; Initial simplex: {Me.InitialSimplex}")
        builder.AppendLine($"; Final simplex: {Me}")
        builder.AppendLine($"; Desired Objective: {Me.ObjectiveLimit.ToString(objectiveFormat)}")
        Return builder.ToString.TrimEnd(Environment.NewLine.ToCharArray)
    End Function

#End Region

End Class
