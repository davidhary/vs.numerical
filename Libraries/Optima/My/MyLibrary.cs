﻿
namespace isr.Numerical.Optima.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-10-09. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = ( int ) Core.ProjectTraceEventId.Optima;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Optimization Algorithms Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Optimization Algorithms Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Numerical.Optima";

        /// <summary> The Strong Name of the test assembly. </summary>
        public const string TestAssemblyStrongName = "isr.Numerical.SimplexTests,PublicKey=" + Numerical.My.SolutionInfo.PublicKey;
    }
}