Imports System

' True during development 
#Const Debugging = True

Friend Module AmoebaProgram

    ''' <summary> Amoeba program. </summary>
    ''' <remarks> (c) 2013 Microsoft Corporation. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 3/19/2014, 1.0.0.0. based on
    ''' Amoeba Method Optimization using C# by James McCaffrey
    ''' http://msdn.microsoft.com/en-us/magazine/dn201752.aspx </para></remarks>
    Friend Class AmoebaProgram
#Const Debugging = True

        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="Rosenbrock's")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Shared Sub Main(ByVal args() As String)
            Try
                Console.WriteLine(vbCrLf & "Begin amoeba method optimization demo" & vbCrLf)
                Console.WriteLine("Solving Rosenbrock's function f(x,y) = 100*(y-x^2)^2 + (1-x)^2")
                Console.WriteLine("Function Has a minimum at x = 1.0, y = 1.0 when f = 0.0" & vbCrLf)

                Dim dimension As Integer = 2 ' problem dimension (number of variables to solve for)
                Dim amoebaSize As Integer = 3 ' number of potential solutions in the amoeba
                Dim minX As Double = -10.0
                Dim maxX As Double = 10.0
                Dim maxLoop As Integer = 50

                Console.WriteLine("Creating amoeba with size = " & amoebaSize)
                Console.WriteLine("Setting max loop = " & maxLoop)
                Dim a As New Amoeba(amoebaSize, dimension, minX, maxX, maxLoop) ' an amoeba method optimization solver

                Console.WriteLine(vbCrLf & "Initial amoeba is: " & vbCrLf)
                Console.WriteLine(a.ToString)

                Console.WriteLine(vbCrLf & "Beginning reflect-expand-contract solve loop" & vbCrLf)
                Dim sln As Solution = a.Solve
                Console.WriteLine(vbCrLf & "Solve complete" & vbCrLf)

                Console.WriteLine("Final amoeba is: " & vbCrLf)
                Console.WriteLine(a.ToString)

                Console.WriteLine(vbCrLf & "Best solution found: " & vbCrLf)
                Console.WriteLine(sln.ToString)

                Console.WriteLine(vbCrLf & "End amoeba method optimization demo" & vbCrLf)
                Console.ReadLine()
            Catch ex As Exception
                Console.WriteLine(ex.Message)
                Console.ReadLine()
            End Try
        End Sub

        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Shared Function ObjectiveFunction(ByVal vector() As Double, ByVal dataSource As Object) As Double
            ' Rosenbrock's function, the function to be minimized
            ' no data source needed here but real optimization problems will often be based on data
            Dim x As Double = vector(0)
            Dim y As Double = vector(1)
            Return 100.0 * Math.Pow((y - x * x), 2) + Math.Pow(1 - x, 2)
            'Return (x * x) + (y * y) 'sphere function
        End Function

    End Class ' program class

    ''' <summary> A potential Solution. </summary>
    ''' <remarks> (c) 2013 Microsoft Corporation. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 3/19/2014, 1.0.0.0"> Documented. </para></remarks>
    Public Class Solution
        Implements IComparable(Of Solution)

        ' a potential solution (array of double) and associated value (so can be sorted against several potential solutions
        Public ReadOnly Property Vector() As Double()

        Public Property Value As Double

#Const Debugging = True
#If Debugging Then
        Private Shared ReadOnly Random As New System.Random(1)
#Else
    Shared random As New System.Random(DateTimeOffset.Now.Millisecond)
#End If
        ''' <summary>   Constructor. </summary>
        ''' <remarks>   David, 2020-03-09. </remarks>
        ''' <param name="dimension"> The dimension. </param>
        ''' <param name="minX">      The minimum x coordinate. </param>
        ''' <param name="maxX">      The maximum x coordinate. </param>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Sub New(ByVal dimension As Integer, ByVal minX As Double, ByVal maxX As Double)
            ' a random Solution
            Me.Vector = New Double(dimension - 1) {}
            For i As Integer = 0 To dimension - 1
                Me.Vector(i) = (maxX - minX) * Solution.Random.NextDouble + minX
            Next i
            Me.Value = AmoebaProgram.ObjectiveFunction(Me.Vector, Nothing)
        End Sub

        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Sub New(ByVal vector() As Double)
            ' a specified solution
            Me.Vector = New Double(vector.Length - 1) {}
            Array.Copy(vector, Me.Vector, vector.Length)
            Me.Value = AmoebaProgram.ObjectiveFunction(Me.Vector, Nothing)
        End Sub

        ''' <summary>
        ''' Compares the current instance with another object of the same type and returns an integer
        ''' that indicates whether the current instance precedes, follows, or occurs in the same position
        ''' in the sort order as the other object.
        ''' </summary>
        ''' <remarks>   David, 2020-03-09. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="other"> An object to compare with this instance. </param>
        ''' <returns>
        ''' A value that indicates the relative order of the objects being compared. The return value has
        ''' these meanings: Value Meaning Less than zero This instance precedes <paramref name="other" />
        ''' in the sort order.  Zero This instance occurs in the same position in the sort order as
        ''' <paramref name="other" />. Greater than zero This instance follows <paramref name="other" />
        ''' in the sort order.
        ''' </returns>
        Public Function CompareTo(ByVal other As Solution) As Integer Implements IComparable(Of Solution).CompareTo ' based on vector/solution value
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            If Me.Value < other.Value Then
                Return -1
            ElseIf Me.Value > other.Value Then
                Return 1
            Else
                Return 0
            End If
        End Function

        Public Overrides Function ToString() As String
            Dim s As String = "[ "
            For i As Integer = 0 To Me.Vector.Length - 1
                If Me.Vector(i) >= 0.0 Then
                    s &= " "
                End If
                s &= Me.Vector(i).ToString("F2") & " "
            Next i
            s &= "] = " & Me.Value.ToString("F4")
            Return s
        End Function
    End Class

    ''' <summary> Amoeba method numerical optimization. </summary>
    ''' <remarks> David, 3/19/2014, Documented. <para>
    ''' Reference: </para><para>
    ''' A Simplex Method for Function Minimization, J.A. Nelder and R. Mead,
    ''' The Computer Journal, vol. 7, no. 4, 1965, pp.308-313. </para><para>
    ''' (c) 2013 Microsoft Corporation. All rights reserved. </para><para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Class Amoeba

        Public Property AmoebaSize As Integer ' number of solutions
        Public Property Dimension As Integer ' vector-solution size, also problem dimension
        Public ReadOnly Property Solutions() As Solution() ' potential solutions (vector + value)

        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0052:Remove unread private members", Justification:="<Pending>")>
        Public Property MinX As Double
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0052:Remove unread private members", Justification:="<Pending>")>
        Public Property MaxX As Double

        Public Property Alpha As Double ' reflection
        Public Property Beta As Double ' contraction
        Public Property Gamma As Double ' expansion

        Public Property MaxLoop As Integer ' limits main solving loop

        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Sub New(ByVal amoebaSize As Integer, ByVal dimension As Integer, ByVal minX As Double, ByVal maxX As Double, ByVal maxLoop As Integer)
            Me.AmoebaSize = amoebaSize
            Me.Dimension = dimension
            Me.MinX = minX
            Me.MaxX = maxX
            Me.Alpha = 1.0 ' hard-coded values from theory
            Me.Beta = 0.5
            Me.Gamma = 2.0

            Me.MaxLoop = maxLoop

            Me.Solutions = New Solution(amoebaSize - 1) {}
            For i As Integer = 0 To Me.Solutions.Length - 1
                Me.Solutions(i) = New Solution(dimension, minX, maxX) ' the Solution ctor calls the objective function to compute value
            Next i

            Array.Sort(Me.Solutions)
        End Sub

        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Function Centroid() As Solution
            ' return the centroid of all solution vectors except for the worst (highest index) vector
            Dim c(Me.Dimension - 1) As Double
            For i As Integer = 0 To Me.AmoebaSize - 2
                For j As Integer = 0 To Me.Dimension - 1
                    c(j) += Me.Solutions(i).Vector(j) ' accumulate sum of each vector component
                Next j
            Next i

            For j As Integer = 0 To Me.Dimension - 1
                c(j) = c(j) / (Me.AmoebaSize - 1)
            Next j

            Dim s As New Solution(c) ' feed vector to ctor which calls objective function to compute value
            Return s
        End Function

        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Function Reflected(ByVal centroid As Solution) As Solution
            ' the reflected solution extends from the worst (lowest index) solution through the centroid
            Dim r(Me.Dimension - 1) As Double
            Dim worst() As Double = Me.Solutions(Me.AmoebaSize - 1).Vector ' convenience only
            For j As Integer = 0 To Me.Dimension - 1
                r(j) = ((1 + Me.Alpha) * centroid.Vector(j)) - (Me.Alpha * worst(j))
            Next j
            Dim s As New Solution(r)
            Return s
        End Function

        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Function Expanded(ByVal reflected As Solution, ByVal centroid As Solution) As Solution
            ' expanded extends even more, from centroid, thru reflected
            Dim e(Me.Dimension - 1) As Double
            For j As Integer = 0 To Me.Dimension - 1
                e(j) = (Me.Gamma * reflected.Vector(j)) + ((1 - Me.Gamma) * centroid.Vector(j))
            Next j
            Dim s As New Solution(e)
            Return s
        End Function

        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Function Contracted(ByVal centroid As Solution) As Solution
            ' contracted extends from worst (lowest index) towards centoid, but not past centroid
            Dim v(Me.Dimension - 1) As Double ' didn't want to reuse 'c' from centoid routine
            Dim worst() As Double = Me.Solutions(Me.AmoebaSize - 1).Vector ' convenience only
            For j As Integer = 0 To Me.Dimension - 1
                v(j) = (Me.Beta * worst(j)) + ((1 - Me.Beta) * centroid.Vector(j))
            Next j
            Dim s As New Solution(v)
            Return s
        End Function

        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Sub Shrink()
            ' move all vectors, except for the best vector (at index 0), halfway to the best vector
            ' compute new objective function values and sort result
            For i As Integer = 1 To Me.AmoebaSize - 1 ' note we don't start at 0
                For j As Integer = 0 To Me.Dimension - 1
                    Me.Solutions(i).Vector(j) = (Me.Solutions(i).Vector(j) + Me.Solutions(0).Vector(j)) / 2.0
                    Me.Solutions(i).Value = AmoebaProgram.ObjectiveFunction(Me.Solutions(i).Vector, Nothing)
                Next j
            Next i
            Array.Sort(Me.Solutions)
        End Sub

        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Sub ReplaceWorst(ByVal newSolution As Solution)
            ' replace the worst solution (at index size-1) with contents of parameter newSolution's vector
            For j As Integer = 0 To Me.Dimension - 1
                Me.Solutions(Me.AmoebaSize - 1).Vector(j) = newSolution.Vector(j)
            Next j
            Me.Solutions(Me.AmoebaSize - 1).Value = newSolution.Value
            Array.Sort(Me.Solutions)
        End Sub

        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Function IsWorseThanAllButWorst(ByVal reflected As Solution) As Boolean
            ' Solve needs to know if the reflected vector is worse (greater value) than every vector in the amoeba, except for the worst vector (highest index)
            For i As Integer = 0 To Me.AmoebaSize - 2 ' not the highest index (worst)
                If reflected.Value <= Me.Solutions(i).Value Then ' reflected is better (smaller value) than at least one of the non-worst solution vectors
                    Return False
                End If
            Next i
            Return True
        End Function

        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Function Solve() As Solution
            Dim t As Integer = 0 ' loop counter
            Do While t < Me.MaxLoop
                t += 1

#If Debugging Then
                Console.Write("At t = " & t & " current best solution = ")
                Console.WriteLine(Me.Solutions(0))
#Else
                If t = 1 OrElse t Mod 10 = 0 Then
                    Console.Write("At t = " & t & " current best solution = ")
                    Console.WriteLine(Me.solutions(0))
                End If
#End If

                Dim centroid As Solution = Me.Centroid ' compute centroid
                Dim reflected As Solution = Me.Reflected(centroid) ' compute reflected

                If reflected.Value < Me.Solutions(0).Value Then ' reflected is better than the current best
                    Dim expanded As Solution = Me.Expanded(reflected, centroid) ' can we do even better??
                    If expanded.Value < Me.Solutions(0).Value Then ' winner! expanded is better than current best
                        Me.ReplaceWorst(expanded) ' replace current worst solution with expanded
                    Else
                        Me.ReplaceWorst(reflected) ' it was worth a try...
                    End If
                    Continue Do
                End If

                If Me.IsWorseThanAllButWorst(reflected) = True Then ' reflected is worse (larger value) than all solution vectors (except possibly the worst one)
                    If reflected.Value <= Me.Solutions(Me.AmoebaSize - 1).Value Then ' reflected is better (smaller) than the curr worst (last index) vector
                        Me.ReplaceWorst(reflected)
                    End If

                    Dim contracted As Solution = Me.Contracted(centroid) ' compute a point 'inside' the amoeba

                    If contracted.Value > Me.Solutions(Me.AmoebaSize - 1).Value Then ' contracted is worse (larger value) than curr worst (last index) solution vector
                        Me.Shrink()
                    Else
                        Me.ReplaceWorst(contracted)
                    End If

                    Continue Do
                End If

                Me.ReplaceWorst(reflected)

                'If IsSorted = false Then
                '  throw New Exception("Unsorted at k = " &k)
                'End If
            Loop ' solve loop

            Return Me.Solutions(0) ' best solution is always at 0
        End Function

#If Debugging Then
        ''' <summary> Checks if sorted. </summary>
        ''' <returns> <c>True</c> if sorted. </returns>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Function IsSorted() As Boolean
            For i As Integer = 0 To Me.Solutions.Length - 2
                If Me.Solutions(i).Value > Me.Solutions(i + 1).Value Then
                    Return False
                End If
            Next i
            Return True
        End Function
#End If

        Public Overrides Function ToString() As String
            Dim s As String = String.Empty
            For i As Integer = 0 To Me.Solutions.Length - 1
                s &= "[" & i & "] " & Me.Solutions(i).ToString & Environment.NewLine
            Next i
            Return s
        End Function

    End Class ' class Amoeba

End Module

