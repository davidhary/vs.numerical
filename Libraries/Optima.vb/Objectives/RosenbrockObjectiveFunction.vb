﻿''' <summary> Rosenbrock objective function. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/20/2014 </para>
''' </remarks>
Public Class RosenbrockObjectiveFunction
    Inherits ObjectiveFunctionBase

    ''' <summary> Evaluates the value for the specified arguments. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="arguments"> The solution values. </param>
    ''' <returns> The objective or nothing if the function failed to evaluate the objective. </returns>
    Public Overrides Function EvaluateObjective(ByVal arguments As IEnumerable(Of Double)) As Double
        If arguments Is Nothing Then Throw New ArgumentNullException(NameOf(arguments))
        Dim x As Double = arguments(0)
        Dim y As Double = arguments(1)
        ' Rosenbrock's function, the function to be minimized
        Return 100.0 * Math.Pow((y - x * x), 2) + Math.Pow(1 - x, 2)
    End Function

End Class
