﻿''' <summary> A Pulse Response function. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/23/2017 </para>
''' </remarks>
Public Class PulseResponseFunction
    Inherits ApproximationFunctionBase

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="observations"> The observations. </param>
    Public Sub New(ByVal observations()() As Double)
        MyBase.New(observations)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="observations"> The observations. </param>
    Public Sub New(ByVal observations As IEnumerable(Of System.Windows.Point))
        MyBase.New(observations)
    End Sub

    ''' <summary> Gets or sets the objective function mode. </summary>
    ''' <value> The objective function mode. </value>
    Public Property ObjectiveFunctionMode As ObjectiveFunctionMode

    ''' <summary> Evaluates the objective value for the specified arguments. </summary>
    ''' <remarks>
    ''' Evaluates the sum of squares of differences between <see cref="FunctionValue">the function
    ''' values</see> and the observations.
    ''' </remarks>
    ''' <param name="arguments"> The solution values. </param>
    ''' <returns> The objective. </returns>
    Public Overrides Function EvaluateObjective(arguments As IEnumerable(Of Double)) As Double
        Select Case Me.ObjectiveFunctionMode
            Case ObjectiveFunctionMode.Correlation
                Me.EvaluateFunctionValues(arguments)
                Return 1 - Me.EvaluateCorrelationCoefficient
            Case Else
                Me.EvaluateFunctionValues(arguments)
                Return Me.EvaluateSquareDeviations
        End Select
    End Function

    ''' <summary> Function Value. </summary>
    ''' <remarks>
    ''' Evaluates the pulse response function<para>
    ''' v(t) = V(1-e(-t/T)) where</para><para>
    ''' V = argument(0) and </para><para>
    ''' -1/T = argument(1) </para>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="arguments">   The solution values. </param>
    ''' <param name="observation"> The observation. </param>
    ''' <returns> The function value at the specified arguments. </returns>
    Public Overrides Function FunctionValue(arguments As IEnumerable(Of Double), ByVal observation As IEnumerable(Of Double)) As Double
        If arguments Is Nothing Then Throw New ArgumentNullException(NameOf(arguments))
        If observation Is Nothing Then Throw New ArgumentNullException(NameOf(observation))
        Return arguments(0) * (1 - Math.Exp(observation(0) * arguments(1)))
    End Function

End Class

''' <summary> Values that represent objective function mode. </summary>
''' <remarks> David, 2020-10-09. </remarks>
Public Enum ObjectiveFunctionMode

    ''' <summary> An enum constant representing the deviations option. </summary>
    Deviations

    ''' <summary> An enum constant representing the correlation option. </summary>
    Correlation
End Enum