Namespace My

    Partial Friend Class MyApplication

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Simplex Console"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Simplex Console"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Numerical.Simplex.Console"

    End Class

End Namespace

