
namespace isr.Numerical.SimplexTests
{

    /// <summary> Test information for the Polynomial Fit Tests. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2/12/2018 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0")]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
    public class RosenbrockFunctionSimplexTestInfo : Core.ApplicationSettingsBase
    {

        #region " SINGLETON "

        /// <summary>
        /// Initializes an instance of the <see cref="T:System.Configuration.ApplicationSettingsBase" />
        /// class to its default state.
        /// </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        private RosenbrockFunctionSimplexTestInfo() : base()
        {
        }

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration($"{typeof(RosenbrockFunctionSimplexTestInfo)} Editor", Get());
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static RosenbrockFunctionSimplexTestInfo _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static RosenbrockFunctionSimplexTestInfo Get()
        {
            if (_Instance is null)
            {
                lock (_SyncLocker)
                    _Instance = (RosenbrockFunctionSimplexTestInfo)Synchronized(new RosenbrockFunctionSimplexTestInfo());
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get
            {
                lock (_SyncLocker)
                    return _Instance is object;
            }
        }

        #endregion

        #region " CONFIGURATION INFORMATION "

        /// <summary> Returns true if test settings exist. </summary>
        /// <value> <c>True</c> if testing settings exit. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool Exists
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("False")]
        public bool Verbose
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool Enabled
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool All
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

        #region " RESENBROCK CONDITIONS "

        /// <summary> Gets or sets the seed. </summary>
        /// <value> The seed. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("1")]
        public int Seed
        {
            get
            {
                return AppSettingGetter(0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the number of iterations. </summary>
        /// <value> The number of iterations. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("50")]
        public int IterationCount
        {
            get
            {
                return AppSettingGetter(0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the objective. </summary>
        /// <value> The objective. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("0.01")]
        public double Objective
        {
            get
            {
                return (double)AppSettingGetter(0m);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the convergence. </summary>
        /// <value> The convergence. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("0.01")]
        public double Convergence
        {
            get
            {
                return (double)AppSettingGetter(0m);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the minimum. </summary>
        /// <value> The minimum value. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("-10")]
        public double Minimum
        {
            get
            {
                return (double)AppSettingGetter(0m);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the maximum. </summary>
        /// <value> The maximum value. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("10")]
        public double Maximum
        {
            get
            {
                return (double)AppSettingGetter(0m);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

    }
}
