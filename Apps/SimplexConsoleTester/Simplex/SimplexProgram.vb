Imports System

' True during development 
#Const Debugging = True

Friend Module SimplexProgram

    ''' <summary> Simplex program. </summary>
    ''' <remarks> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 3/19/2014, 1.0.0.0. based on
    ''' Amoeba Method Optimization using C# by James McCaffrey
    ''' http://msdn.microsoft.com/en-us/magazine/dn201752.aspx </para></remarks>
    Friend Class SimplexProgram

        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        Public Shared Sub Main(ByVal args() As String)
            Try
                Console.WriteLine(vbCrLf & "Begin simplex method optimization demo" & vbCrLf)
                Console.WriteLine("Solving Rosenbrock's function f(x,y) = 100*(y-x^2)^2 + (1-x)^2")
                Console.WriteLine("Function Has a minimum at x = 1.0, y = 1.0 when f = 0.0" & vbCrLf)

                Dim dimension As Integer = 2 ' problem dimension (number of variables to solve for)
                Dim simplexSize As Integer = dimension + 1 ' number of potential solutions in the simplex
                Dim minX As Double = -10.0
                Dim maxX As Double = 10.0
                Dim maxLoop As Integer = 50
                Dim objectivePrecision As Double = 0.01
                Dim valuesPrecision As Double = 0.01

                Console.WriteLine("Creating simplex with size = " & simplexSize)
                Console.WriteLine("Setting max loop = " & maxLoop)
                ' an simplex method optimization solver
                Dim a As New Simplex("Rosenbrock", dimension, minX, maxX, maxLoop, valuesPrecision, objectivePrecision)
                a.Initialize(New RosenbrockObjectiveFunction)

                Console.WriteLine(vbCrLf & "Initial simplex is: " & vbCrLf)
                Console.WriteLine(a.ToString)

                Console.WriteLine(vbCrLf & "Beginning reflect-expand-contract solve loop" & vbCrLf)
                Dim sln As Solution = a.Solve
                Console.WriteLine(vbCrLf & "Solve complete" & vbCrLf)

                Console.WriteLine("Final simplex is: " & vbCrLf)
                Console.WriteLine(a.ToString)

                Console.WriteLine(vbCrLf & "Best solution found after " & a.IterationNumber & " iterations: " & vbCrLf)
                Console.WriteLine(sln.ToString)

                Console.WriteLine(vbCrLf & "End simplex method optimization demo" & vbCrLf)
                Console.ReadLine()
            Catch ex As Exception
                Console.WriteLine(ex.Message)
                Console.ReadLine()
            End Try
        End Sub

    End Class ' program class

End Module

