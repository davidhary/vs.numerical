''' <summary> Defines the process function which is subject to optimization. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public MustInherit Class ObjectiveFunctionBase

    ''' <summary> Evaluates the objective for the specified arguments. </summary>
    ''' <remarks>
    ''' Evaluates the objective for the solution values, which are the arguments of the objective
    ''' function. The objective could be the difference between a desired value and an actual value
    ''' for the potential solution represented by the values. The search attempts to attain a minimum
    ''' of this objective.<para>
    ''' A very high objective value is returned if the function fails to evaluate an objective.
    ''' </para>
    ''' </remarks>
    ''' <param name="arguments"> The solution values. </param>
    ''' <returns> The objective. </returns>
    Public MustOverride Function EvaluateObjective(ByVal arguments As IEnumerable(Of Double)) As Double

    ''' <summary> Evaluates the objective for the specified arguments. </summary>
    ''' <remarks>
    ''' Evaluates the objective for the solution values, which are the arguments of the objective
    ''' function. The objective could be the difference between a desired value and an actual value
    ''' for the potential solution represented by the values. The search attempts to attain a minimum
    ''' of this objective.<para>
    ''' A very high objective value is returned if the function fails to evaluate an objective.
    ''' </para>
    ''' </remarks>
    ''' <param name="arguments"> The solution values. </param>
    ''' <returns> The objective. </returns>
    Public Function EvaluateObjective(ByVal arguments As Double()) As Double
        Return Me.EvaluateObjective(New List(Of Double)(arguments))
    End Function

    ''' <summary> Validates the objective function described by objectiveFunction. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="objectiveFunction"> The objective function. </param>
    ''' <returns> An ObjectiveFunctionBase. </returns>
    Public Shared Function ValidateObjectiveFunction(ByVal objectiveFunction As ObjectiveFunctionBase) As ObjectiveFunctionBase
        If objectiveFunction Is Nothing Then Throw New ArgumentNullException(NameOf(objectiveFunction))
        Return objectiveFunction
    End Function

End Class


