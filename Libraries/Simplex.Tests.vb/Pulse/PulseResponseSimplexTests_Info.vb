''' <summary> Test information for the Polynomial Fit Tests. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2/12/2018 </para></remarks>
<Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),
 Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0"),
 Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>
Friend Class PulseResponseSimplexTestInfo
    Inherits isr.Core.ApplicationSettingsBase

#Region " SINGLETON "

    ''' <summary>
    ''' Initializes an instance of the <see cref="T:System.Configuration.ApplicationSettingsBase" />
    ''' class to its default state.
    ''' </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration($"{GetType(PulseResponseSimplexTestInfo)} Editor", PulseResponseSimplexTestInfo.Get)
    End Sub

    ''' <summary>
    ''' Gets the locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    ''' <value> The sync locker. </value>
    Private Shared Property _SyncLocker As New Object

    ''' <summary> Gets the instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property _Instance As PulseResponseSimplexTestInfo

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As PulseResponseSimplexTestInfo
        If _Instance Is Nothing Then
            SyncLock _SyncLocker
                _Instance = CType(Global.System.Configuration.ApplicationSettingsBase.Synchronized(New PulseResponseSimplexTestInfo()), PulseResponseSimplexTestInfo)
            End SyncLock
        End If
        Return _Instance
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
    Public Shared ReadOnly Property Instantiated() As Boolean
        Get
            SyncLock _SyncLocker
                Return _Instance IsNot Nothing
            End SyncLock
        End Get
    End Property

#End Region

#Region " CONFIGURATION INFORMATION "

    ''' <summary> Returns true if test settings exist. </summary>
    ''' <value> <c>True</c> if testing settings exit. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Property Exists As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Returns true to output test messages at the verbose level. </summary>
    ''' <value> The verbose messaging level. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
    Public Property Verbose As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Returns true to enable this device. </summary>
    ''' <value> The device enable option. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Property Enabled As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets all. </summary>
    ''' <value> all. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Property All As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

#End Region

#Region " EXPONENET SETTINGS "

    ''' <summary> Gets or sets the asymptote. </summary>
    ''' <value> The asymptote. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1.0")>
    Public Property Asymptote As Double
        Get
            Return Me.AppSettingGetter(0D)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the time constant. </summary>
    ''' <value> The time constant. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0.5")>
    Public Property TimeConstant As Double
        Get
            Return Me.AppSettingGetter(0D)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the data points. </summary>
    ''' <value> The data points. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("100")>
    Public Property DataPoints As Integer
        Get
            Return Me.AppSettingGetter(0)
        End Get
        Set(value As Integer)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the sampling interval. </summary>
    ''' <value> The sampling interval. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0.01")>
    Public Property SamplingInterval As Double
        Get
            Return Me.AppSettingGetter(0D)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the relative noise level. </summary>
    ''' <value> The relative noise level. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0.01")>
    Public Property RelativeNoiseLevel As Double
        Get
            Return Me.AppSettingGetter(0D)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

#End Region

#Region " CONVERGENCE CONDITIONS "

    ''' <summary> Gets or sets the objective function mode. </summary>
    ''' <value> The objective function mode. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("Deviations")>
    Public Property ObjectiveFunctionMode As Optima.ObjectiveFunctionMode
        Get
            Return Me.AppSettingEnum(Of Optima.ObjectiveFunctionMode)
        End Get
        Set(value As Optima.ObjectiveFunctionMode)
            Me.AppSettingSetter(value.ToString)
        End Set

    End Property

    ''' <summary> Gets or sets the number of iterations. </summary>
    ''' <value> The number of iterations. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("50")>
    Public Property IterationCount As Integer
        Get
            Return Me.AppSettingGetter(0)
        End Get
        Set(value As Integer)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the relative convergence radius. This is the amount by which the convergence
    ''' radius is compressed to help ensure achieving the convergence accuracy in case the simplex
    ''' converges from outside the expected values.
    ''' </summary>
    ''' <value> The relative convergence radius. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0.05")>
    Public Property RelativeConvergenceRadius As Double
        Get
            Return Me.AppSettingGetter(0D)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Asymptote accuracy. </summary>
    ''' <remarks>
    ''' The Asymptote accuracy must be greater than the relative noise level for the unit test to
    ''' pass because the amplitude may change by the noise level.
    ''' </remarks>
    ''' <value> The amplitude accuracy. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0.025")>
    Public Property AsymptoteAccuracy As Double
        Get
            Return Me.AppSettingGetter(0D)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the time constant accuracy. </summary>
    ''' <value> The time constant accuracy. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0.025")>
    Public Property TimeConstantAccuracy As Double
        Get
            Return Me.AppSettingGetter(0D)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the relative objective limit. </summary>
    ''' <value> The relative objective limit. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1.05")>
    Public Property RelativeObjectiveLimit As Double
        Get
            Return Me.AppSettingGetter(0D)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

#End Region

End Class

