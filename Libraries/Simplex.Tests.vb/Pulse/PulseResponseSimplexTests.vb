Imports isr.Core.RandomExtensions

''' <summary> A pulse response simplex test. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 9/23/2017 </para>
''' </remarks>
<TestClass()>
Public Class PulseResponseSimplexTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Numerical.Optima.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
            Optima.Solution.ResetRandomGenerator(DateTimeOffset.Now.Millisecond)
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
        Assert.IsTrue(PulseResponseSimplexTestInfo.Get.Exists, $"{GetType(PulseResponseSimplexTestInfo)} settings should exist")
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " BUILDERS "

    ''' <summary> Enumerates build time series in this collection. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="sampleInterval"> The sample interval. </param>
    ''' <param name="count">          Number of elements. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process build time series in this collection.
    ''' </returns>
    Private Shared Function BuildTimeSeries(ByVal sampleInterval As Double, ByVal count As Integer) As IEnumerable(Of Double)
        Dim l As New List(Of Double)
        For i As Integer = 0 To count - 1
            l.Add(i * sampleInterval)
        Next
        Return l
    End Function

    ''' <summary> Returns an exponent. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="asymptote">    The asymptote. </param>
    ''' <param name="timeConstant"> The time constant. </param>
    ''' <param name="timeSeries">   The time series. </param>
    ''' <returns> A list of time and amplitude values. </returns>
    Private Shared Function BuildExponent(ByVal asymptote As Double, ByVal timeConstant As Double,
                                          ByVal timeSeries As IEnumerable(Of Double)) As IEnumerable(Of System.Windows.Point)
        Dim l As New List(Of System.Windows.Point)

        For i As Integer = 0 To timeSeries.Count - 1
            Dim t As Double = timeSeries(i)
            Dim TinverseTau As Double = t / timeConstant
            Dim value As Double = asymptote * (1 - Math.Exp(-TinverseTau))

            l.Add(New System.Windows.Point(t, value))
        Next
        Return l
    End Function

    ''' <summary> Enumerates add in this collection. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="timeSeries"> The time series. </param>
    ''' <param name="noise">      The noise. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process add in this collection.
    ''' </returns>
    Private Shared Function Add(ByVal timeSeries As IEnumerable(Of System.Windows.Point),
                                ByVal noise As IEnumerable(Of Double)) As IEnumerable(Of System.Windows.Point)
        Dim l As New List(Of System.Windows.Point)
        For i As Integer = 0 To timeSeries.Count - 1
            l.Add(New System.Windows.Point(timeSeries(i).X, timeSeries(i).Y + noise(i)))
        Next
        Return l
    End Function

    ''' <summary> Builds an exponent using integration. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="asymptote">    The asymptote. </param>
    ''' <param name="timeConstant"> The time constant. </param>
    ''' <param name="count">        Number of. </param>
    ''' <returns> A list of. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Shared Function Integrate(ByVal asymptote As Double, ByVal timeConstant As Double, ByVal count As Integer) As IEnumerable(Of System.Windows.Point)
        Dim l As New List(Of Windows.Point)
        Dim vc As Double = 0
        Dim deltaT As Double = 0.0001
        Dim unused As Double = deltaT * (asymptote - vc) / timeConstant
        For i As Integer = 0 To count - 1
            l.Add(New Windows.Point(CSng(deltaT * (i + 1)), CSng(vc)))
            Dim deltaV As Double = deltaT * (asymptote - vc) / timeConstant
            vc += deltaV
        Next
        Return l
    End Function

#End Region

#Region " TESTS "

    ''' <summary> (Unit Test Method) tests exponent simplex. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    <TestMethod()>
    Public Sub ExponentSimplexTest()

        Dim asymptote As Double = PulseResponseSimplexTestInfo.Get.Asymptote
        Dim dataPoints As Integer = PulseResponseSimplexTestInfo.Get.DataPoints
        Dim timeConstant As Double = PulseResponseSimplexTestInfo.Get.TimeConstant

        Dim rnd As Random = Optima.Solution.Random
        Dim expectedAsymptote As Double = asymptote * rnd.NextUniform(0.9, 1.1)
        Dim expectedTimeConstant As Double = timeConstant * rnd.NextUniform(0.9, 1.1)

        ' model initialization parameters
        Dim asymptoteRange As Double() = New Double() {0.5 * asymptote, 2 * asymptote}
        Dim negativeInverseTauRange As Double() = New Double() {-2 / timeConstant, -0.5 / timeConstant}

        Dim noise As List(Of Double) = New List(Of Double)(rnd.Normal(dataPoints, 0, PulseResponseSimplexTestInfo.Get.RelativeNoiseLevel * asymptote))
        Dim noiseSample As New Core.Engineering.SampleStatistics
        noiseSample.AddValues(noise.ToArray)
        noiseSample.Evaluate()

        Dim timeSeries As List(Of System.Double) = New List(Of System.Double)(PulseResponseSimplexTests.BuildTimeSeries(PulseResponseSimplexTestInfo.Get.SamplingInterval,
                                                                                                                            PulseResponseSimplexTestInfo.Get.DataPoints))
        Dim exponenet As List(Of System.Windows.Point) = New List(Of System.Windows.Point)(PulseResponseSimplexTests.BuildExponent(expectedAsymptote,
                                                                                                                                  expectedTimeConstant, timeSeries))
        Dim testData As List(Of System.Windows.Point) = New List(Of System.Windows.Point)(PulseResponseSimplexTests.Add(exponenet, noise))

        Dim _Model As Optima.PulseResponseFunction = New Optima.PulseResponseFunction(testData) With {
                                                                                    .ObjectiveFunctionMode = PulseResponseSimplexTestInfo.Get.ObjectiveFunctionMode}

        Dim expectedMaximumSSQ As Double = noiseSample.SumSquareDeviations
        Dim objectivePrecision As Double = 0
        Dim asymptoteAccuracy As Double = PulseResponseSimplexTestInfo.Get.AsymptoteAccuracy * expectedAsymptote
        Dim inverseTauAccuracy As Double = PulseResponseSimplexTestInfo.Get.TimeConstantAccuracy / expectedTimeConstant
        Dim testSample As New Core.Engineering.SampleStatistics
        For Each x As System.Windows.Point In testData
            testSample.AddValue(x.Y)
        Next
        Dim exponentSample As New Core.Engineering.SampleStatistics
        For Each x As System.Windows.Point In exponenet
            exponentSample.AddValue(x.Y)
        Next
        exponentSample.CastToArray()
        Dim expectedCorrelationCoeffient As Double = testSample.EvaluateCorrelationCoefficient(exponentSample.ValuesArray)

        If _Model.ObjectiveFunctionMode = Optima.ObjectiveFunctionMode.Deviations Then
            objectivePrecision = PulseResponseSimplexTestInfo.Get.RelativeObjectiveLimit * expectedMaximumSSQ
        ElseIf _Model.ObjectiveFunctionMode = Optima.ObjectiveFunctionMode.Correlation Then
            objectivePrecision = PulseResponseSimplexTestInfo.Get.RelativeObjectiveLimit * (1 - expectedCorrelationCoeffient)
        End If

        Dim simplex As Optima.Simplex
        Dim dimension As Integer = 2
        simplex = New Optima.Simplex("Exponent", dimension,
                                                    New Double() {asymptoteRange(0), negativeInverseTauRange(0)},
                                                    New Double() {asymptoteRange(1), negativeInverseTauRange(1)},
                                                    PulseResponseSimplexTestInfo.Get.IterationCount,
                                                    New Double() {PulseResponseSimplexTestInfo.Get.RelativeConvergenceRadius * asymptoteAccuracy,
                                                                  PulseResponseSimplexTestInfo.Get.RelativeConvergenceRadius * inverseTauAccuracy},
                                                    objectivePrecision)
        simplex.Initialize(Optima.Solution.Random, _Model)
        Dim intialSimplex As String = simplex.ToString
        ' simplex.Solve()
        simplex.Solve(Function()
                          Return simplex.HitCountOverflow OrElse (simplex.Converged AndAlso simplex.Optimized)
                      End Function)

        ' update the model function values
        _Model.EvaluateObjective(simplex.BestSolution.Values)
        Dim actualAsymptote As Double = simplex.BestSolution.Values(0)
        Dim actualTimeConstant As Double = -1 / simplex.BestSolution.Values(1)
        TestInfo.TraceMessage($"Expected  exponent {expectedAsymptote:G4}(1-exp(-t/{expectedTimeConstant:G4})")
        TestInfo.TraceMessage($"Estimated exponent {actualAsymptote:G4}(1-exp(-t/{actualTimeConstant:G4})")
        TestInfo.TraceMessage($"Initial simplex:{Environment.NewLine}{intialSimplex}")
        TestInfo.TraceMessage($" Final simplex:{Environment.NewLine}{simplex}")
        TestInfo.TraceMessage($"Best solution {simplex.BestSolution} found after {simplex.IterationNumber} iterations")
        TestInfo.TraceMessage($"Best solution Objective: {simplex.BestSolution.Objective:G4}; Desired: {objectivePrecision:G4}")
        'TestInfo.TraceMessage("Expected Asymptote = {0:G4}", expectedAsymptote)
        'TestInfo.TraceMessage("Expected Time Constant = {0:G4}", expectedTimeConstant)
        'TestInfo.TraceMessage("Estimated Time Constant = {0:G4}", actualTimeConstant)
        TestInfo.TraceMessage("Correlation Coefficient = {0:G5}", _Model.EvaluateCorrelationCoefficient)
        TestInfo.TraceMessage("Exp.  Corr. Coefficient = {0:G5}", expectedCorrelationCoeffient)
        TestInfo.TraceMessage("         Standard Error = {0:G4}", _Model.EvaluateStandardError(simplex.BestSolution.Objective))
        'TestInfo.TraceMessage("Simulated Noise = {0:G4}", noiseSample.Sigma)
        TestInfo.TraceMessage("Simulated SSQ = {0:G4}", noiseSample.SumSquareDeviations)
        TestInfo.TraceMessage("    Model SSQ = {0:G4}", _Model.EvaluateSquareDeviations)
        TestInfo.TraceMessage($"Converged: {simplex.Converged}")
        TestInfo.TraceMessage($"Optimized: {simplex.Optimized}")

        ' often, the expected solution resides outside the simplex convergence region between the best
        ' and worst simplex nodes. Consequently, the distance between the best and solution and
        ' expected value may exceed the convergence precision. This the expected precision cannot be
        ' used to predict the success of the unit test. 
        Assert.AreEqual(expectedAsymptote, actualAsymptote, PulseResponseSimplexTestInfo.Get.AsymptoteAccuracy * expectedAsymptote,
                        $"Asymptote failed; Converged: {simplex.Converged}; Optimized: {simplex.Optimized}; Expected {expectedAsymptote:G4}(1-exp(-t/{expectedTimeConstant:G4})")
        Assert.AreEqual(expectedTimeConstant, actualTimeConstant, PulseResponseSimplexTestInfo.Get.TimeConstantAccuracy * expectedTimeConstant,
                        $"Time constant failed; Converged: {simplex.Converged}; Optimized: {simplex.Optimized}; Expected {expectedAsymptote:G4}(1-exp(-t/{expectedTimeConstant:G4})")

    End Sub

#End Region

End Class
