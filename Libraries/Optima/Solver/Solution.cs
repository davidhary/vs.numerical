using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Numerical.Optima
{

    /// <summary> A potential Solution. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-03-19, 1.0.0.0. based on Amoeba Method Optimization using C# by James McCaffrey
    /// http://msdn.microsoft.com/en-us/magazine/dn201752.aspx </para>
    /// </remarks>
    public class Solution : IComparable<Solution>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor for a solution using a linear range of values. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="slope">             The slope. </param>
        /// <param name="dimension">         The dimension. </param>
        /// <param name="min">               The minimum of the solution value. </param>
        /// <param name="max">               The maximum of the solution value. </param>
        /// <param name="objectiveFunction"> The objective function. </param>
        public Solution( double slope, int dimension, double min, double max, ObjectiveFunctionBase objectiveFunction ) : this( Linear( slope, dimension, min, max ), objectiveFunction )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="slope">             The slope. </param>
        /// <param name="minima">            The minimum of the solution values. </param>
        /// <param name="maxima">            The maximum of the solution values. </param>
        /// <param name="objectiveFunction"> The objective function. </param>
        public Solution( double slope, IEnumerable<double> minima, IEnumerable<double> maxima, ObjectiveFunctionBase objectiveFunction ) : this( Linear( slope, minima, maxima ), objectiveFunction )
        {
        }

        /// <summary> Constructor for a solution using a linear range of values. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="random">            The random generator for creating random solutions. </param>
        /// <param name="slope">             The slope. </param>
        /// <param name="minima">            The minimum of the solution values. </param>
        /// <param name="maxima">            The maximum of the solution values. </param>
        /// <param name="objectiveFunction"> The objective function. </param>
        public Solution( Random random, double slope, IEnumerable<double> minima, IEnumerable<double> maxima, ObjectiveFunctionBase objectiveFunction ) : this( Linear( random, slope, minima, maxima ), objectiveFunction )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="random">            The random generator for creating random solutions. </param>
        /// <param name="dimension">         The dimension. </param>
        /// <param name="min">               The minimum of the solution value. </param>
        /// <param name="max">               The maximum of the solution value. </param>
        /// <param name="objectiveFunction"> The objective function. </param>
        public Solution( Random random, int dimension, double min, double max, ObjectiveFunctionBase objectiveFunction ) : this( RandomUniform( random, dimension, min, max ), objectiveFunction )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="random">            The random generator for creating random solutions. </param>
        /// <param name="minima">            The minimum of the solution values. </param>
        /// <param name="maxima">            The maximum of the solution values. </param>
        /// <param name="objectiveFunction"> The objective function. </param>
        public Solution( Random random, IEnumerable<double> minima, IEnumerable<double> maxima, ObjectiveFunctionBase objectiveFunction ) : this( RandomUniform( random, minima, maxima ), objectiveFunction )
        {
        }

        /// <summary> Constructor for initialization only. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="values">            The potential solution values. </param>
        /// <param name="objectiveFunction"> The objective function. </param>
        private Solution( IEnumerable<double> values, ObjectiveFunctionBase objectiveFunction ) : this( values, ObjectiveFunctionBase.ValidateObjectiveFunction( objectiveFunction ).EvaluateObjective( values ) )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="values">         The potential solution values. </param>
        /// <param name="objectiveValue"> The objective value. </param>
        public Solution( IEnumerable<double> values, double objectiveValue ) : base()
        {
            this.Initialize( values, objectiveValue );
        }

        /// <summary> Copy constructor. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="solution"> The solution. </param>
        public Solution( Solution solution ) : this( ValidateSolution( solution ).Values(), ValidateSolution( solution ).Objective )
        {
        }

        /// <summary> Validates the solution described by solution. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="solution"> The solution. </param>
        /// <returns> A Solution. </returns>
        public static Solution ValidateSolution( Solution solution )
        {
            if ( solution is null )
                throw new ArgumentNullException( nameof( solution ) );
            return solution;
        }

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values">         The potential solution values. </param>
        /// <param name="objectiveValue"> The objective value. </param>
        private void Initialize( IEnumerable<double> values, double objectiveValue )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            this._Values = new List<double>( values );
            this.Objective = objectiveValue;
        }

        #endregion

        #region " LINEAR VALUES GENERATORS "

        /// <summary> Generate a linear range of values between the minimum and maximum. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="slope">   The slope. </param>
        /// <param name="minimum"> The minimum. </param>
        /// <param name="maximum"> The maximum. </param>
        /// <returns> A Double. </returns>
        private static double Linear( double slope, double minimum, double maximum )
        {
            return minimum + slope * (maximum - minimum);
        }

        /// <summary> Generate a linear range of values between the minimum and maximum. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="slope">  The slope. </param>
        /// <param name="minima"> The minimum. </param>
        /// <param name="maxima"> The maximum. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process uniform in this collection.
        /// </returns>
        private static IEnumerable<double> Linear( double slope, IEnumerable<double> minima, IEnumerable<double> maxima )
        {
            if ( minima is null )
            {
                throw new ArgumentNullException( nameof( minima ) );
            }
            else if ( maxima is null )
            {
                throw new ArgumentNullException( nameof( maxima ) );
            }
            else if ( !minima.Any() )
            {
                throw new ArgumentException( "Array is empty", nameof( minima ) );
            }
            else if ( !maxima.Any() )
            {
                throw new ArgumentException( "Array is empty", nameof( maxima ) );
            }
            else if ( minima.Count() != maxima.Count() )
            {
                throw new ArgumentException( "Length of inputs must be the same", nameof( maxima ) );
            }

            var l = new List<double>();
            for ( int i = 0, loopTo = minima.Count() - 1; i <= loopTo; i++ )
                l.Add( Linear( slope, minima.ElementAtOrDefault( i ), maxima.ElementAtOrDefault( i ) ) );
            return l;
        }

        /// <summary> Generate a linear range of values between the minimum and maximum. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="slope">  The slope. </param>
        /// <param name="length"> The length. </param>
        /// <param name="min">    The minimum of the solution value. </param>
        /// <param name="max">    The maximum of the solution value. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process uniform in this collection.
        /// </returns>
        private static IEnumerable<double> Linear( double slope, int length, double min, double max )
        {
            if ( length <= 0 )
                throw new ArgumentException( "Length must be positive", nameof( length ) );
            var values = new double[length];
            for ( int i = 0, loopTo = length - 1; i <= loopTo; i++ )
                values[i] = Linear( slope, min, max );
            return values;
        }

        /// <summary> Generate a linear range of values between the minimum and maximum. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="random">  The random generator for creating random solutions. </param>
        /// <param name="slope">   The slope. </param>
        /// <param name="minimum"> The minimum. </param>
        /// <param name="maximum"> The maximum. </param>
        /// <returns> A Double. </returns>
        private static double Linear( Random random, double slope, double minimum, double maximum )
        {
            return NextRandomUniform( random, minimum, minimum + slope * (maximum - minimum) );
        }

        /// <summary> Generate a linear range of values between the minimum and maximum. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="random"> The random generator for creating random solutions. </param>
        /// <param name="slope">  The slope. </param>
        /// <param name="minima"> The minimum. </param>
        /// <param name="maxima"> The maximum. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process uniform in this collection.
        /// </returns>
        private static IEnumerable<double> Linear( Random random, double slope, IEnumerable<double> minima, IEnumerable<double> maxima )
        {
            if ( minima is null )
            {
                throw new ArgumentNullException( nameof( minima ) );
            }
            else if ( maxima is null )
            {
                throw new ArgumentNullException( nameof( maxima ) );
            }
            else if ( !minima.Any() )
            {
                throw new ArgumentException( "Array is empty", nameof( minima ) );
            }
            else if ( !maxima.Any() )
            {
                throw new ArgumentException( "Array is empty", nameof( maxima ) );
            }
            else if ( minima.Count() != maxima.Count() )
            {
                throw new ArgumentException( "Length of inputs must be the same", nameof( maxima ) );
            }

            var l = new List<double>();
            for ( int i = 0, loopTo = minima.Count() - 1; i <= loopTo; i++ )
                l.Add( Linear( random, slope, minima.ElementAtOrDefault( i ), maxima.ElementAtOrDefault( i ) ) );
            return l;
        }

        #endregion

        #region " RANDOM VALUES GENERATORS "

        /// <summary> Gets or sets the random. </summary>
        /// <value> The random. </value>
        public static Random Random { get; private set; } = new Random( DateTimeOffset.Now.Millisecond );

        /// <summary> Resets the random generator described by seed. </summary>
        /// <remarks> use from unit tests to get predictable results. </remarks>
        /// <param name="seed"> The seed. </param>
        public static void ResetRandomGenerator( int seed )
        {
            Random = new Random( seed );
        }

        /// <summary> Generates the next random uniform number. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="random"> The random generator for creating random solutions. </param>
        /// <param name="min">    The minimum of the solution value. </param>
        /// <param name="max">    The maximum of the solution value. </param>
        /// <returns> A random value between the minimum and maximum. </returns>
        private static double NextRandomUniform( Random random, double min, double max )
        {
            return (max - min) * random.NextDouble() + min;
        }

        /// <summary> Generates an array of random numbers uniformly distributed. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="random"> The random generator for creating random solutions. </param>
        /// <param name="length"> The length. </param>
        /// <param name="min">    The minimum of the solution value. </param>
        /// <param name="max">    The maximum of the solution value. </param>
        /// <returns> An array of random numbers. </returns>
        private static IEnumerable<double> RandomUniform( Random random, int length, double min, double max )
        {
            if ( length <= 0 )
                throw new ArgumentException( "Length must be positive", nameof( length ) );
            var values = new double[length];
            for ( int i = 0, loopTo = values.Length - 1; i <= loopTo; i++ )
                values[i] = NextRandomUniform( random, min, max );
            return values;
        }

        /// <summary> Generates an array of random numbers uniformly distributed. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="random"> The random generator for creating random solutions. </param>
        /// <param name="minima"> The minimum. </param>
        /// <param name="maxima"> The maximum. </param>
        /// <returns> An array of random numbers. </returns>
        private static IEnumerable<double> RandomUniform( Random random, IEnumerable<double> minima, IEnumerable<double> maxima )
        {
            if ( minima is null )
            {
                throw new ArgumentNullException( nameof( minima ) );
            }
            else if ( maxima is null )
            {
                throw new ArgumentNullException( nameof( maxima ) );
            }
            else if ( !minima.Any() )
            {
                throw new ArgumentException( "Array is empty", nameof( minima ) );
            }
            else if ( !maxima.Any() )
            {
                throw new ArgumentException( "Array is empty", nameof( maxima ) );
            }
            else if ( minima.Count() != maxima.Count() )
            {
                throw new ArgumentException( "Length of inputs must be the same", nameof( maxima ) );
            }

            var l = new List<double>();
            for ( int i = 0, loopTo = minima.Count() - 1; i <= loopTo; i++ )
                l.Add( NextRandomUniform( random, minima.ElementAtOrDefault( i ), maxima.ElementAtOrDefault( i ) ) );
            return l;
        }

        #endregion

        #region " I COMPARABLE "

        /// <summary> Compares two Solution objects to determine their relative ordering. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns>
        /// Negative if 'left' is less than 'right', 0 if they are equal, or positive if it is greater.
        /// </returns>
        public static int Compare( Solution left, Solution right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            if ( right is null )
                throw new ArgumentNullException( nameof( right ) );
            return left.Objective.CompareTo( right.Objective );
            // return object.Equals( left.Objective, right.Objective ) ? 0 : left.Objective.CompareTo( right.Objective ));
        }

        /// <summary>
        /// Compares this Solution object to another to determine their relative ordering based on the
        /// solution <see cref="Objective">Objective</see>.
        /// </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="other"> Another instance to compare. </param>
        /// <returns>
        /// Negative if this object is less than the other, 0 if they are equal, or positive if this is
        /// greater.
        /// </returns>
        public int CompareTo( Solution other )
        {
            return Compare( this, other );
        }

        #endregion

        #region " EQUALS "

        /// <summary> Tests if two Solution objects are considered equal. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> <c>True</c> if the two solutions have the same objective value. </returns>
        public new static bool Equals( object left, object right )
        {
            return Equals( left as Solution, right as Solution );
        }

        /// <summary> Tests if two Solution objects are considered equal. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> <c>True</c> if the two solutions have the same objective value. </returns>
        public static bool Equals( Solution left, Solution right )
        {
            if ( left is null )
            {
                return right is null;
            }
            else if ( right is null )
            {
                return false;
            }
            else
            {
                return left.Objective.Equals( right.Objective );
            }
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="obj"> The object to compare with the current object. </param>
        /// <returns>
        /// true if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, false.
        /// </returns>
        public override bool Equals( object obj )
        {
            return Equals( this, obj as Solution );
        }

        /// <summary> Serves as a hash function for a particular type. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns> A hash code for the current <see cref="T:System.Object" />. </returns>
        public override int GetHashCode()
        {
            return this.Objective.GetHashCode();
        }

        /// <summary> = casting operator. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( Solution left, Solution right )
        {
            return Equals( left, right );
        }

        /// <summary> &lt;&gt; casting operator. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( Solution left, Solution right )
        {
            return !Equals( left, right );
        }

        /// <summary> > casting operator. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator >( Solution left, Solution right )
        {
            return Compare( left, right ) > 0;
        }

        /// <summary> >= casting operator. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator >=( Solution left, Solution right )
        {
            return Compare( left, right ) >= 0;
        }

        /// <summary> &lt; casting operator. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator <( Solution left, Solution right )
        {
            return Compare( left, right ) < 0;
        }

        /// <summary> &lt;= casting operator. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator <=( Solution left, Solution right )
        {
            return Compare( left, right ) <= 0;
        }

        #endregion

        #region " VALUES and OBJECTIVE "

        /// <summary> The values. </summary>
        private IEnumerable<double> _Values;

        /// <summary> Gets the solution values. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns>
        /// A potential solution (array of double) and associated value (so can be sorted against several
        /// potential solutions.
        /// </returns>
        public IEnumerable<double> Values()
        {
            return this._Values;
        }

        /// <summary> Copies from described by values. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="values"> The potential solution values. </param>
        private void CopyFromThis( IEnumerable<double> values )
        {
            this._Values = new List<double>( values );
        }

        /// <summary> Copies from described by values. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="values"> The potential solution values. </param>
        public void CopyFrom( IEnumerable<double> values )
        {
            this.CopyFromThis( values );
        }

        /// <summary> Gets or sets the objective value. </summary>
        /// <value> The objective value. </value>
        public double Objective { get; set; }

        /// <summary> Clips the values. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The potential solution values. </param>
        /// <param name="mode">   The mode. </param>
        /// <param name="minima"> The minimum values. </param>
        /// <param name="maxima"> The maximum values. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process clip in this collection.
        /// </returns>
        public static IEnumerable<double> Clip( IEnumerable<double> values, ClippingModes mode, IEnumerable<double> minima, IEnumerable<double> maxima )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            if ( minima is null )
                throw new ArgumentNullException( nameof( minima ) );
            if ( maxima is null )
                throw new ArgumentNullException( nameof( maxima ) );
            var l = new List<double>();
            for ( int i = 0, loopTo = values.Count() - 1; i <= loopTo; i++ )
            {
                double v = values.ElementAtOrDefault( i );
                var m = mode;
                if ( minima.ElementAtOrDefault( i ) == maxima.ElementAtOrDefault( i ) )
                    m = ClippingModes.Minimum | ClippingModes.Maximum;
                if ( m != ClippingModes.None )
                {
                    if ( (m & ClippingModes.Minimum) == ClippingModes.Minimum && v < minima.ElementAtOrDefault( i ) )
                    {
                        v = minima.ElementAtOrDefault( i );
                    }
                    else if ( (m & ClippingModes.Maximum) == ClippingModes.Maximum && v > maxima.ElementAtOrDefault( i ) )
                    {
                        v = maxima.ElementAtOrDefault( i );
                    }
                }

                l.Add( v );
            }

            return l;
        }

        #endregion

        #region " PRESENTATION "

        /// <summary> Gets or sets the default values format. </summary>
        /// <value> The default values format. </value>
        public static string DefaultValuesFormat { get; set; } = "G5";

        /// <summary> Gets or sets the default objective format. </summary>
        /// <value> The default objective format. </value>
        public static string DefaultObjectiveFormat { get; set; } = "G3";

        /// <summary> Returns a string that represents the current object. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns> A string that represents the current object. </returns>
        public override string ToString()
        {
            return this.ToString( DefaultValuesFormat, DefaultObjectiveFormat );
        }

        /// <summary> Returns a string that represents the current object. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="valuesFormat">    The values format. </param>
        /// <param name="objectiveFormat"> The objective format. </param>
        /// <returns> A string that represents the current object. </returns>
        public string ToString( string valuesFormat, string objectiveFormat )
        {
            var builder = new System.Text.StringBuilder();
            _ = builder.Append( "[ " );
            foreach ( double v in this.Values() )
                _ = builder.Append( $"{v.ToString( valuesFormat )} " );
            _ = builder.Append( "] = " );
            _ = builder.Append( this.Objective.ToString( objectiveFormat ) );
            return builder.ToString();
        }

        #endregion

        #region " DISTANCE "

        /// <summary> Compute the distance between the array and the zero array. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="solution"> The solution. </param>
        /// <returns> The distance between the solution and the zero solution. </returns>
        public static double Hypotenuse( Solution solution )
        {
            if ( solution is null )
                throw new ArgumentNullException( nameof( solution ) );
            return Hypotenuse( solution.Values() );
        }

        /// <summary> Compute the distance between the array and the zero array. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="polygon"> The sides of the polygon. </param>
        /// <returns> The distance between the two arrays. </returns>
        public static double Hypotenuse( IEnumerable<double> polygon )
        {
            if ( polygon is null )
                throw new ArgumentNullException( nameof( polygon ) );
            double d = 0d;
            foreach ( double p in polygon )
                d += Math.Pow( p, 2d );
            d = Math.Sqrt( d );
            return d;
        }

        /// <summary> Compute the distance between two polygons. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="solution"> The solution. </param>
        /// <param name="other">    The other polygon. </param>
        /// <returns> The distance between the two solutions. </returns>
        public static double Hypotenuse( Solution solution, Solution other )
        {
            if ( solution is null )
                throw new ArgumentNullException( nameof( solution ) );
            if ( other is null )
                throw new ArgumentNullException( nameof( other ) );
            return Hypotenuse( solution.Values(), other.Values() );
        }

        /// <summary> Compute the distance between two polygons. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="polygon"> The polygon. </param>
        /// <param name="other">   The other polygon. </param>
        /// <returns> The distance between the two arrays. </returns>
        public static double Hypotenuse( IEnumerable<double> polygon, IEnumerable<double> other )
        {
            if ( polygon is null )
                throw new ArgumentNullException( nameof( polygon ) );
            if ( other is null )
                throw new ArgumentNullException( nameof( other ) );
            double d = 0d;
            for ( int j = 0, loopTo = polygon.Count() - 1; j <= loopTo; j++ )
                d += Math.Pow( polygon.ElementAtOrDefault( j ) - other.ElementAtOrDefault( j ), 2d );
            d = Math.Sqrt( d );
            return d;
        }

        /// <summary>
        /// Determined if this solution converged. That is, its values are within or on a sphere around
        /// the centroid.
        /// </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="convergenceRadii"> The convergence radii setting the convergence sphere. </param>
        /// <param name="centroid">         The centroid. </param>
        /// <returns>
        /// <c>True</c> if this solution converged. That is, its values are within or on a sphere around
        /// the centroid.
        /// </returns>
        public bool Converged( IEnumerable<double> convergenceRadii, Solution centroid )
        {
            if ( convergenceRadii is null )
                throw new ArgumentNullException( nameof( convergenceRadii ) );
            if ( centroid is null )
                throw new ArgumentNullException( nameof( centroid ) );
            var r = Radius( this, centroid );
            bool affirmative = true;
            for ( int i = 0, loopTo = r.Length - 1; i <= loopTo; i++ )
            {
                // uses smaller or equal allows to converge to zero values.
                affirmative = affirmative && Math.Abs( r[i] ) <= convergenceRadii.ElementAtOrDefault( i );
                if ( !affirmative )
                {
                    break;
                }
            }

            return affirmative;
        }

        /// <summary> The radii of the solutions from the centroid. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="solution"> The solution. </param>
        /// <param name="centroid"> The centroid. </param>
        /// <returns> The radiuses between the centroid and the solutions. </returns>
        public static double[] Radius( Solution solution, Solution centroid )
        {
            if ( centroid is null )
                throw new ArgumentNullException( nameof( centroid ) );
            if ( solution is null )
                throw new ArgumentNullException( nameof( solution ) );
            var r = new double[(solution.Values().Count())];
            for ( int i = 0, loopTo = r.Length - 1; i <= loopTo; i++ )
                r[i] = solution.Values().ElementAtOrDefault( i ) - centroid.Values().ElementAtOrDefault( i );
            return r;
        }

        /// <summary> The radiuses of the solutions from the centroid. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="solutions"> The solutions. </param>
        /// <param name="centroid">  The centroid. </param>
        /// <returns> The radiuses between the centroid and the solutions. </returns>
        public static double[][] Radii( Solution[] solutions, Solution centroid )
        {
            if ( centroid is null )
                throw new ArgumentNullException( nameof( centroid ) );
            if ( solutions is null )
                throw new ArgumentNullException( nameof( solutions ) );
            var r = new double[solutions.Length][];
            for ( int i = 0, loopTo = solutions.GetLength( 0 ) - 1; i <= loopTo; i++ )
            {
                r[i] = new double[(solutions[i].Values().Count())];
                for ( int j = 0, loopTo1 = solutions[i].Values().Count() - 1; j <= loopTo1; j++ )
                    r[i][j] = solutions[i].Values().ElementAtOrDefault( j ) - centroid.Values().ElementAtOrDefault( j );
            }

            return r;
        }

        /// <summary> The distances of the solutions from the centroid. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="solutions"> The solutions. </param>
        /// <param name="centroid">  The centroid. </param>
        /// <returns> The distances between the centroid and the solutions. </returns>
        public static double[] Hypoteni( Solution[] solutions, Solution centroid )
        {
            if ( centroid is null )
                throw new ArgumentNullException( nameof( centroid ) );
            if ( solutions is null )
                throw new ArgumentNullException( nameof( solutions ) );
            var r = new double[solutions.Length];
            for ( int i = 0, loopTo = r.Length - 1; i <= loopTo; i++ )
                r[i] = Hypotenuse( solutions[i], centroid );
            return r;
        }

        #endregion

    }
}
