using System;
using System.Collections.Generic;

namespace isr.Numerical.Optima
{
    /// <summary> Defines the process function which is subject to optimization. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public abstract class ObjectiveFunctionBase
    {

        /// <summary> Evaluates the objective for the specified arguments. </summary>
        /// <remarks>
        /// Evaluates the objective for the solution values, which are the arguments of the objective
        /// function. The objective could be the difference between a desired value and an actual value
        /// for the potential solution represented by the values. The search attempts to attain a minimum
        /// of this objective.<para>
        /// A very high objective value is returned if the function fails to evaluate an objective.
        /// </para>
        /// </remarks>
        /// <param name="arguments"> The solution values. </param>
        /// <returns> The objective. </returns>
        public abstract double EvaluateObjective( IEnumerable<double> arguments );

        /// <summary>   Evaluates the objective for the specified arguments. </summary>
        /// <remarks>
        /// Evaluates the objective for the solution values, which are the arguments of the objective
        /// function. The objective could be the difference between a desired value and an actual value
        /// for the potential solution represented by the values. The search attempts to attain a minimum
        /// of this objective.<para>
        /// A very high objective value is returned if the function fails to evaluate an objective.
        /// </para>
        /// </remarks>
        /// <param name="arguments">    The solution values. </param>
        /// <returns>   The objective. </returns>
        public double EvaluateObjective( double[] arguments )
        {
            return this.EvaluateObjective( new List<double>( arguments ) );
        }

        /// <summary> Validates the objective function described by objectiveFunction. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="objectiveFunction"> The objective function. </param>
        /// <returns> An ObjectiveFunctionBase. </returns>
        public static ObjectiveFunctionBase ValidateObjectiveFunction( ObjectiveFunctionBase objectiveFunction )
        {
            if ( objectiveFunction is null )
                throw new ArgumentNullException( nameof( objectiveFunction ) );
            return objectiveFunction;
        }
    }
}
