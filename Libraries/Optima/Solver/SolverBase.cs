using System;
using System.Collections.Generic;
using System.Linq;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Numerical.Optima
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> Solver base. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-03-22 </para>
    /// </remarks>
    public abstract class SolverBase
    {

        #region " CONSTRUCTION "

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="identity">          The identity. </param>
        /// <param name="dimension">         The dimension or the size of the solution values. </param>
        /// <param name="maximumIterations"> The maximum iterations. </param>
        /// <param name="convergenceRadii">  The convergence radii. </param>
        /// <param name="objectiveLimit">    The objective limit. The solution is said to be optimized if
        /// the objective is lower than the objective limen. </param>
        protected SolverBase( string identity, int dimension, int maximumIterations, double[] convergenceRadii, double objectiveLimit ) : base()
        {
            this.Identity = identity;
            this._Solutions = new List<Solution>();
            this.Dimension = dimension;
            this.MaximumIterations = maximumIterations;
            this.ConvergenceRadii = new List<double>( convergenceRadii );
            this.ObjectiveLimit = objectiveLimit;

            // establish a distance for determining convergence of values. This is the Hypotenuse.
            this.ConvergenceRadius = Solution.Hypotenuse( this.ConvergenceRadii );
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="identity">          The identity. </param>
        /// <param name="dimension">         The dimension or the size of the solution values. </param>
        /// <param name="maximumIterations"> The maximum iterations. </param>
        /// <param name="convergenceRadii">  The convergence radii. </param>
        /// <param name="objectiveLimit">    The objective limit. The solution is said to be optimized if
        /// the objective is lower than the objective limen. </param>
        protected SolverBase( string identity, int dimension, int maximumIterations, IEnumerable<double> convergenceRadii, double objectiveLimit ) : base()
        {
            this.Identity = identity;
            this._Solutions = new List<Solution>();
            this.Dimension = dimension;
            this.MaximumIterations = maximumIterations;
            this.ConvergenceRadii = new List<double>( convergenceRadii );
            this.ObjectiveLimit = objectiveLimit;

            // establish a distance for determining convergence of values. This is the Hypotenuse.
            this.ConvergenceRadius = Solution.Hypotenuse( this.ConvergenceRadii );
        }

        #endregion

        #region " SOLUTION "

        /// <summary> Gets the identity. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The identity. </value>
        public string Identity { get; set; }

        /// <summary> The dimension or the size of the solution values. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The dimension. </value>
        public int Dimension { get; set; }

        /// <summary> Gets the objective function. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The objective function. </value>
        public ObjectiveFunctionBase ObjectiveFunction { get; set; }

        #endregion

        #region " SOLVER "

        /// <summary> Gets the initial simplex. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The initial simplex. </value>
        public string InitialSimplex { get; private set; }

        /// <summary> Gets the initial objective. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The initial objective. </value>
        public double InitialObjective { get; private set; }

        /// <summary> Solves one step. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        public abstract void SolveStep();

        /// <summary> Optimizes the solution objective. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="checkCompletion"> The check completion function. </param>
        /// <returns> The best solution. </returns>
        public Solution Solve( Func<bool> checkCompletion )
        {
            if ( checkCompletion is null )
                throw new ArgumentNullException( nameof( checkCompletion ) );
            this.IterationNumber = 0;
            this.InitialSimplex = this.ToString();
            this.InitialObjective = this.BestSolution.Objective;
            while ( !checkCompletion.Invoke() )
            {
                this.IterationNumber += 1;
                if ( this.DebugEnabled )
                {
                    if ( true )
                    {
                        if ( this.IterationNumber == 1 || this.IterationNumber % 10 == 0 )
                        {
                            Console.Write( $"At t = {this.IterationNumber} current best solution = " );
                            Console.WriteLine( this.BestSolution );
                        }
                    }
                    else
                    {
#pragma warning disable CS0162 // Unreachable code detected
                        Console.Write( $"At t = {this.IterationNumber} current best solution = " );
                        Console.WriteLine( this.BestSolution );
#pragma warning restore CS0162 // Unreachable code detected
                    }
                }

                // run one step.            
                this.SolveStep();
            }

            return this.BestSolution;
        }

        /// <summary>   Gets or sets a value indicating whether the debug is enabled. </summary>
        /// <value> True if debug enabled, false if not. </value>
        public bool DebugEnabled { get; set; }

        /// <summary> Optimizes the solution objective. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns> The best solution. </returns>
        public Solution Solve()
        {
            return this.Solve( () => this.HitCountOverflow() || this.Converged() || this.Optimized() );
        }

        #endregion

        #region " CONVERGENCE "

        /// <summary> Gets the centroid. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <returns> A Solution. </returns>
        protected abstract Solution Centroid();

        /// <summary> Gets the best solution. </summary>
        /// <value> The solution. </value>
        public virtual Solution BestSolution => this.Solutions[0];

        /// <summary> Gets the worst solution. </summary>
        /// <value> The worst solution. </value>
        public Solution WorstSolution => this.Solutions[this.Dimension];

        /// <summary> The solutions. </summary>
        private List<Solution> _Solutions;

        /// <summary> Gets or sets the solutions. </summary>
        /// <value> The solutions. </value>
        public IList<Solution> Solutions
        {
            get => this._Solutions;

            protected set {
                this._Solutions = new List<Solution>( value );
                this.Sort();
            }
        }

        /// <summary> Clears the solutions. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        public void ClearSolutions()
        {
            this._Solutions.Clear();
        }

        /// <summary> Adds a solution. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="solution"> The solution. </param>
        public virtual void AddSolution( Solution solution )
        {
            if ( solution is null )
                throw new ArgumentNullException( nameof( solution ) );
            this._Solutions.Add( solution );
            this.Sort();
        }

        /// <summary> Sorts the solutions. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        protected void Sort()
        {
            this._Solutions.Sort();
        }

        /// <summary> Gets or sets the iteration number. </summary>
        /// <value> The iteration number. </value>
        public int IterationNumber { get; set; }

        /// <summary>
        /// The objective limit. The solution is said to be optimized if the objective is lower than the
        /// objective limen.
        /// </summary>
        /// <value> The objective limit. </value>
        public double ObjectiveLimit { get; set; }

        /// <summary> The maximum iterations. </summary>
        /// <value> The maximum iterations. </value>
        protected int MaximumIterations { get; set; }

        /// <summary>
        /// The convergence radius. The Hypotenuse of the  <see cref="ConvergenceSphere()"/>.
        /// </summary>
        /// <value> The convergence radius. </value>
        protected double ConvergenceRadius { get; set; }

        /// <summary> The convergence radii. </summary>
        /// <value> The convergence radii. </value>
        public virtual IList<double> ConvergenceRadii { get; private set; }

        /// <summary> Gets or sets the sentinel indicating if convergence sphere is relative. </summary>
        /// <value> <c>True</c> if the convergence sphere is relative to the centroid. </value>
        public bool UsesRelativeConvergenceSphere { get; set; }

        /// <summary> The convergence radii. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns> The radii establishing the convergence sphere. </returns>
        public IEnumerable<double> ConvergenceSphere()
        {
            return this.UsesRelativeConvergenceSphere ? this.ConvergenceSphere( this.Centroid() ) : this.ConvergenceRadii;
        }

        /// <summary>
        /// Returns the radii establishing the convergence sphere for relative convergence.
        /// </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="centroid"> The centroid. </param>
        /// <returns> The radii establishing the convergence sphere. </returns>
        public IEnumerable<double> ConvergenceSphere( Solution centroid )
        {
            if ( centroid is null )
                throw new ArgumentNullException( nameof( centroid ) );
            var values = new List<double>();
            for ( int i = 0, loopTo = this.ConvergenceRadii.Count - 1; i <= loopTo; i++ )
                values.Add( this.ConvergenceRadii[i] * centroid.Values().ElementAtOrDefault( i ) );
            return values;
        }

        /// <summary> Hit count overflow. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns> <c>True</c> if the number of iterations hit the maximum iteration allowed. </returns>
        public bool HitCountOverflow()
        {
            return this.IterationNumber >= this.MaximumIterations;
        }

        /// <summary> Checks if the best solution is within the objective. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns> <c>True</c> if the best solution reached the objective. </returns>
        public bool Optimized()
        {
            return Math.Abs( this.BestSolution.Objective ) < this.ObjectiveLimit;
        }

        /// <summary>
        /// Checks if the solution converged. Namely, if the values are close to each other.
        /// </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns> <c>True</c> if the solution values are close to each other. </returns>
        public bool Converged()
        {
            var centroid = this.Centroid();
            return this.Converged( this.ConvergenceSphere( centroid ), centroid );
        }

        /// <summary>
        /// Checks if the solution converged. Namely, if values are within or on a convergence sphere
        /// around the centroid.
        /// </summary>
        /// <remarks>
        /// Checks if the solutions are within or on a convergence sphere around the centroid.
        /// </remarks>
        /// <param name="convergenceRadii"> The values previsions. </param>
        /// <returns>
        /// <c>True</c> if the solutions are all within or on a convergence sphere around the centroid.
        /// </returns>
        public bool Converged( IEnumerable<double> convergenceRadii )
        {
            return this.Converged( convergenceRadii, this.Centroid() );
        }

        /// <summary>
        /// Checks if the solution converged. Namely, if values are within or on a convergence sphere
        /// around the centroid.
        /// </summary>
        /// <remarks>
        /// Checks if the solutions are within or on a convergence sphere around the centroid.
        /// </remarks>
        /// <param name="convergenceRadii"> The values previsions. </param>
        /// <param name="centroid">         The centroid; center of current simplex. </param>
        /// <returns>
        /// <c>True</c> if the solutions are all within or on a convergence sphere around the centroid.
        /// </returns>
        public bool Converged( IEnumerable<double> convergenceRadii, Solution centroid )
        {
            bool affirmative = true;
            foreach ( Solution solution in this.Solutions )
            {
                affirmative = affirmative && solution.Converged( convergenceRadii, centroid );
                if ( !affirmative )
                {
                    break;
                }
            }

            return affirmative;
        }

        #endregion

        #region " PRESENTATION "

        /// <summary> Gets or sets the default format. </summary>
        /// <value> The default format. </value>
        public static string DefaultFormat { get; set; } = "G5";

        /// <summary> Returns a string that represents the current object. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns> A string that represents the current object. </returns>
        public override string ToString()
        {
            return this.ToString( Solution.DefaultValuesFormat, Solution.DefaultObjectiveFormat );
        }

        /// <summary> Returns a string that represents the current object. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="valuesFormat">    The values format. </param>
        /// <param name="objectiveFormat"> The objective format. </param>
        /// <returns> A string that represents the current object. </returns>
        public string ToString( string valuesFormat, string objectiveFormat )
        {
            var builder = new System.Text.StringBuilder();
            foreach ( Solution s in this.Solutions )
                _ = builder.AppendLine( $"{s.ToString( valuesFormat, objectiveFormat )}" );
            return builder.ToString().TrimEnd( Environment.NewLine.ToCharArray() );
        }

        /// <summary> Returns a string summarizing the simplex state. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns> A String. </returns>
        public string ToSummary()
        {
            return this.ToSummary( Solution.DefaultValuesFormat, Solution.DefaultObjectiveFormat );
        }

        /// <summary> Returns a string summarizing the simplex state. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="valuesFormat">    The values format. </param>
        /// <param name="objectiveFormat"> The objective format. </param>
        /// <returns> A String. </returns>
        public string ToSummary( string valuesFormat, string objectiveFormat )
        {
            var builder = new System.Text.StringBuilder();
            _ = builder.Append( $"Simplex {this.Identity}" );
            _ = builder.Append( $"; Solution: {this.BestSolution.ToString( valuesFormat, objectiveFormat )}" );
            _ = builder.Append( $"; Iterations: {this.IterationNumber}" );
            _ = builder.Append( $"; Converged: {this.Converged()}" );
            _ = builder.Append( $"; Optimized: {this.Optimized()}" );
            return builder.ToString().TrimEnd( Environment.NewLine.ToCharArray() );
        }

        /// <summary> Converts this object to a detailed summary. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns> The given data converted to a String. </returns>
        public string ToDetailedSummary()
        {
            return this.ToDetailedSummary( Solution.DefaultValuesFormat, Solution.DefaultObjectiveFormat );
        }

        /// <summary> Converts this object to a detailed summary. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="valuesFormat">    The values format. </param>
        /// <param name="objectiveFormat"> The objective format. </param>
        /// <returns> The given data converted to a String. </returns>
        public string ToDetailedSummary( string valuesFormat, string objectiveFormat )
        {
            var builder = new System.Text.StringBuilder();
            _ = builder.AppendLine( this.ToSummary( valuesFormat, objectiveFormat ) );
            _ = builder.AppendLine( $"; Initial simplex: {this.InitialSimplex}" );
            _ = builder.AppendLine( $"; Final simplex: {this}" );
            _ = builder.AppendLine( $"; Desired Objective: {this.ObjectiveLimit.ToString( objectiveFormat )}" );
            return builder.ToString().TrimEnd( Environment.NewLine.ToCharArray() );
        }

        #endregion

    }
}
