# Changelog
All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [3.0.8067] - 2022-02-01
* Change target framework from 4.0 to 4.72.

## [3.0.6795] - 2018-08-09
* Created from Algorithms Optima.

## [3.0.6667] - 2018-01-17
* Uses upgraded core libraries.

## [2.1.6667] - 2018-04-03
* 2018 release.

## [1.0.5191] - 2014-03-18
* Created.

\(C\) 2014 Integrated Scientific Resources, Inc. All rights reserved.

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
```
[3.0.8067]: https://bitbucket.org/davidhary/vs.numerical.git