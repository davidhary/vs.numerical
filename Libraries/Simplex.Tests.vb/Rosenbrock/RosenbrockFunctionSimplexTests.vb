Imports isr.Numerical.Optima

''' <summary>
''' This is a test class for RosenbrockFunctionSimplexTest and is intended to contain all
''' RosenbrockFunctionSimplexTest Unit Tests.
''' </summary>
''' <remarks> David, 2020-10-09. </remarks>
<TestClass()>
Public Class RosenbrockFunctionSimplexTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Numerical.Optima.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
            Solution.ResetRandomGenerator(RosenbrockFunctionSimplexTestInfo.Get.Seed)
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
        Assert.IsTrue(RosenbrockFunctionSimplexTestInfo.Get.Exists, $"{GetType(RosenbrockFunctionSimplexTestInfo)} settings should exist")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " SOLVER "

    ''' <summary> A test for Solver. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    <TestMethod()>
    Public Sub RosenbrockFunctionMinimization()

        TestInfo.TraceMessage("Begin simplex method optimization demo")
        TestInfo.TraceMessage("Solving Rosenbrock function f(x,y) = 100*(y-x^2)^2 + (1-x)^2")
        TestInfo.TraceMessage("Function Has a minimum at x = 1.0, y = 1.0 when f = 0.0")

        Dim dimension As Integer = 2 ' problem dimension (number of variables to solve for)
        Dim minX As Double = RosenbrockFunctionSimplexTestInfo.Get.Minimum
        Dim maxX As Double = RosenbrockFunctionSimplexTestInfo.Get.Maximum
        Dim maxLoop As Integer = RosenbrockFunctionSimplexTestInfo.Get.IterationCount
        Dim objectivePrecision As Double = RosenbrockFunctionSimplexTestInfo.Get.Objective
        Dim valuesPrecision As Double = RosenbrockFunctionSimplexTestInfo.Get.Convergence

        ' an simplex method optimization solver
        Dim simplex As New Optima.Simplex("Rosenbrock", dimension, minX, maxX, maxLoop, valuesPrecision, objectivePrecision)
        simplex.Initialize(Solution.Random, New RosenbrockObjectiveFunction)

        TestInfo.TraceMessage("Initial simplex is: ")
        TestInfo.TraceMessage(simplex.ToString)

        Dim sln As Solution = simplex.Solve

        TestInfo.TraceMessage("Final simplex is: ")
        TestInfo.TraceMessage(simplex.ToString)

        TestInfo.TraceMessage("Best solution found after {0} iterations: ", simplex.IterationNumber)
        TestInfo.TraceMessage(sln.ToString)

        TestInfo.TraceMessage("End simplex method optimization test")

        Dim actual As Double = sln.Objective
        Dim expected As Double = 0
        Assert.AreEqual(expected, actual, objectivePrecision)

    End Sub
#End Region

End Class
