using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Numerical.Optima
{

    /// <summary> Approximation function base. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-03-22 </para>
    /// </remarks>
    public abstract class ApproximationFunctionBase : ObjectiveFunctionBase
    {

        #region " EVALUATE "

        /// <summary> Specialized constructor for use only by derived classes. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="observations"> The observations. </param>
        protected ApproximationFunctionBase( double[][] observations ) : base()
        {
            this.PopulateObservations( observations );
        }

        /// <summary> Specialized constructor for use only by derived classes. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="observations"> The observations. </param>
        protected ApproximationFunctionBase( IEnumerable<System.Windows.Point> observations ) : base()
        {
            this.PopulateObservations( observations );
        }

        /// <summary> The observations. </summary>
        private double[][] _Observations;

        /// <summary> Gets the observations. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns> The observation values. </returns>
        protected double[][] Observations()
        {
            return this._Observations;
        }

        /// <summary> Populate observations. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="observations"> The observations. </param>
        private void PopulateObservations( double[][] observations )
        {
            if ( observations is null )
                throw new ArgumentNullException( nameof( observations ) );
            this._Observations = new double[(observations.GetLength( 0 ))][];
            for ( int i = 0, loopTo = observations.GetLength( 0 ) - 1; i <= loopTo; i++ )
            {
                int len = observations[i].Length;
                this._Observations[i] = new double[len];
                Array.Copy( observations[i], this._Observations[i], len );
            }
        }

        /// <summary> Populate observations. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="timeSeries"> The time series. </param>
        private void PopulateObservations( IEnumerable<System.Windows.Point> timeSeries )
        {
            if ( timeSeries is null )
            {
                throw new ArgumentNullException( nameof( timeSeries ) );
            }
            else if ( timeSeries.Count() == 0 )
            {
                throw new ArgumentException( "Time series has no values", nameof( timeSeries ) );
            }
            else if ( timeSeries.Count() <= 10 )
            {
                throw new ArgumentException( "Time series has less than 10 values", nameof( timeSeries ) );
            }

            this._Observations = new double[(timeSeries.Count())][];
            for ( int i = 0, loopTo = this.Observations().GetLength( 0 ) - 1; i <= loopTo; i++ )
                this._Observations[i] = new double[2] { timeSeries.ElementAtOrDefault( i ).X, timeSeries.ElementAtOrDefault( i ).Y };
        }

        /// <summary> Evaluate function values. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="arguments"> The arguments. </param>
        public void EvaluateFunctionValues( IEnumerable<double> arguments )
        {
            if ( arguments is null )
                throw new ArgumentNullException( nameof( arguments ) );
            this._Dimension = arguments.Count();
            int len = this.Observations().GetLength( 0 );
            var v = new double[len];
            for ( int i = 0, loopTo = this.Observations().GetLength( 0 ) - 1; i <= loopTo; i++ )
                v[i] = this.FunctionValue( arguments, this.Observations()[i] );
            this._FunctionValues = new List<double>( v );
        }

        /// <summary> Function Value. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="arguments">   The arguments. </param>
        /// <param name="observation"> The observation. </param>
        /// <returns> The function value corresponding to the specific observation. </returns>
        public abstract double FunctionValue( IEnumerable<double> arguments, IEnumerable<double> observation );

        /// <summary> The function values. </summary>
        private IEnumerable<double> _FunctionValues;

        /// <summary> Function Values. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns> The function values for the last iteration. </returns>
        public IEnumerable<double> FunctionValues()
        {
            return this._FunctionValues;
        }

        /// <summary> The dimension=the number of function variables, or arguments. </summary>
        private int _Dimension;

        #endregion

        #region " GOODNESS OF FIT "

        /// <summary> Evaluates average function. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns> The average. </returns>
        private double EvaluateAverageFunction()
        {
            double avg = 0d;
            foreach ( var v in this._FunctionValues )
                avg += v;
            return avg / this._FunctionValues.Count();
        }

        /// <summary> Evaluates average observation. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns> The average. </returns>
        private double EvaluateAverageObservation()
        {
            double avg = 0d;
            int len = this.Observations().GetLength( 0 );
            for ( int i = 0, loopTo = len - 1; i <= loopTo; i++ )
                avg += this.Observations()[i][1];
            return avg / len;
        }

        /// <summary> Evaluates correlation coefficient. </summary>
        /// <remarks> Assumes that the function values already exist. </remarks>
        /// <returns> The correlation coefficient or coefficient of multiple determination. </returns>
        public double EvaluateCorrelationCoefficient()
        {
            double favg = this.EvaluateAverageFunction();
            double oavg = this.EvaluateAverageObservation();
            double fssq = 0d;
            double ossq = 0d;
            double ofssq = 0d;
            for ( int i = 0, loopTo = this._FunctionValues.Count() - 1; i <= loopTo; i++ )
            {
                double f = this._FunctionValues.ElementAtOrDefault( i ) - favg;
                double o = this.Observations()[i][1] - oavg;
                fssq += f * f;
                ossq += o * o;
                ofssq += o * f;
            }

            return ofssq / (Math.Sqrt( fssq ) * Math.Sqrt( ossq ));
        }

        /// <summary>
        /// Evaluates the sum of squares of deviations between observations and function values.
        /// </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns> The sum of squares of deviations between observations and function values. </returns>
        public double EvaluateSquareDeviations()
        {
            double ssq = 0d;
            for ( int i = 0, loopTo = this._FunctionValues.Count() - 1; i <= loopTo; i++ )
            {
                double v = this.Observations()[i][1] - this._FunctionValues.ElementAtOrDefault( i );
                ssq += v * v;
            }

            return ssq;
        }

        /// <summary> Evaluates standard error. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="objectiveValue"> The objective value == sum of square of deviations. </param>
        /// <returns> The standard error of the estimate. </returns>
        public double EvaluateStandardError( double objectiveValue )
        {
            return Math.Sqrt( objectiveValue / (this._FunctionValues.Count() - this._Dimension) );
        }

        #endregion

    }
}
