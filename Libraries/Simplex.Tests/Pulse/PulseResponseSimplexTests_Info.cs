
namespace isr.Numerical.SimplexTests
{

    /// <summary> Test information for the Polynomial Fit Tests. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2/12/2018 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0")]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
    internal class PulseResponseSimplexTestInfo : Core.ApplicationSettingsBase
    {

        #region " SINGLETON "

        /// <summary>
        /// Initializes an instance of the <see cref="T:System.Configuration.ApplicationSettingsBase" />
        /// class to its default state.
        /// </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        private PulseResponseSimplexTestInfo() : base()
        {
        }

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration($"{typeof(PulseResponseSimplexTestInfo)} Editor", Get());
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static PulseResponseSimplexTestInfo _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static PulseResponseSimplexTestInfo Get()
        {
            if (_Instance is null)
            {
                lock (_SyncLocker)
                    _Instance = (PulseResponseSimplexTestInfo)Synchronized(new PulseResponseSimplexTestInfo());
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get
            {
                lock (_SyncLocker)
                    return _Instance is object;
            }
        }

        #endregion

        #region " CONFIGURATION INFORMATION "

        /// <summary> Returns true if test settings exist. </summary>
        /// <value> <c>True</c> if testing settings exit. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool Exists
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("False")]
        public bool Verbose
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool Enabled
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool All
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

        #region " EXPONENET SETTINGS "

        /// <summary> Gets or sets the asymptote. </summary>
        /// <value> The asymptote. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("1.0")]
        public double Asymptote
        {
            get
            {
                return (double)AppSettingGetter(0m);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the time constant. </summary>
        /// <value> The time constant. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("0.5")]
        public double TimeConstant
        {
            get
            {
                return (double)AppSettingGetter(0m);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the data points. </summary>
        /// <value> The data points. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("100")]
        public int DataPoints
        {
            get
            {
                return AppSettingGetter(0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the sampling interval. </summary>
        /// <value> The sampling interval. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("0.01")]
        public double SamplingInterval
        {
            get
            {
                return (double)AppSettingGetter(0m);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the relative noise level. </summary>
        /// <value> The relative noise level. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("0.01")]
        public double RelativeNoiseLevel
        {
            get
            {
                return (double)AppSettingGetter(0m);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

        #region " CONVERGENCE CONDITIONS "

        /// <summary> Gets or sets the objective function mode. </summary>
        /// <value> The objective function mode. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("Deviations")]
        public Optima.ObjectiveFunctionMode ObjectiveFunctionMode
        {
            get
            {
                return AppSettingEnum<Optima.ObjectiveFunctionMode>();
            }

            set
            {
                AppSettingSetter(value.ToString());
            }
        }

        /// <summary> Gets or sets the number of iterations. </summary>
        /// <value> The number of iterations. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("50")]
        public int IterationCount
        {
            get
            {
                return AppSettingGetter(0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary>
        /// Gets or sets the relative convergence radius. This is the amount by which the convergence
        /// radius is compressed to help ensure achieving the convergence accuracy in case the simplex
        /// converges from outside the expected values.
        /// </summary>
        /// <value> The relative convergence radius. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("0.05")]
        public double RelativeConvergenceRadius
        {
            get
            {
                return (double)AppSettingGetter(0m);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the Asymptote accuracy. </summary>
        /// <remarks>
        /// The Asymptote accuracy must be greater than the relative noise level for the unit test to
        /// pass because the amplitude may change by the noise level.
        /// </remarks>
        /// <value> The amplitude accuracy. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("0.025")]
        public double AsymptoteAccuracy
        {
            get
            {
                return (double)AppSettingGetter(0m);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the time constant accuracy. </summary>
        /// <value> The time constant accuracy. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("0.025")]
        public double TimeConstantAccuracy
        {
            get
            {
                return (double)AppSettingGetter(0m);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the relative objective limit. </summary>
        /// <value> The relative objective limit. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("1.05")]
        public double RelativeObjectiveLimit
        {
            get
            {
                return (double)AppSettingGetter(0m);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

    }
}
