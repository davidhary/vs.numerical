using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using isr.Core.RandomExtensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace isr.Numerical.SimplexTests
{

    /// <summary> A pulse response simplex test. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 9/23/2017 </para>
    /// </remarks>
    [TestClass()]
    public class PulseResponseSimplexTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        [ClassInitialize()]
        [CLSCompliant(false)]
        public static void MyClassInitialize(TestContext testContext)
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue(TestInfo.TraceMessagesQueueListener);
                TestInfo.AddTraceMessagesQueue(Optima.My.MyLibrary.UnpublishedTraceMessages);
                TestInfo.InitializeTraceListener();
                Optima.Solution.ResetRandomGenerator(DateTimeOffset.Now.Millisecond);
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if (TestInfo is object)
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue(TestInfo.Exists, $"{nameof(TestInfo)} settings should exist");
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( isr.Numerical.SimplexTests.PulseResponseSimplexTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit,
                $"{nameof( isr.Numerical.SimplexTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
            Assert.IsTrue(PulseResponseSimplexTestInfo.Get().Exists, $"{typeof(PulseResponseSimplexTestInfo)} settings should exist");
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant(false)]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " BUILDERS "

        /// <summary> Enumerates build time series in this collection. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="sampleInterval"> The sample interval. </param>
        /// <param name="count">          Number of elements. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process build time series in this collection.
        /// </returns>
        private static IEnumerable<double> BuildTimeSeries(double sampleInterval, int count)
        {
            var l = new List<double>();
            for (int i = 0, loopTo = count - 1; i <= loopTo; i++)
                l.Add(i * sampleInterval);
            return l;
        }

        /// <summary> Returns an exponent. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="asymptote">    The asymptote. </param>
        /// <param name="timeConstant"> The time constant. </param>
        /// <param name="timeSeries">   The time series. </param>
        /// <returns> A list of time and amplitude values. </returns>
        private static IEnumerable<System.Windows.Point> BuildExponent(double asymptote, double timeConstant, IEnumerable<double> timeSeries)
        {
            var l = new List<System.Windows.Point>();
            for (int i = 0, loopTo = timeSeries.Count() - 1; i <= loopTo; i++)
            {
                double t = timeSeries.ElementAtOrDefault(i);
                double TinverseTau = t / timeConstant;
                double value = asymptote * (1d - Math.Exp(-TinverseTau));
                l.Add(new System.Windows.Point(t, value));
            }

            return l;
        }

        /// <summary> Enumerates add in this collection. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="timeSeries"> The time series. </param>
        /// <param name="noise">      The noise. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process add in this collection.
        /// </returns>
        private static IEnumerable<System.Windows.Point> Add(IEnumerable<System.Windows.Point> timeSeries, IEnumerable<double> noise)
        {
            var l = new List<System.Windows.Point>();
            for (int i = 0, loopTo = timeSeries.Count() - 1; i <= loopTo; i++)
                l.Add(new System.Windows.Point(timeSeries.ElementAtOrDefault(i).X, timeSeries.ElementAtOrDefault(i).Y + noise.ElementAtOrDefault(i)));
            return l;
        }

        /// <summary> Builds an exponent using integration. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="asymptote">    The asymptote. </param>
        /// <param name="timeConstant"> The time constant. </param>
        /// <param name="count">        Number of. </param>
        /// <returns> A list of. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>")]
        private static IEnumerable<System.Windows.Point> Integrate(double asymptote, double timeConstant, int count)
        {
            var l = new List<System.Windows.Point>();
            double vc = 0d;
            double deltaT = 0.0001d;
            _ = deltaT * (asymptote - vc) / timeConstant;
            for (int i = 0, loopTo = count - 1; i <= loopTo; i++)
            {
                l.Add(new System.Windows.Point((float)(deltaT * (i + 1)), (float)vc));
                double deltaV = deltaT * (asymptote - vc) / timeConstant;
                vc += deltaV;
            }

            return l;
        }

        #endregion

        #region " TESTS "

        /// <summary> (Unit Test Method) tests exponent simplex. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        [TestMethod()]
        public void ExponentSimplexTest()
        {
            double asymptote = PulseResponseSimplexTestInfo.Get().Asymptote;
            int dataPoints = PulseResponseSimplexTestInfo.Get().DataPoints;
            double timeConstant = PulseResponseSimplexTestInfo.Get().TimeConstant;
            var rnd = Optima.Solution.Random;
            double expectedAsymptote = asymptote * rnd.NextUniform(0.9d, 1.1d);
            double expectedTimeConstant = timeConstant * rnd.NextUniform(0.9d, 1.1d);

            // model initialization parameters
            var asymptoteRange = new double[] { 0.5d * asymptote, 2d * asymptote };
            var negativeInverseTauRange = new double[] { -2 / timeConstant, -0.5d / timeConstant };
            var noise = new List<double>(rnd.Normal(dataPoints, 0d, PulseResponseSimplexTestInfo.Get().RelativeNoiseLevel * asymptote));
            var noiseSample = new Core.Engineering.SampleStatistics();
            noiseSample.AddValues(noise.ToArray());
            noiseSample.Evaluate();
            var timeSeries = new List<double>(BuildTimeSeries(PulseResponseSimplexTestInfo.Get().SamplingInterval, PulseResponseSimplexTestInfo.Get().DataPoints));
            var exponenet = new List<System.Windows.Point>(BuildExponent(expectedAsymptote, expectedTimeConstant, timeSeries));
            var testData = new List<System.Windows.Point>(Add(exponenet, noise));
            var _Model = new Optima.PulseResponseFunction(testData) { ObjectiveFunctionMode = PulseResponseSimplexTestInfo.Get().ObjectiveFunctionMode };
            double expectedMaximumSSQ = noiseSample.SumSquareDeviations;
            double objectivePrecision = 0d;
            double asymptoteAccuracy = PulseResponseSimplexTestInfo.Get().AsymptoteAccuracy * expectedAsymptote;
            double inverseTauAccuracy = PulseResponseSimplexTestInfo.Get().TimeConstantAccuracy / expectedTimeConstant;
            var testSample = new Core.Engineering.SampleStatistics();
            foreach (System.Windows.Point x in testData)
                testSample.AddValue(x.Y);
            var exponentSample = new Core.Engineering.SampleStatistics();
            foreach (System.Windows.Point x in exponenet)
                exponentSample.AddValue(x.Y);
            exponentSample.CastToArray();
            double expectedCorrelationCoeffient = testSample.EvaluateCorrelationCoefficient(exponentSample.ValuesArray);
            if (_Model.ObjectiveFunctionMode == Optima.ObjectiveFunctionMode.Deviations)
            {
                objectivePrecision = PulseResponseSimplexTestInfo.Get().RelativeObjectiveLimit * expectedMaximumSSQ;
            }
            else if (_Model.ObjectiveFunctionMode == Optima.ObjectiveFunctionMode.Correlation)
            {
                objectivePrecision = PulseResponseSimplexTestInfo.Get().RelativeObjectiveLimit * (1d - expectedCorrelationCoeffient);
            }

            Optima.Simplex simplex;
            int dimension = 2;
            simplex = new Optima.Simplex("Exponent", dimension, new double[] { asymptoteRange[0], negativeInverseTauRange[0] }, new double[] { asymptoteRange[1], negativeInverseTauRange[1] }, PulseResponseSimplexTestInfo.Get().IterationCount, new double[] { PulseResponseSimplexTestInfo.Get().RelativeConvergenceRadius * asymptoteAccuracy, PulseResponseSimplexTestInfo.Get().RelativeConvergenceRadius * inverseTauAccuracy }, objectivePrecision);
            simplex.Initialize(Optima.Solution.Random, _Model);
            string intialSimplex = simplex.ToString();
            // simplex.Solve()
            _ = simplex.Solve( () => simplex.HitCountOverflow() || simplex.Converged() && simplex.Optimized() );

            // update the model function values
            _ = _Model.EvaluateObjective( simplex.BestSolution.Values() );
            double actualAsymptote = simplex.BestSolution.Values().ElementAtOrDefault(0);
            double actualTimeConstant = -1 / simplex.BestSolution.Values().ElementAtOrDefault(1);
            TestInfo.TraceMessage($"Expected  exponent {expectedAsymptote:G4}(1-exp(-t/{expectedTimeConstant:G4})");
            TestInfo.TraceMessage($"Estimated exponent {actualAsymptote:G4}(1-exp(-t/{actualTimeConstant:G4})");
            TestInfo.TraceMessage($"Initial simplex:{Environment.NewLine}{intialSimplex}");
            TestInfo.TraceMessage($" Final simplex:{Environment.NewLine}{simplex}");
            TestInfo.TraceMessage($"Best solution {simplex.BestSolution} found after {simplex.IterationNumber} iterations");
            TestInfo.TraceMessage($"Best solution Objective: {simplex.BestSolution.Objective:G4}; Desired: {objectivePrecision:G4}");
            // TestInfo.TraceMessage("Expected Asymptote = {0:G4}", expectedAsymptote)
            // TestInfo.TraceMessage("Expected Time Constant = {0:G4}", expectedTimeConstant)
            // TestInfo.TraceMessage("Estimated Time Constant = {0:G4}", actualTimeConstant)
            TestInfo.TraceMessage("Correlation Coefficient = {0:G5}", (object)_Model.EvaluateCorrelationCoefficient());
            TestInfo.TraceMessage("Exp.  Corr. Coefficient = {0:G5}", (object)expectedCorrelationCoeffient);
            TestInfo.TraceMessage("         Standard Error = {0:G4}", (object)_Model.EvaluateStandardError(simplex.BestSolution.Objective));
            // TestInfo.TraceMessage("Simulated Noise = {0:G4}", noiseSample.Sigma)
            TestInfo.TraceMessage("Simulated SSQ = {0:G4}", (object)noiseSample.SumSquareDeviations);
            TestInfo.TraceMessage("    Model SSQ = {0:G4}", (object)_Model.EvaluateSquareDeviations());
            TestInfo.TraceMessage($"Converged: {simplex.Converged()}");
            TestInfo.TraceMessage($"Optimized: {simplex.Optimized()}");

            // often, the expected solution resides outside the simplex convergence region between the best
            // and worst simplex nodes. Consequently, the distance between the best and solution and
            // expected value may exceed the convergence precision. This the expected precision cannot be
            // used to predict the success of the unit test. 
            Assert.AreEqual(expectedAsymptote, actualAsymptote, PulseResponseSimplexTestInfo.Get().AsymptoteAccuracy * expectedAsymptote, $"Asymptote failed; Converged: {simplex.Converged()}; Optimized: {simplex.Optimized()}; Expected {expectedAsymptote:G4}(1-exp(-t/{expectedTimeConstant:G4})");
            Assert.AreEqual(expectedTimeConstant, actualTimeConstant, PulseResponseSimplexTestInfo.Get().TimeConstantAccuracy * expectedTimeConstant, $"Time constant failed; Converged: {simplex.Converged()}; Optimized: {simplex.Optimized()}; Expected {expectedAsymptote:G4}(1-exp(-t/{expectedTimeConstant:G4})");
        }

        #endregion

    }
}
