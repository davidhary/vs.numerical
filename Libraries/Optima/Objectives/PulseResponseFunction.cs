using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Numerical.Optima
{

    /// <summary> A Pulse Response function. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-09-23 </para>
    /// </remarks>
    public class PulseResponseFunction : ApproximationFunctionBase
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="observations"> The observations. </param>
        public PulseResponseFunction( double[][] observations ) : base( observations )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="observations"> The observations. </param>
        public PulseResponseFunction( IEnumerable<System.Windows.Point> observations ) : base( observations )
        {
        }

        /// <summary> Gets or sets the objective function mode. </summary>
        /// <value> The objective function mode. </value>
        public ObjectiveFunctionMode ObjectiveFunctionMode { get; set; }

        /// <summary> Evaluates the objective value for the specified arguments. </summary>
        /// <remarks>
        /// Evaluates the sum of squares of differences between <see cref="FunctionValue">the function
        /// values</see> and the observations.
        /// </remarks>
        /// <param name="arguments"> The solution values. </param>
        /// <returns> The objective. </returns>
        public override double EvaluateObjective( IEnumerable<double> arguments )
        {
            switch ( this.ObjectiveFunctionMode )
            {
                case ObjectiveFunctionMode.Correlation:
                    {
                        this.EvaluateFunctionValues( arguments );
                        return 1d - this.EvaluateCorrelationCoefficient();
                    }

                default:
                    {
                        this.EvaluateFunctionValues( arguments );
                        return this.EvaluateSquareDeviations();
                    }
            }
        }

        /// <summary> Function Value. </summary>
        /// <remarks>
        /// Evaluates the pulse response function<para>
        /// v(t) = V(1-e(-t/T)) where</para><para>
        /// V = argument(0) and </para><para>
        /// -1/T = argument(1) </para>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="arguments">   The solution values. </param>
        /// <param name="observation"> The observation. </param>
        /// <returns> The function value at the specified arguments. </returns>
        public override double FunctionValue( IEnumerable<double> arguments, IEnumerable<double> observation )
        {
            if ( arguments is null )
                throw new ArgumentNullException( nameof( arguments ) );
            if ( observation is null )
                throw new ArgumentNullException( nameof( observation ) );
            return arguments.ElementAtOrDefault( 0 ) * (1d - Math.Exp( observation.ElementAtOrDefault( 0 ) * arguments.ElementAtOrDefault( 1 ) ));
        }
    }

    /// <summary> Values that represent objective function mode. </summary>
    /// <remarks> David, 2020-10-09. </remarks>
    public enum ObjectiveFunctionMode
    {

        /// <summary> An enum constant representing the deviations option. </summary>
        Deviations,

        /// <summary> An enum constant representing the correlation option. </summary>
        Correlation
    }
}
