using System;
using isr.Numerical.Optima;
using Microsoft.VisualStudio.TestTools.UnitTesting;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Numerical.SimplexTests
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary>
    /// This is a test class for RosenbrockFunctionSimplexTest and is intended to contain all
    /// RosenbrockFunctionSimplexTest Unit Tests.
    /// </summary>
    /// <remarks> David, 2020-10-09. </remarks>
    [TestClass()]
    public class RosenbrockFunctionSimplexTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        [ClassInitialize()]
        [CLSCompliant(false)]
        public static void MyClassInitialize(TestContext testContext)
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue(TestInfo.TraceMessagesQueueListener);
                TestInfo.AddTraceMessagesQueue(Optima.My.MyLibrary.UnpublishedTraceMessages);
                TestInfo.InitializeTraceListener();
                Solution.ResetRandomGenerator(RosenbrockFunctionSimplexTestInfo.Get().Seed);
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if (TestInfo is object)
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue(TestInfo.Exists, $"{nameof(TestInfo)} settings should exist");
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( isr.Numerical.SimplexTests.RosenbrockFunctionSimplexTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit, 
                $"{nameof( isr.Numerical.SimplexTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
            Assert.IsTrue(RosenbrockFunctionSimplexTestInfo.Get().Exists, $"{typeof(RosenbrockFunctionSimplexTestInfo)} settings should exist");
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " SOLVER "

        /// <summary> A test for Solver. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        [TestMethod()]
        public void RosenbrockFunctionMinimization()
        {
            TestInfo.TraceMessage("Begin simplex method optimization demo");
            TestInfo.TraceMessage("Solving Rosenbrock function f(x,y) = 100*(y-x^2)^2 + (1-x)^2");
            TestInfo.TraceMessage("Function Has a minimum at x = 1.0, y = 1.0 when f = 0.0");
            int dimension = 2; // problem dimension (number of variables to solve for)
            double minX = RosenbrockFunctionSimplexTestInfo.Get().Minimum;
            double maxX = RosenbrockFunctionSimplexTestInfo.Get().Maximum;
            int maxLoop = RosenbrockFunctionSimplexTestInfo.Get().IterationCount;
            double objectivePrecision = RosenbrockFunctionSimplexTestInfo.Get().Objective;
            double valuesPrecision = RosenbrockFunctionSimplexTestInfo.Get().Convergence;

            // an simplex method optimization solver
            var simplex = new Simplex("Rosenbrock", dimension, minX, maxX, maxLoop, valuesPrecision, objectivePrecision);
            simplex.Initialize(Solution.Random, new RosenbrockObjectiveFunction());
            TestInfo.TraceMessage("Initial simplex is: ");
            TestInfo.TraceMessage(simplex.ToString());
            var sln = simplex.Solve();
            TestInfo.TraceMessage("Final simplex is: ");
            TestInfo.TraceMessage(simplex.ToString());
            TestInfo.TraceMessage("Best solution found after {0} iterations: ", (object)simplex.IterationNumber);
            TestInfo.TraceMessage(sln.ToString());
            TestInfo.TraceMessage("End simplex method optimization test");
            double actual = sln.Objective;
            double expected = 0d;
            Assert.AreEqual(expected, actual, objectivePrecision);
        }
        #endregion

    }
}
