''' <summary> A potential Solution. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/19/2014, 1.0.0.0. based on Amoeba Method Optimization using C# by James McCaffrey
''' http://msdn.microsoft.com/en-us/magazine/dn201752.aspx </para>
''' </remarks>
Public Class Solution
    Implements IComparable(Of Solution)

#Region " CONSTRUCTION "

    ''' <summary> Constructor for a solution using a linear range of values. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="slope">             The slope. </param>
    ''' <param name="dimension">         The dimension. </param>
    ''' <param name="min">               The minimum of the solution value. </param>
    ''' <param name="max">               The maximum of the solution value. </param>
    ''' <param name="objectiveFunction"> The objective function. </param>
    Public Sub New(ByVal slope As Double, ByVal dimension As Integer, ByVal min As Double, ByVal max As Double, ByVal objectiveFunction As ObjectiveFunctionBase)
        Me.New(Solution.Linear(slope, dimension, min, max), objectiveFunction)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="slope">             The slope. </param>
    ''' <param name="minima">            The minimum of the solution values. </param>
    ''' <param name="maxima">            The maximum of the solution values. </param>
    ''' <param name="objectiveFunction"> The objective function. </param>
    Public Sub New(ByVal slope As Double, ByVal minima As IEnumerable(Of Double), ByVal maxima As IEnumerable(Of Double),
                   ByVal objectiveFunction As ObjectiveFunctionBase)
        Me.New(Solution.Linear(slope, minima, maxima), objectiveFunction)
    End Sub

    ''' <summary> Constructor for a solution using a linear range of values. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="random">            The random generator for creating random solutions. </param>
    ''' <param name="slope">             The slope. </param>
    ''' <param name="minima">            The minimum of the solution values. </param>
    ''' <param name="maxima">            The maximum of the solution values. </param>
    ''' <param name="objectiveFunction"> The objective function. </param>
    Public Sub New(ByVal random As Random,
                   ByVal slope As Double, ByVal minima As IEnumerable(Of Double), ByVal maxima As IEnumerable(Of Double),
                   ByVal objectiveFunction As ObjectiveFunctionBase)
        Me.New(Solution.Linear(random, slope, minima, maxima), objectiveFunction)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="random">            The random generator for creating random solutions. </param>
    ''' <param name="dimension">         The dimension. </param>
    ''' <param name="min">               The minimum of the solution value. </param>
    ''' <param name="max">               The maximum of the solution value. </param>
    ''' <param name="objectiveFunction"> The objective function. </param>
    Public Sub New(ByVal random As Random,
                   ByVal dimension As Integer, ByVal min As Double, ByVal max As Double, ByVal objectiveFunction As ObjectiveFunctionBase)
        Me.New(Solution.RandomUniform(random, dimension, min, max), objectiveFunction)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="random">            The random generator for creating random solutions. </param>
    ''' <param name="minima">            The minimum of the solution values. </param>
    ''' <param name="maxima">            The maximum of the solution values. </param>
    ''' <param name="objectiveFunction"> The objective function. </param>
    Public Sub New(ByVal random As Random,
                   ByVal minima As IEnumerable(Of Double), ByVal maxima As IEnumerable(Of Double),
                   ByVal objectiveFunction As ObjectiveFunctionBase)
        Me.New(Solution.RandomUniform(random, minima, maxima), objectiveFunction)
    End Sub

    ''' <summary> Constructor for initialization only. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="values">            The potential solution values. </param>
    ''' <param name="objectiveFunction"> The objective function. </param>
    Private Sub New(ByVal values As IEnumerable(Of Double), ByVal objectiveFunction As ObjectiveFunctionBase)
        Me.New(values, ObjectiveFunctionBase.ValidateObjectiveFunction(objectiveFunction).EvaluateObjective(values))
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="values">         The potential solution values. </param>
    ''' <param name="objectiveValue"> The objective value. </param>
    Public Sub New(ByVal values As IEnumerable(Of Double), ByVal objectiveValue As Double)
        MyBase.New()
        Me.Initialize(values, objectiveValue)
    End Sub

    ''' <summary> Copy constructor. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="solution"> The solution. </param>
    Public Sub New(ByVal solution As Solution)
        Me.New(Solution.ValidateSolution(solution).Values, Solution.ValidateSolution(solution).Objective)
    End Sub

    ''' <summary> Validates the solution described by solution. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="solution"> The solution. </param>
    ''' <returns> A Solution. </returns>
    Public Shared Function ValidateSolution(ByVal solution As Solution) As Solution
        If solution Is Nothing Then Throw New ArgumentNullException(NameOf(solution))
        Return solution
    End Function

    ''' <summary> Initializes this object. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values">         The potential solution values. </param>
    ''' <param name="objectiveValue"> The objective value. </param>
    Private Sub Initialize(ByVal values As IEnumerable(Of Double), ByVal objectiveValue As Double)
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Me._Values = New List(Of Double)(values)
        Me._Objective = objectiveValue
    End Sub

#End Region

#Region " LINEAR VALUES GENERATORS "

    ''' <summary> Generate a linear range of values between the minimum and maximum. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="slope">   The slope. </param>
    ''' <param name="minimum"> The minimum. </param>
    ''' <param name="maximum"> The maximum. </param>
    ''' <returns> A Double. </returns>
    Private Shared Function Linear(ByVal slope As Double, ByVal minimum As Double, ByVal maximum As Double) As Double
        Return minimum + slope * (maximum - minimum)
    End Function

    ''' <summary> Generate a linear range of values between the minimum and maximum. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
    '''                                          illegal values. </exception>
    ''' <param name="slope">  The slope. </param>
    ''' <param name="minima"> The minimum. </param>
    ''' <param name="maxima"> The maximum. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process uniform in this collection.
    ''' </returns>
    Private Shared Function Linear(ByVal slope As Double, ByVal minima As IEnumerable(Of Double), ByVal maxima As IEnumerable(Of Double)) As IEnumerable(Of Double)
        If minima Is Nothing Then
            Throw New ArgumentNullException(NameOf(minima))
        ElseIf maxima Is Nothing Then
            Throw New ArgumentNullException(NameOf(maxima))
        ElseIf Not minima.Any Then
            Throw New ArgumentException("Array is empty", NameOf(minima))
        ElseIf Not maxima.Any Then
            Throw New ArgumentException("Array is empty", NameOf(maxima))
        ElseIf minima.Count <> maxima.Count Then
            Throw New ArgumentException("Length of inputs must be the same", NameOf(maxima))
        End If
        Dim l As New List(Of Double)
        For i As Integer = 0 To minima.Count - 1
            l.Add(Solution.Linear(slope, minima(i), maxima(i)))
        Next i
        Return l
    End Function

    ''' <summary> Generate a linear range of values between the minimum and maximum. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="slope">  The slope. </param>
    ''' <param name="length"> The length. </param>
    ''' <param name="min">    The minimum of the solution value. </param>
    ''' <param name="max">    The maximum of the solution value. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process uniform in this collection.
    ''' </returns>
    Private Shared Function Linear(ByVal slope As Double, ByVal length As Integer, ByVal min As Double, ByVal max As Double) As IEnumerable(Of Double)
        If length <= 0 Then Throw New ArgumentException("Length must be positive", NameOf(length))
        Dim values() As Double = New Double(length - 1) {}
        For i As Integer = 0 To length - 1
            values(i) = Solution.Linear(slope, min, max)
        Next i
        Return values
    End Function

    ''' <summary> Generate a linear range of values between the minimum and maximum. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="random">  The random generator for creating random solutions. </param>
    ''' <param name="slope">   The slope. </param>
    ''' <param name="minimum"> The minimum. </param>
    ''' <param name="maximum"> The maximum. </param>
    ''' <returns> A Double. </returns>
    Private Shared Function Linear(ByVal random As Random, ByVal slope As Double, ByVal minimum As Double, ByVal maximum As Double) As Double
        Return NextRandomUniform(random, minimum, minimum + slope * (maximum - minimum))
    End Function

    ''' <summary> Generate a linear range of values between the minimum and maximum. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
    '''                                          illegal values. </exception>
    ''' <param name="random"> The random generator for creating random solutions. </param>
    ''' <param name="slope">  The slope. </param>
    ''' <param name="minima"> The minimum. </param>
    ''' <param name="maxima"> The maximum. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process uniform in this collection.
    ''' </returns>
    Private Shared Function Linear(ByVal random As Random, ByVal slope As Double,
                                   ByVal minima As IEnumerable(Of Double), ByVal maxima As IEnumerable(Of Double)) As IEnumerable(Of Double)
        If minima Is Nothing Then
            Throw New ArgumentNullException(NameOf(minima))
        ElseIf maxima Is Nothing Then
            Throw New ArgumentNullException(NameOf(maxima))
        ElseIf Not minima.Any Then
            Throw New ArgumentException("Array is empty", NameOf(minima))
        ElseIf Not maxima.Any Then
            Throw New ArgumentException("Array is empty", NameOf(maxima))
        ElseIf minima.Count <> maxima.Count Then
            Throw New ArgumentException("Length of inputs must be the same", NameOf(maxima))
        End If
        Dim l As New List(Of Double)
        For i As Integer = 0 To minima.Count - 1
            l.Add(Solution.Linear(random, slope, minima(i), maxima(i)))
        Next i
        Return l
    End Function

#End Region

#Region " RANDOM VALUES GENERATORS "

    ''' <summary> Gets or sets the random. </summary>
    ''' <value> The random. </value>
    Public Shared ReadOnly Property Random As New System.Random(DateTimeOffset.Now.Millisecond)

    ''' <summary> Resets the random generator described by seed. </summary>
    ''' <remarks> use from unit tests to get predictable results. </remarks>
    ''' <param name="seed"> The seed. </param>
    Public Shared Sub ResetRandomGenerator(ByVal seed As Integer)
        Solution._Random = New System.Random(seed)
    End Sub

    ''' <summary> Generates the next random uniform number. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="random"> The random generator for creating random solutions. </param>
    ''' <param name="min">    The minimum of the solution value. </param>
    ''' <param name="max">    The maximum of the solution value. </param>
    ''' <returns> A random value between the minimum and maximum. </returns>
    Private Shared Function NextRandomUniform(ByVal random As Random, ByVal min As Double, ByVal max As Double) As Double
        Return (max - min) * random.NextDouble + min
    End Function

    ''' <summary> Generates an array of random numbers uniformly distributed. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="random"> The random generator for creating random solutions. </param>
    ''' <param name="length"> The length. </param>
    ''' <param name="min">    The minimum of the solution value. </param>
    ''' <param name="max">    The maximum of the solution value. </param>
    ''' <returns> An array of random numbers. </returns>
    Private Shared Function RandomUniform(ByVal random As Random, ByVal length As Integer, ByVal min As Double, ByVal max As Double) As IEnumerable(Of Double)
        If length <= 0 Then Throw New ArgumentException("Length must be positive", NameOf(length))
        Dim values() As Double = New Double(length - 1) {}
        For i As Integer = 0 To values.Length - 1
            values(i) = Solution.NextRandomUniform(random, min, max)
        Next i
        Return values
    End Function

    ''' <summary> Generates an array of random numbers uniformly distributed. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
    '''                                          illegal values. </exception>
    ''' <param name="random"> The random generator for creating random solutions. </param>
    ''' <param name="minima"> The minimum. </param>
    ''' <param name="maxima"> The maximum. </param>
    ''' <returns> An array of random numbers. </returns>
    Private Shared Function RandomUniform(ByVal random As Random,
                                          ByVal minima As IEnumerable(Of Double),
                                          ByVal maxima As IEnumerable(Of Double)) As IEnumerable(Of Double)
        If minima Is Nothing Then
            Throw New ArgumentNullException(NameOf(minima))
        ElseIf maxima Is Nothing Then
            Throw New ArgumentNullException(NameOf(maxima))
        ElseIf Not minima.Any Then
            Throw New ArgumentException("Array is empty", NameOf(minima))
        ElseIf Not maxima.Any Then
            Throw New ArgumentException("Array is empty", NameOf(maxima))
        ElseIf minima.Count <> maxima.Count Then
            Throw New ArgumentException("Length of inputs must be the same", NameOf(maxima))
        End If
        Dim l As New List(Of Double)
        For i As Integer = 0 To minima.Count - 1
            l.Add(Solution.NextRandomUniform(random, minima(i), maxima(i)))
        Next i
        Return l
    End Function

#End Region

#Region " I COMPARABLE "

    ''' <summary> Compares two Solution objects to determine their relative ordering. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns>
    ''' Negative if 'left' is less than 'right', 0 if they are equal, or positive if it is greater.
    ''' </returns>
    Public Shared Function Compare(ByVal left As Solution, ByVal right As Solution) As Integer
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        If right Is Nothing Then Throw New ArgumentNullException(NameOf(right))
        Return If(Nullable.Equals(left.Objective, right.Objective), 0, Nullable.Compare(left.Objective, right.Objective))
    End Function

    ''' <summary>
    ''' Compares this Solution object to another to determine their relative ordering based on the
    ''' solution <see cref="Objective">Objective</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="other"> Another instance to compare. </param>
    ''' <returns>
    ''' Negative if this object is less than the other, 0 if they are equal, or positive if this is
    ''' greater.
    ''' </returns>
    Public Function CompareTo(ByVal other As Solution) As Integer Implements IComparable(Of Solution).CompareTo
        Return Solution.Compare(Me, other)
    End Function

#End Region

#Region " EQUALS "

    ''' <summary> Tests if two Solution objects are considered equal. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>True</c> if the two solutions have the same objective value. </returns>
    Public Overloads Shared Function Equals(ByVal left As Object, ByVal right As Object) As Boolean
        Return Solution.Equals(TryCast(left, Solution), TryCast(right, Solution))
    End Function

    ''' <summary> Tests if two Solution objects are considered equal. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>True</c> if the two solutions have the same objective value. </returns>
    Public Overloads Shared Function Equals(ByVal left As Solution, ByVal right As Solution) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return left.Objective.Equals(right.Objective)
        End If
    End Function

    ''' <summary>
    ''' Determines whether the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />.
    ''' </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="obj"> The object to compare with the current object. </param>
    ''' <returns>
    ''' true if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, false.
    ''' </returns>
    Public Overrides Function Equals(obj As Object) As Boolean
        Return Solution.Equals(Me, TryCast(obj, Solution))
    End Function

    ''' <summary> Serves as a hash function for a particular type. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <returns> A hash code for the current <see cref="T:System.Object" />. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.Objective.GetHashCode
    End Function

    ''' <summary> = casting operator. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As Solution, ByVal right As Solution) As Boolean
        Return Solution.Equals(left, right)
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As Solution, ByVal right As Solution) As Boolean
        Return Not Solution.Equals(left, right)
    End Operator

    ''' <summary> > casting operator. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator >(ByVal left As Solution, ByVal right As Solution) As Boolean
        Return Solution.Compare(left, right) > 0
    End Operator

    ''' <summary> >= casting operator. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator >=(ByVal left As Solution, ByVal right As Solution) As Boolean
        Return Solution.Compare(left, right) >= 0
    End Operator

    ''' <summary> &lt; casting operator. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <(ByVal left As Solution, ByVal right As Solution) As Boolean
        Return Solution.Compare(left, right) < 0
    End Operator

    ''' <summary> &lt;= casting operator. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <=(ByVal left As Solution, ByVal right As Solution) As Boolean
        Return Solution.Compare(left, right) <= 0
    End Operator

#End Region

#Region " VALUES and OBJECTIVE "

    ''' <summary> The values. </summary>
    Private _Values As IEnumerable(Of Double)

    ''' <summary> Gets the solution values. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <returns>
    ''' A potential solution (array of double) and associated value (so can be sorted against several
    ''' potential solutions.
    ''' </returns>
    Public Function Values() As IEnumerable(Of Double)
        Return Me._Values
    End Function

    ''' <summary> Copies from described by values. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="values"> The potential solution values. </param>
    Private Sub CopyFromThis(ByVal values As IEnumerable(Of Double))
        Me._Values = New List(Of Double)(values)
    End Sub

    ''' <summary> Copies from described by values. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="values"> The potential solution values. </param>
    Public Sub CopyFrom(ByVal values As IEnumerable(Of Double))
        Me.CopyFromThis(values)
    End Sub

    ''' <summary> Gets or sets the objective value. </summary>
    ''' <value> The objective value. </value>
    Public Property Objective As Double

    ''' <summary> Clips the values. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The potential solution values. </param>
    ''' <param name="mode">   The mode. </param>
    ''' <param name="minima"> The minimum values. </param>
    ''' <param name="maxima"> The maximum values. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process clip in this collection.
    ''' </returns>
    Public Shared Function Clip(ByVal values As IEnumerable(Of Double), ByVal mode As ClippingModes,
                                ByVal minima As IEnumerable(Of Double), ByVal maxima As IEnumerable(Of Double)) As IEnumerable(Of Double)
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If minima Is Nothing Then Throw New ArgumentNullException(NameOf(minima))
        If maxima Is Nothing Then Throw New ArgumentNullException(NameOf(maxima))
        Dim l As New List(Of Double)

        For i As Integer = 0 To values.Count - 1
            Dim v As Double = values(i)
            Dim m As ClippingModes = mode

            If minima(i) = maxima(i) Then m = ClippingModes.Minimum Or ClippingModes.Maximum
            If m <> ClippingModes.None Then
                If ((m And ClippingModes.Minimum) = ClippingModes.Minimum) AndAlso (v < minima(i)) Then
                    v = minima(i)
                ElseIf ((m And ClippingModes.Maximum) = ClippingModes.Maximum) AndAlso (v > maxima(i)) Then
                    v = maxima(i)
                End If
            End If
            l.Add(v)
        Next
        Return l
    End Function

#End Region

#Region " PRESENTATION "

    ''' <summary> Gets or sets the default values format. </summary>
    ''' <value> The default values format. </value>
    Public Shared Property DefaultValuesFormat As String = "G5"

    ''' <summary> Gets or sets the default objective format. </summary>
    ''' <value> The default objective format. </value>
    Public Shared Property DefaultObjectiveFormat As String = "G3"

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <returns> A string that represents the current object. </returns>
    Public Overrides Function ToString() As String
        Return Me.ToString(Solution.DefaultValuesFormat, Solution.DefaultObjectiveFormat)
    End Function

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <param name="valuesFormat">    The values format. </param>
    ''' <param name="objectiveFormat"> The objective format. </param>
    ''' <returns> A string that represents the current object. </returns>
    Public Overloads Function ToString(ByVal valuesFormat As String, ByVal objectiveFormat As String) As String
        Dim builder As New Text.StringBuilder
        builder.Append("[ ")
        For Each v As Double In Me.Values
            builder.Append($"{v.ToString(valuesFormat)} ")
        Next
        builder.Append("] = ")
        builder.Append(Me.Objective.ToString(objectiveFormat))
        Return builder.ToString
    End Function

#End Region

#Region " DISTANCE "

    ''' <summary> Compute the distance between the array and the zero array. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="solution"> The solution. </param>
    ''' <returns> The distance between the solution and the zero solution. </returns>
    Public Shared Function Hypotenuse(ByVal solution As Solution) As Double
        If solution Is Nothing Then Throw New ArgumentNullException(NameOf(solution))
        Return Optima.Solution.Hypotenuse(solution.Values)
    End Function

    ''' <summary> Compute the distance between the array and the zero array. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="polygon"> The sides of the polygon. </param>
    ''' <returns> The distance between the two arrays. </returns>
    Public Shared Function Hypotenuse(ByVal polygon As IEnumerable(Of Double)) As Double
        If polygon Is Nothing Then Throw New ArgumentNullException(NameOf(polygon))
        Dim d As Double = 0
        For Each p As Double In polygon
            d += Math.Pow(p, 2D)
        Next
        d = Math.Sqrt(d)
        Return d
    End Function

    ''' <summary> Compute the distance between two polygons. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="solution"> The solution. </param>
    ''' <param name="other">    The other polygon. </param>
    ''' <returns> The distance between the two solutions. </returns>
    Public Shared Function Hypotenuse(ByVal solution As Solution, ByVal other As Solution) As Double
        If solution Is Nothing Then Throw New ArgumentNullException(NameOf(solution))
        If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
        Return Optima.Solution.Hypotenuse(solution.Values, other.Values)
    End Function

    ''' <summary> Compute the distance between two polygons. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="polygon"> The polygon. </param>
    ''' <param name="other">   The other polygon. </param>
    ''' <returns> The distance between the two arrays. </returns>
    Public Shared Function Hypotenuse(ByVal polygon As IEnumerable(Of Double), ByVal other As IEnumerable(Of Double)) As Double
        If polygon Is Nothing Then Throw New ArgumentNullException(NameOf(polygon))
        If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
        Dim d As Double = 0
        For j As Integer = 0 To polygon.Count - 1
            d += Math.Pow((polygon(j) - other(j)), 2D)
        Next j
        d = Math.Sqrt(d)
        Return d
    End Function

    ''' <summary>
    ''' Determined if this solution converged. That is, its values are within or on a sphere around
    ''' the centroid.
    ''' </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="convergenceRadii"> The convergence radii setting the convergence sphere. </param>
    ''' <param name="centroid">         The centroid. </param>
    ''' <returns>
    ''' <c>True</c> if this solution converged. That is, its values are within or on a sphere around
    ''' the centroid.
    ''' </returns>
    Public Function Converged(ByVal convergenceRadii As IEnumerable(Of Double), ByVal centroid As Solution) As Boolean
        If convergenceRadii Is Nothing Then Throw New ArgumentNullException(NameOf(convergenceRadii))
        If centroid Is Nothing Then Throw New ArgumentNullException(NameOf(centroid))
        Dim r As Double() = Solution.Radius(Me, centroid)
        Dim affirmative As Boolean = True
        For i As Integer = 0 To r.Length - 1
            ' uses smaller or equal allows to converge to zero values.
            affirmative = affirmative AndAlso Math.Abs(r(i)) <= convergenceRadii(i)
            If Not affirmative Then
                Exit For
            End If
        Next
        Return affirmative
    End Function

    ''' <summary> The radii of the solutions from the centroid. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="solution"> The solution. </param>
    ''' <param name="centroid"> The centroid. </param>
    ''' <returns> The radiuses between the centroid and the solutions. </returns>
    Public Shared Function Radius(ByVal solution As Solution, ByVal centroid As Solution) As Double()
        If centroid Is Nothing Then Throw New ArgumentNullException(NameOf(centroid))
        If solution Is Nothing Then Throw New ArgumentNullException(NameOf(solution))
        Dim r() As Double = New Double(solution.Values.Count - 1) {}
        For i As Integer = 0 To r.Length - 1
            r(i) = solution.Values(i) - centroid.Values(i)
        Next
        Return r
    End Function

    ''' <summary> The radiuses of the solutions from the centroid. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="solutions"> The solutions. </param>
    ''' <param name="centroid">  The centroid. </param>
    ''' <returns> The radiuses between the centroid and the solutions. </returns>
    Public Shared Function Radii(ByVal solutions() As Solution, ByVal centroid As Solution) As Double()()
        If centroid Is Nothing Then Throw New ArgumentNullException(NameOf(centroid))
        If solutions Is Nothing Then Throw New ArgumentNullException(NameOf(solutions))
        Dim r()() As Double = New Double(solutions.Length - 1)() {}
        For i As Integer = 0 To solutions.GetLength(0) - 1
            r(i) = New Double(solutions(i).Values.Count - 1) {}
            For j As Integer = 0 To solutions(i).Values.Count - 1
                r(i)(j) = solutions(i).Values(j) - centroid.Values(j)
            Next
        Next
        Return r
    End Function

    ''' <summary> The distances of the solutions from the centroid. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="solutions"> The solutions. </param>
    ''' <param name="centroid">  The centroid. </param>
    ''' <returns> The distances between the centroid and the solutions. </returns>
    Public Shared Function Hypoteni(ByVal solutions() As Solution, ByVal centroid As Solution) As Double()
        If centroid Is Nothing Then Throw New ArgumentNullException(NameOf(centroid))
        If solutions Is Nothing Then Throw New ArgumentNullException(NameOf(solutions))
        Dim r(solutions.Length - 1) As Double
        For i As Integer = 0 To r.Length - 1
            r(i) = Solution.Hypotenuse(solutions(i), centroid)
        Next i
        Return r
    End Function

#End Region

End Class

