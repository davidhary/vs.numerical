''' <summary> Test information for the Polynomial Fit Tests. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2/12/2018 </para></remarks>
<Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),
 Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0"),
 Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>
Public Class RosenbrockFunctionSimplexTestInfo
    Inherits isr.Core.ApplicationSettingsBase

#Region " SINGLETON "

    ''' <summary>
    ''' Initializes an instance of the <see cref="T:System.Configuration.ApplicationSettingsBase" />
    ''' class to its default state.
    ''' </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-10-09. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration($"{GetType(RosenbrockFunctionSimplexTestInfo)} Editor", RosenbrockFunctionSimplexTestInfo.Get)
    End Sub

    ''' <summary>
    ''' Gets the locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    ''' <value> The sync locker. </value>
    Private Shared Property _SyncLocker As New Object

    ''' <summary> Gets the instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property _Instance As RosenbrockFunctionSimplexTestInfo

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As RosenbrockFunctionSimplexTestInfo
        If _Instance Is Nothing Then
            SyncLock _SyncLocker
                _Instance = CType(Global.System.Configuration.ApplicationSettingsBase.Synchronized(New RosenbrockFunctionSimplexTestInfo()), RosenbrockFunctionSimplexTestInfo)
            End SyncLock
        End If
        Return _Instance
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
    Public Shared ReadOnly Property Instantiated() As Boolean
        Get
            SyncLock _SyncLocker
                Return _Instance IsNot Nothing
            End SyncLock
        End Get
    End Property

#End Region

#Region " CONFIGURATION INFORMATION "

    ''' <summary> Returns true if test settings exist. </summary>
    ''' <value> <c>True</c> if testing settings exit. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Property Exists As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Returns true to output test messages at the verbose level. </summary>
    ''' <value> The verbose messaging level. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
    Public Property Verbose As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Returns true to enable this device. </summary>
    ''' <value> The device enable option. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Property Enabled As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets all. </summary>
    ''' <value> all. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Property All As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

#End Region

#Region " RESENBROCK CONDITIONS "

    ''' <summary> Gets or sets the seed. </summary>
    ''' <value> The seed. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1")>
    Public Property Seed As Integer
        Get
            Return Me.AppSettingGetter(0)
        End Get
        Set(value As Integer)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the number of iterations. </summary>
    ''' <value> The number of iterations. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("50")>
    Public Property IterationCount As Integer
        Get
            Return Me.AppSettingGetter(0)
        End Get
        Set(value As Integer)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the objective. </summary>
    ''' <value> The objective. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0.01")>
    Public Property Objective As Double
        Get
            Return Me.AppSettingGetter(0D)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the convergence. </summary>
    ''' <value> The convergence. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0.01")>
    Public Property Convergence As Double
        Get
            Return Me.AppSettingGetter(0D)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the minimum. </summary>
    ''' <value> The minimum value. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("-10")>
    Public Property Minimum As Double
        Get
            Return Me.AppSettingGetter(0D)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the maximum. </summary>
    ''' <value> The maximum value. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("10")>
    Public Property Maximum As Double
        Get
            Return Me.AppSettingGetter(0D)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

#End Region

End Class

