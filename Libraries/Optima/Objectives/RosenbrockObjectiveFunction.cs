using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Numerical.Optima
{

    /// <summary> Rosenbrock objective function. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-03-20 </para>
    /// </remarks>
    public class RosenbrockObjectiveFunction : ObjectiveFunctionBase
    {

        /// <summary> Evaluates the value for the specified arguments. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="arguments"> The solution values. </param>
        /// <returns> The objective or nothing if the function failed to evaluate the objective. </returns>
        public override double EvaluateObjective( IEnumerable<double> arguments )
        {
            if ( arguments is null )
                throw new ArgumentNullException( nameof( arguments ) );
            double x = arguments.ElementAtOrDefault( 0 );
            double y = arguments.ElementAtOrDefault( 1 );
            // Rosenbrock's function, the function to be minimized
            return 100.0d * Math.Pow( y - x * x, 2d ) + Math.Pow( 1d - x, 2d );
        }
    }
}
