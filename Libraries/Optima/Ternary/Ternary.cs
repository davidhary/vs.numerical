using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace isr.Numerical.Optima
{

    /// <summary> Perform a ternary search. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class Ternary : SolverBase
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="minimumValue">       The minimum value. </param>
        /// <param name="maximumValue">       The maximum value. </param>
        /// <param name="maximumIterations">  The maximum iterations. </param>
        /// <param name="valuesPrecision">    The values precision. </param>
        /// <param name="objectivePrecision"> The objective precision. </param>
        public Ternary( double minimumValue, double maximumValue, int maximumIterations, double valuesPrecision, double objectivePrecision ) : base( "ternary", 2, 100, new double[2] { valuesPrecision, valuesPrecision }, objectivePrecision )
        {
            this._MinimumValue = minimumValue;
            this._MaximumValue = maximumValue;
            this.ConvergenceRadius = valuesPrecision;
            this.ObjectiveLimit = objectivePrecision;
            this.MaximumIterations = maximumIterations;
        }

        /// <summary> The minimum value. </summary>
        private readonly double _MinimumValue;

        /// <summary> The maximum value. </summary>
        private readonly double _MaximumValue;

        #endregion

        #region " INITIALIZE "

        /// <summary> Initializes the simplex with a random set of solution values. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="objectiveFunction"> The objective function. </param>
        public void Initialize( ObjectiveFunctionBase objectiveFunction )
        {
            if ( objectiveFunction is null )
            {
                throw new ArgumentNullException( nameof( objectiveFunction ) );
            }

            this.ObjectiveFunction = objectiveFunction;
            this.Solutions = new Solution[2] { this.CreateSolution( new double[1] { this._MinimumValue }, objectiveFunction ), this.CreateSolution( new double[1] { this._MaximumValue }, objectiveFunction ) };
            // 0 is the left solution; 1 is the right solution
        }


        #endregion

        #region " TERNARY "

        /// <summary> Gets the left. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns> The left side solution. </returns>
        private Solution Left()
        {
            return this.Solutions[0];
        }

        /// <summary> Gets the right. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns> The right side solution. </returns>
        private Solution Right()
        {
            return this.Solutions[1];
        }

        /// <summary> Gets the best solution. </summary>
        /// <value> The solution. </value>
        public override Solution BestSolution => this.Centroid();

        /// <summary> Creates a solution. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="values">            The values. </param>
        /// <param name="objectiveFunction"> The objective function. </param>
        /// <returns> The new solution. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        private Solution CreateSolution( IEnumerable<double> values, ObjectiveFunctionBase objectiveFunction )
        {
            return new Solution( values, ObjectiveFunctionBase.ValidateObjectiveFunction( objectiveFunction ).EvaluateObjective( values ) );
        }

        /// <summary> Calculates the centroid solution. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <returns>
        /// The centroid of all solution vectors except for the worst (highest index) vector.
        /// </returns>
        protected override Solution Centroid()
        {
            if ( this.Left().Equals( this.Right() ) )
            {
                return new Solution( this.Left() );
            }
            else
            {
                double argument = this.Left().Values().ElementAtOrDefault( 0 ) - this.Left().Objective * (this.Right().Values().ElementAtOrDefault( 0 ) - this.Left().Values().ElementAtOrDefault( 0 )) / (this.Right().Objective - this.Left().Objective);
                return this.CreateSolution( new double[1] { argument }, this.ObjectiveFunction );
            }
        }

        #endregion

        #region " SOLVE "

        /// <summary> Move left. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="stepSize"> Size of the step. </param>
        private void MoveLeft( double stepSize )
        {
            this.Right().Objective = this.Left().Objective;
            this.Right().CopyFrom( new double[1] { this.Left().Values().ElementAtOrDefault( 0 ) } );
            this.Left().CopyFrom( new double[1] { this.Left().Values().ElementAtOrDefault( 0 ) - stepSize } );
            this.Left().Objective = this.ObjectiveFunction.EvaluateObjective( this.Left().Values() );
        }

        /// <summary> Move right. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="stepSize"> Size of the step. </param>
        private void MoveRight( double stepSize )
        {
            this.Left().Objective = this.Right().Objective;
            this.Left().CopyFrom( new double[1] { this.Right().Values().ElementAtOrDefault( 0 ) } );
            this.Right().CopyFrom( new double[1] { this.Right().Values().ElementAtOrDefault( 0 ) + stepSize } );
            this.Right().Objective = this.ObjectiveFunction.EvaluateObjective( this.Right().Values() );
        }

        /// <summary> Expands. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="stepSize"> Size of the step. </param>
        private void Expand( double stepSize )
        {
            this.Left().CopyFrom( new double[1] { this.Left().Values().ElementAtOrDefault( 0 ) - stepSize } );
            this.Right().CopyFrom( new double[1] { this.Right().Values().ElementAtOrDefault( 0 ) + stepSize } );
            this.Left().Objective = this.ObjectiveFunction.EvaluateObjective( this.Left().Values() );
            this.Right().Objective = this.ObjectiveFunction.EvaluateObjective( this.Right().Values() );
        }

        /// <summary> Step towards root. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="stepSize"> Size of the step. </param>
        private void StepTowardsRoot( double stepSize )
        {
            // left and right are the current bounds; the location of the root is outside the range
            if ( this.Left().Objective.CompareTo( 0d ) > 0 )
            {
                // value is above the function
                int slope = 0;
                switch ( slope )
                {
                    case 1:
                        {
                            // with positive slope, move left
                            this.MoveLeft( stepSize );
                            break;
                        }

                    case 0:
                        {
                            // with unknown slope, move based on the closer value.
                            int leftCompareRight = this.Left().Objective.CompareTo( this.Right().Objective );
                            switch ( leftCompareRight )
                            {
                                case 1:
                                    {
                                        // if left is larger, move right 
                                        this.MoveRight( stepSize );
                                        break;
                                    }

                                case 0:
                                    {
                                        // if left and right have the same value, expand the range.
                                        this.Expand( stepSize );
                                        break;
                                    }

                                case -1:
                                    {
                                        // if right is larger, move left
                                        this.MoveLeft( stepSize );
                                        break;
                                    }
                            }

                            break;
                        }

                    case -1:
                        {
                            // with negative slope, move right
                            this.MoveLeft( stepSize );
                            break;
                        }
                }
            }
            else
            {
                // value is below the function
                int slope = 0;
                switch ( slope )
                {
                    case 1:
                        {
                            // if slope is positive, move right
                            this.MoveRight( stepSize );
                            break;
                        }

                    case 0:
                        {
                            // is slope is unknown, use the closer value
                            int leftCompareRight = this.Left().Objective.CompareTo( this.Right().Objective );
                            switch ( leftCompareRight )
                            {
                                case 1:
                                    {
                                        // left is larger, move left
                                        this.MoveLeft( stepSize );
                                        break;
                                    }

                                case 0:
                                    {
                                        // if left and right have the same value, expand the range.
                                        this.Expand( stepSize );
                                        break;
                                    }

                                case -1:
                                    {
                                        // right is larger, move right
                                        this.MoveRight( stepSize );
                                        break;
                                    }
                            }

                            break;
                        }

                    case -1:
                        {
                            // if slope is negative, move left
                            this.MoveLeft( stepSize );
                            break;
                        }
                }
            }
        }

        /// <summary> Contracts. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="stepSize"> Size of the step. </param>
        private void Contract( double stepSize )
        {
            // left and right are the current bounds; the solution is between them
            var leftThird = this.CreateSolution( new double[1] { this.Left().Values().ElementAtOrDefault( 0 ) + stepSize / 3d }, this.ObjectiveFunction );
            if ( leftThird.Objective.Approximates( this.ObjectiveLimit ) )
            {
                // left has it; this will terminate on the next iteration.
                this.Solutions = new Solution[2] { this.Solutions[0], leftThird };
            }
            else if ( this.Left().Objective.CompareTo( 0d ) != leftThird.Objective.CompareTo( 0d ) )
            {
                // value between left and left third
                // left has it
                this.Solutions = new Solution[2] { this.Solutions[0], leftThird };
            }
            else
            {
                var rightThird = this.CreateSolution( new double[1] { this.Right().Values().ElementAtOrDefault( 0 ) - stepSize / 3d }, this.ObjectiveFunction );
                if ( rightThird.Objective.Approximates( this.ObjectiveLimit ) )
                {
                    // right has it; this will terminate on the next iteration.
                    this.Solutions = new Solution[2] { rightThird, this.Solutions[1] };
                }
                else if ( leftThird.Objective.CompareTo( 0d ) != rightThird.Objective.CompareTo( 0d ) )
                {
                    // value is between left and right thirds.
                    this.Solutions = new Solution[2] { leftThird, rightThird };
                }
                else if ( rightThird.Objective.CompareTo( 0d ) != this.Right().Objective.CompareTo( 0d ) )
                {
                    // value is between right and right third.
                    this.Solutions = new Solution[2] { rightThird, this.Solutions[1] };
                }
                else
                {
                    Debug.Assert( !Debugger.IsAttached, "Unhandled case", "Unhandled case 4 tuples ({0},{1}) ({2},{3}) ({4},{5}) ({6},{7})", this.Left().Values().ElementAtOrDefault( 0 ), this.Left().Objective, leftThird.Values().ElementAtOrDefault( 0 ), leftThird.Objective, rightThird.Values().ElementAtOrDefault( 0 ), rightThird.Objective, this.Right().Values().ElementAtOrDefault( 0 ), this.Right().Objective );
                }
            }
        }

        /// <summary> Solves one step. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        public override void SolveStep()
        {
            double stepSize = this.Right().Values().ElementAtOrDefault( 0 ) - this.Left().Values().ElementAtOrDefault( 0 );
            if ( this.Left().Objective.CompareTo( 0d ) == this.Right().Objective.CompareTo( 0d ) )
            {
                // left and right are the current bounds; the location of the root is outside the range
                this.StepTowardsRoot( stepSize );
            }
            else
            {
                this.Contract( stepSize );
            }
        }

        #endregion

    }

    /// <summary>   An extensions. </summary>
    /// <remarks>   David, 2020-10-09. </remarks>
    public static class Extensions
    {

        /// <summary> Returns True if the value approximates the reference within some delta. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="value">     The value. </param>
        /// <param name="reference"> The reference. </param>
        /// <param name="delta">     The delta. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool Approximates( this double value, double reference, double delta )
        {
            return Math.Abs( value - reference ) <= delta;
        }

        /// <summary> Returns True if the value approximates zero within some delta. </summary>
        /// <remarks> David, 2020-10-09. </remarks>
        /// <param name="value"> The value. </param>
        /// <param name="delta"> The delta. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool Approximates( this double value, double delta )
        {
            return value.Approximates( 0d, delta );
        }
    }
}
